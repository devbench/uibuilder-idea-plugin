<idea-plugin>
  <id>io.devbench.uibuilder.idea</id>
  <name>UIBuilder Support</name>
  <version>@version@</version>
  <vendor email="support@devbench.io" url="https://gitlab.com/devbench/uibuilder-idea-plugin">DevBench</vendor>

  <description><![CDATA[
      Adds support for DevBench's UIBuilder framework
    ]]></description>

  <change-notes><![CDATA[
      <ul>
        <li>
          <b>1.0.10</b></br>
          <ul>
            <li>Additional intentions to warn and provide fix for non-public or static event handler methods</li>
            <li>Added common filter template</li>
          </ul>
          <b>1.0.9</b></br>
          <ul>
            <li>added controller bean property usage handling and reference to the html usage</li>
          </ul>
          <b>1.0.8</b></br>
          <ul>
            <li>UI component ID completion filter for corresponding types and tags</li>
            <li>Fixed java tag class and html tag index for basic html</li>
          </ul>
          <b>1.0.7</b></br>
          <ul>
            <li>fixed bug where every name attribute offered completion for the datasources, not just the data-source related tags</li>
            <li>added reference handling from html files to backend component field injections</li>
            <li>improved reference between components in html and in controller beans (with fixing method usage indications)</li>
            <li>added support for uibuilder-rich-text-editor component and it's config tags</li>
            <li>added support for uibuilder-double-click component and on-double-click event on uibuilder-grid</li>
            <li>added "backend:" prefix support for "item-bind" attributes</li>
            <li>improved completion confidence</li>
          </ul>
          <b>1.0.6</b></br>
          <ul>
            <li>Bugfixes</li>
              <ul>
                <li>UIEventHandler annotation's handle fixed after the new debounce parameter</li>
              </ul>
            <li>Added initial uibuilder-multi-value support</li>
            <li>Added gutter icons and navigation for flow ID reference</li>
            <li>Other small refactors</li>
          </ul>
          <b>1.0.5</b></br>
          <ul>
            <li>Plugin facet configuration state NPE fix</li>
            <li>Bean property completion fix (controller bean part prefix fix and NPE fix + collection filter in a special case)</li>
          </ul>
          <b>1.0.4</b></br>
          <ul>
            <li>html component resolver points to the java class instead of the bower html file in case of the 1.x UIBuilder version</li>
            <li>context aware itemBind prefix</li>
            <li>itemBind completion fix for multi level propertyPath</li>
            <li>form property bind for for filter attribute and path attribute</li>
            <li>additional html component support</li>
          </ul>
          <b>1.0.3</b></br>
          <ul>
            <li>Added directory exclude list to facet configuration</li>
            <li>Improved UIBuilder detection in modules</li>
            <li>Indexing performance and other improvements</li>
            <li>Binding property resolver improvements</li>
            <li>Added form property resolver and completion for filter in filter-type, vaadin-grid-sorter path and uibuilder-item-editable path</li>
            <li>Fix: Gutter icons doesn't show up on super classes</li>
            <li>Fix: Form item class resolved incorrectly when multiple data-source tags are present in a crud-panel</li>
            <li>Fix: item-bind support for other form item class specifying components</li>
            <li>Fix: Custom components are not resolved</li>
            <li>Fixed bean property completion</li>
          </ul>
          <b>1.0.2</b></br>
          <ul>
            <li>Fixed a bug with UIBuilder framework detection</li>
            <li>item binding support for components with items attribute or item-data-source</li>
            <li>item-bind property prefix support</li>
            <li>IntelliJ IDEA 2020.3 support and removed the usage of deprecated API's</li>
          </ul>
          <b>1.0.1</b></br>
          <ul>
            <li>Flow definition XML html path annotator and quickfix</li>
            <li>UIBuilder version detection fix</li>
            <li>Updated plugin icon</li>
          </ul>
          <b>1.0.0</b></br>
          <ul>
            <li>UIBuilder 2.0.0+ and 1.3.0+ support</li>
            <li>Controller bean and event handler reference and completion</li>
            <li>Controller bean property reference and completion</li>
            <li>Page contextId to page scoped controller bean reference and completion</li>
            <li>Flow definition xml ID, html and target reference and completion (ID to flow: prefixed event references in the HTML files)</li>
            <li>HTML on-event missing controller bean quickFix (create controller bean with autodetected location)</li>
            <li>Gutter navigation for event handlers, controller bean properties and UI components</li>
            <li>Optional constant resolver in @UIEventHandler and @UIComponent annotations</li>
            <li>Initial form-item completion inside a crud-panel</li>
            <li>Import handler for UIBuilder version 1.3.0+</li>
            <li>Import remover for UIBuilder version 2.0.0+</li>
            <li>Live template for grid (grid), grid column (grid-col) and crud-panel (cp)</li>
            <li>File template for flow-definition.xml and TargetDataSourceRepository</li>
          </ul>
        </li>
      </ul>
    ]]>
  </change-notes>

  <idea-version since-build="211.6693"/>
  <depends>com.intellij.modules.java</depends>

  <extensions defaultExtensionNs="com.intellij">

    <facetType implementation="io.devbench.uibuilder.idea.facet.UIBuilderFacetType"/>
    <framework.detector implementation="io.devbench.uibuilder.idea.facet.UIBuilderFacetDetector"/>
    <framework.detector implementation="io.devbench.uibuilder.idea.facet.UIBuilderFacetDetectorControllerBean"/>

    <completion.contributor language="HTML" implementationClass="io.devbench.uibuilder.idea.completion.HtmlPageContextIdCompletionContributor"/>
    <completion.contributor language="HTML" implementationClass="io.devbench.uibuilder.idea.completion.HtmlComponentIdCompletionContributor"/>
    <completion.contributor language="HTML" implementationClass="io.devbench.uibuilder.idea.completion.HtmlEventReferenceCompletionContributor"/>
    <completion.contributor language="HTML" implementationClass="io.devbench.uibuilder.idea.completion.HtmlDataSourceNameCompletionContributor"/>
    <completion.contributor language="HTML" implementationClass="io.devbench.uibuilder.idea.completion.HtmlBeanPropertyCompletionContributor"/>
    <completion.contributor language="HTML" implementationClass="io.devbench.uibuilder.idea.completion.HtmlFormItemBindBeanPropertyCompletionContributor"/>
    <completion.contributor language="HTML"
                            implementationClass="io.devbench.uibuilder.idea.completion.HtmlCrudPanelControllerBeanReferenceCompletionContributor"/>
    <completion.contributor language="HTML" implementationClass="io.devbench.uibuilder.idea.completion.HtmlFlowNavigationEventCompletionContributor"/>
    <completion.contributor language="XML" implementationClass="io.devbench.uibuilder.idea.completion.HtmlFilterTypeFilterPropertyCompletionContributor"/>
    <completion.contributor language="XML" implementationClass="io.devbench.uibuilder.idea.completion.HtmlFormItemPathPropertyCompletionContributor"/>
    <completion.contributor language="JAVA" implementationClass="io.devbench.uibuilder.idea.completion.UIComponentIdCompletionContributor"/>
    <completion.contributor language="JAVA" implementationClass="io.devbench.uibuilder.idea.completion.UIEventHandlerCompletionContributor"/>
    <completion.contributor language="JAVA" implementationClass="io.devbench.uibuilder.idea.completion.PageScopeContextIdCompletionContributor"/>
    <completion.contributor language="XML" implementationClass="io.devbench.uibuilder.idea.completion.FlowHtmlResourcePathCompletionContributor"/>
    <completion.contributor language="XML" implementationClass="io.devbench.uibuilder.idea.completion.FlowIncludePathPathCompletionContributor"/>
    <completion.contributor language="XML" implementationClass="io.devbench.uibuilder.idea.completion.FlowTargetIdCompletionContributor"/>
    <completion.contributor language="XML" implementationClass="io.devbench.uibuilder.idea.completion.FlowFallbackIdCompletionContributor"/>

    <completion.confidence language="HTML" implementationClass="io.devbench.uibuilder.idea.completion.HtmlCompletionConfidence"/>

    <psi.referenceContributor language="JAVA" implementation="io.devbench.uibuilder.idea.references.UIBuilderJavaReferenceContributor"/>
    <psi.referenceContributor language="HTML" implementation="io.devbench.uibuilder.idea.references.UIBuilderHtmlReferenceContributor"/>
    <psi.referenceContributor language="XML" implementation="io.devbench.uibuilder.idea.references.UIBuilderFlowXmlReferenceContributor"/>

    <codeInsight.lineMarkerProvider language="JAVA" implementationClass="io.devbench.uibuilder.idea.codeinsight.EventHandlerMethodLineMarkerProvider"/>
    <codeInsight.lineMarkerProvider language="JAVA" implementationClass="io.devbench.uibuilder.idea.codeinsight.UIComponentLineMarkerProvider"/>
    <codeInsight.lineMarkerProvider language="JAVA" implementationClass="io.devbench.uibuilder.idea.codeinsight.TargetDataSourceRepositoryLineMarkerProvider"/>
    <codeInsight.lineMarkerProvider language="HTML" implementationClass="io.devbench.uibuilder.idea.codeinsight.WebComponentLineMarkerProvider"/>
    <codeInsight.lineMarkerProvider language="HTML" implementationClass="io.devbench.uibuilder.idea.codeinsight.WebEventHandlerReferenceLineMarkerProvider"/>
    <codeInsight.lineMarkerProvider language="HTML" implementationClass="io.devbench.uibuilder.idea.codeinsight.WebDataSourceLineMarkerProvider"/>
    <codeInsight.lineMarkerProvider language="HTML" implementationClass="io.devbench.uibuilder.idea.codeinsight.WebPropertyBindingLineMarkerProvider"/>
    <codeInsight.lineMarkerProvider language="HTML" implementationClass="io.devbench.uibuilder.idea.codeinsight.WebFormPropertyBindingLineMarkerProvider"/>
    <codeInsight.lineMarkerProvider language="XML" implementationClass="io.devbench.uibuilder.idea.codeinsight.FlowIdReferenceLineMarkerProvider"/>

    <annotator language="JAVA" implementationClass="io.devbench.uibuilder.idea.intention.JavaEventHandlerAnnotator"/>
    <annotator language="HTML" implementationClass="io.devbench.uibuilder.idea.intention.HtmlEventHandlerAnnotator"/>
    <annotator language="HTML" implementationClass="io.devbench.uibuilder.idea.intention.UIBuilderTagLinkAnnotator"/>
    <annotator language="HTML" implementationClass="io.devbench.uibuilder.idea.intention.UIBuilderTagLinkDeprecatedAnnotator"/>
    <annotator language="HTML" implementationClass="io.devbench.uibuilder.idea.intention.DataSourceRepositoryNameAnnotator"/>
    <annotator language="XML" implementationClass="io.devbench.uibuilder.idea.intention.FlowHtmlPathAnnotator"/>

    <fileBasedIndex implementation="io.devbench.uibuilder.idea.index.HtmlComponentIdIndex"/>
    <fileBasedIndex implementation="io.devbench.uibuilder.idea.index.HtmlEventIndex"/>
    <fileBasedIndex implementation="io.devbench.uibuilder.idea.index.HtmlDomModuleIndex"/>
    <fileBasedIndex implementation="io.devbench.uibuilder.idea.index.HtmlCrossImportIndex"/>
    <fileBasedIndex implementation="io.devbench.uibuilder.idea.index.ControllerBeanComponentIdIndex"/>
    <fileBasedIndex implementation="io.devbench.uibuilder.idea.index.ControllerBeanEventHandlerIndex"/>
    <fileBasedIndex implementation="io.devbench.uibuilder.idea.index.ControllerBeanIndex"/>
    <fileBasedIndex implementation="io.devbench.uibuilder.idea.index.TargetDataSourceIndex"/>
    <fileBasedIndex implementation="io.devbench.uibuilder.idea.index.TagIndex"/>

    <xml.tagNameProvider implementation="io.devbench.uibuilder.idea.xml.html.UIBuilderTagNameProvider"/>
    <xml.elementDescriptorProvider implementation="io.devbench.uibuilder.idea.xml.html.UIBuilderTagNameProvider" order="last"/>
    <xml.xmlExtension implementation="io.devbench.uibuilder.idea.xml.html.UIBuilderHtmlExtension" order="first" id="UIBuilderXmlExtension"/>

    <standardResource url="http://devbench.io/uibuilder/flow-definition" path="resources/flow-definition-1.0.xsd"/>

    <defaultLiveTemplates file="/liveTemplates/UIBuilder.xml"/>

    <postStartupActivity implementation="io.devbench.uibuilder.idea.util.UIBuilderStartupActivity"/>

    <referencesSearch implementation="io.devbench.uibuilder.idea.references.search.UIBuilderReferencesSearch"/>
    <referencesSearch implementation="io.devbench.uibuilder.idea.references.search.UIBuilderBeanPropertyReferencesSearch"/>
    <methodReferencesSearch implementation="io.devbench.uibuilder.idea.references.search.UIBuilderMethodReferencesSearch"/>

    <implicitUsageProvider implementation="io.devbench.uibuilder.idea.references.usage.UIBuilderMethodReferencesImplicitUsageProvider"/>
    <implicitUsageProvider implementation="io.devbench.uibuilder.idea.references.usage.UIBuilderBeanPropertyReferencesImplicitUsageProvider"/>

  </extensions>

  <actions>
    <group id="UIBuilder" text="UIBuilder">
      <action id="UIBuilderExcludeDirectory"
              class="io.devbench.uibuilder.idea.action.ExcludeDirectoryAction"
              text="Exclude Directory from UIBuilder">
        <add-to-group group-id="ProjectViewPopupMenu"/>
      </action>
    </group>
  </actions>

</idea-plugin>
