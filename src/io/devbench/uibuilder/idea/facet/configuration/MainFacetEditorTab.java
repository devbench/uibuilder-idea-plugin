package io.devbench.uibuilder.idea.facet.configuration;

import com.intellij.facet.ui.FacetEditorTab;
import com.intellij.ui.components.fields.ExpandableTextField;
import io.devbench.uibuilder.idea.facet.UIBuilderFacetConfiguration;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainFacetEditorTab extends FacetEditorTab {
    private JCheckBox autoImport;
    private JPanel panel;
    private JTextField uibuilderVersionField;
    private JCheckBox enableConstantResolver;
    private ExpandableTextField excludePathsField;

    private boolean modified = false;
    private UIBuilderFacetConfiguration configuration;

    public MainFacetEditorTab(UIBuilderFacetConfiguration configuration) {
        this.configuration = configuration;
    }

    @NotNull
    @Override
    public JComponent createComponent() {
        UIBuilderFacetConfiguration.State state = configuration.ensureState();

        uibuilderVersionField.setText(state.getUibuilderVersion());
        enableConstantResolver.setSelected(state.isResolveConstantsEnabled());

        List<String> excludePaths = state.getExcludePaths();
        if (excludePaths == null) {
            excludePaths = new ArrayList<>();
        }
        excludePathsField.setText(String.join("\n", excludePaths));

        uibuilderVersionField.addActionListener(e -> {
            configuration.ensureState().setUibuilderVersion(uibuilderVersionField.getText());
            modified = true;
        });

        enableConstantResolver.addActionListener(e -> {
            configuration.ensureState().setResolveConstantsEnabled(enableConstantResolver.isSelected());
            modified = true;
        });

        excludePathsField.addActionListener(e -> {
            configuration.ensureState().setExcludePaths(Arrays.asList(excludePathsField.getText().split("\n")));
            modified = true;
        });

        return panel;
    }

    @Nls(capitalization = Nls.Capitalization.Title)
    @Override
    public String getDisplayName() {
        return "UIBuilder";
    }

    @Override
    public boolean isModified() {
        return modified;
    }
}
