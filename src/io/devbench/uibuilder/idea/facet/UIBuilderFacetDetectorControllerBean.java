package io.devbench.uibuilder.idea.facet;

import com.intellij.facet.FacetType;
import com.intellij.facet.FacetTypeRegistry;
import com.intellij.framework.detection.FacetBasedFrameworkDetector;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.patterns.ElementPattern;
import com.intellij.util.indexing.FileContent;
import io.devbench.uibuilder.idea.util.UIBuilderJavaUtils;
import org.jetbrains.annotations.NotNull;

public class UIBuilderFacetDetectorControllerBean extends FacetBasedFrameworkDetector<UIBuilderFacet, UIBuilderFacetConfiguration> {

    public UIBuilderFacetDetectorControllerBean() {
        super(UIBuilderFacetType.UIBUILDER_FACET_TYPE);
    }

    @NotNull
    @Override
    public FacetType<UIBuilderFacet, UIBuilderFacetConfiguration> getFacetType() {
        return FacetTypeRegistry.getInstance().findFacetType(UIBuilderFacetType.UIBUILDER_FACET_TYPE_ID);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return JavaFileType.INSTANCE;
    }

    @NotNull
    @Override
    public ElementPattern<FileContent> createSuitableFilePattern() {
        return UIBuilderJavaUtils.JAVA_FILE_CONTENT_PATTERN;
    }
}
