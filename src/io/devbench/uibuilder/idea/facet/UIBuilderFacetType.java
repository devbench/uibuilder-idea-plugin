package io.devbench.uibuilder.idea.facet;

import com.intellij.facet.Facet;
import com.intellij.facet.FacetType;
import com.intellij.facet.FacetTypeId;
import com.intellij.openapi.module.JavaModuleType;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UIBuilderFacetType extends FacetType<UIBuilderFacet, UIBuilderFacetConfiguration> {

    static final String UIBUILDER_FACET_TYPE = "UIBuilderFacet";
    public static final FacetTypeId<UIBuilderFacet> UIBUILDER_FACET_TYPE_ID = new FacetTypeId<>(UIBUILDER_FACET_TYPE);

    public UIBuilderFacetType() {
        super(UIBUILDER_FACET_TYPE_ID, UIBUILDER_FACET_TYPE, "UIBuilder");
    }

    @Override
    public UIBuilderFacetConfiguration createDefaultConfiguration() {
        return new UIBuilderFacetConfiguration();
    }

    @Override
    public UIBuilderFacet createFacet(@NotNull Module module, String name, @NotNull UIBuilderFacetConfiguration configuration, @Nullable Facet underlyingFacet) {
        return new UIBuilderFacet(this, module, name, configuration, underlyingFacet);
    }

    @Override
    public boolean isSuitableModuleType(ModuleType moduleType) {
        return moduleType instanceof JavaModuleType;
    }

}
