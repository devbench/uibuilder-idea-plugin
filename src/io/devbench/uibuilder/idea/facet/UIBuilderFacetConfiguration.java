package io.devbench.uibuilder.idea.facet;

import com.intellij.facet.FacetConfiguration;
import com.intellij.facet.ui.FacetEditorContext;
import com.intellij.facet.ui.FacetEditorTab;
import com.intellij.facet.ui.FacetValidatorsManager;
import com.intellij.openapi.components.PersistentStateComponent;
import io.devbench.uibuilder.idea.facet.configuration.MainFacetEditorTab;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class UIBuilderFacetConfiguration implements FacetConfiguration, PersistentStateComponent<UIBuilderFacetConfiguration.State> {

    public static class State implements Serializable {
        private String uibuilderVersion;
        private boolean resolveConstantsEnabled;
        private List<String> excludePaths;

        public String getUibuilderVersion() {
            return uibuilderVersion;
        }

        public void setUibuilderVersion(String uibuilderVersion) {
            this.uibuilderVersion = uibuilderVersion;
        }

        public boolean isResolveConstantsEnabled() {
            return resolveConstantsEnabled;
        }

        public void setResolveConstantsEnabled(boolean resolveConstantsEnabled) {
            this.resolveConstantsEnabled = resolveConstantsEnabled;
        }

        public List<String> getExcludePaths() {
            if (excludePaths == null) {
                excludePaths = new ArrayList<>(Collections.singletonList("target/code-blocks"));
            }
            return excludePaths;
        }

        public void setExcludePaths(List<String> excludePaths) {
            this.excludePaths = excludePaths.stream()
                .filter(s -> !s.isBlank())
                .map(String::trim)
                .collect(Collectors.toList());
        }
    }

    private State state;

    @Override
    public void noStateLoaded() {
        state = new State();
    }

    @Override
    public FacetEditorTab[] createEditorTabs(FacetEditorContext editorContext, FacetValidatorsManager validatorsManager) {
        return new FacetEditorTab[] {
            new MainFacetEditorTab(this)
        };
    }

    public void initUibuilderVersion(String uibuilderVersion) {
        if (ensureState().getUibuilderVersion() == null) {
            ensureState().setUibuilderVersion(uibuilderVersion);
        }
    }

    @NotNull
    public State ensureState() {
        if (state == null) {
            noStateLoaded();
        }
        return state;
    }

    @Nullable
    @Override
    public State getState() {
        return state;
    }

    @Override
    public void loadState(@NotNull State state) {
        this.state = state;
    }
}
