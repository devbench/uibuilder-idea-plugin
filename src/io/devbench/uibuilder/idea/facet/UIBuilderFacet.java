package io.devbench.uibuilder.idea.facet;

import com.intellij.facet.Facet;
import com.intellij.facet.FacetType;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.roots.OrderEnumerator;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.atomic.AtomicReference;

public class UIBuilderFacet extends Facet<UIBuilderFacetConfiguration> {

    public UIBuilderFacet(@NotNull FacetType facetType,
                          @NotNull Module module,
                          @NotNull String name,
                          @NotNull UIBuilderFacetConfiguration configuration,
                          Facet underlyingFacet) {

        super(facetType, module, name, configuration, underlyingFacet);

        AtomicReference<String> uibuilderVersion = new AtomicReference<>("1.0.0");
        OrderEnumerator.orderEntries(module).forEachLibrary(library -> {
            if (library.getName() != null && library.getName().toLowerCase().contains("io.devbench.uibuilder.core:uibuilder-core:")) {
                String[] parts = library.getName().split(":");
                uibuilderVersion.set(parts[parts.length - 1]);
                return false;
            }
            return true;
        });
        configuration.initUibuilderVersion(uibuilderVersion.get());
    }
}
