package io.devbench.uibuilder.idea.facet;

import com.intellij.facet.FacetType;
import com.intellij.facet.FacetTypeRegistry;
import com.intellij.framework.detection.FacetBasedFrameworkDetector;
import com.intellij.ide.highlighter.HtmlFileType;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.patterns.ElementPattern;
import com.intellij.util.indexing.FileContent;
import io.devbench.uibuilder.idea.util.UIBuilderHtmlUtils;
import org.jetbrains.annotations.NotNull;

public class UIBuilderFacetDetector extends FacetBasedFrameworkDetector<UIBuilderFacet, UIBuilderFacetConfiguration> {

    public UIBuilderFacetDetector() {
        super(UIBuilderFacetType.UIBUILDER_FACET_TYPE);
    }

    @NotNull
    @Override
    public FacetType<UIBuilderFacet, UIBuilderFacetConfiguration> getFacetType() {
        return FacetTypeRegistry.getInstance().findFacetType(UIBuilderFacetType.UIBUILDER_FACET_TYPE_ID);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return HtmlFileType.INSTANCE;
    }

    @NotNull
    @Override
    public ElementPattern<FileContent> createSuitableFilePattern() {
        return UIBuilderHtmlUtils.HTML_FILE_CONTENT_PATTERN;
    }
}
