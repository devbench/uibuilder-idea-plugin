package io.devbench.uibuilder.idea.util;

import com.intellij.framework.detection.FileContentPattern;
import com.intellij.ide.highlighter.HtmlFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.patterns.PatternCondition;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiManager;
import com.intellij.psi.xml.XmlFile;
import com.intellij.util.ProcessingContext;
import com.intellij.util.indexing.FileContent;
import com.intellij.util.indexing.FileContentImpl;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;

public final class UIBuilderHtmlUtils {

    public static final FileContentPattern HTML_FILE_CONTENT_PATTERN = FileContentPattern.fileContent()
        .with(new PatternCondition<>("uibuilderPage") {
            @Override
            public boolean accepts(@NotNull final FileContent fileContent, final ProcessingContext context) {
                return fileContent.getContentAsText().toString().contains("<uibuilder-page");
            }
        });

    private UIBuilderHtmlUtils() {
        throw new UnsupportedOperationException();
    }

    public static boolean isUIBuilderHtmlFile(@NotNull VirtualFile virtualFile) {
        try {
            return virtualFile.getFileType() == HtmlFileType.INSTANCE && HTML_FILE_CONTENT_PATTERN
                .accepts(FileContentImpl.createByFile(virtualFile));
        } catch (IOException e) {
            throw new RuntimeException("Could not get file content of " + virtualFile.getPath(), e);
        }
    }

    public static void findInHtml(@NotNull Project project, @NotNull PsiElementVisitor visitor) {
        PsiManager psiManager = PsiManager.getInstance(project);
        ProjectFileIndex projectFileIndex = ProjectFileIndex.getInstance(project);
        UIBuilderProjectUtils.iterateProjectFileIndexInUIBuilderScope(project, html -> {
            XmlFile psiHtml = (XmlFile) psiManager.findFile(html);
            if (psiHtml != null) {
                psiHtml.accept(visitor);
            }
            return true;
        }, fileOrDir -> !projectFileIndex.isInTestSourceContent(fileOrDir) && !fileOrDir.isDirectory() && UIBuilderHtmlUtils.isUIBuilderHtmlFile(fileOrDir));
    }

}
