package io.devbench.uibuilder.idea.util;

import com.google.common.io.Files;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.HtmlCrossImportIndex;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class UIBuilderWebjarUtils {

    public static final String FRONTEND_BOWER_COMPONENTS = "frontend://bower_components/";
    public static final String WEBJARS_RESOURCE_PATH = "/META-INF/resources/webjars/";
    private static final Path WEBJARS_RESOURCE = Paths.get(WEBJARS_RESOURCE_PATH);

    private UIBuilderWebjarUtils() {
        throw new UnsupportedOperationException();
    }

    public static Optional<String> findLinkTagHref(@NotNull XmlTag tag) {
        if ("import".equals(tag.getAttributeValue("rel"))) {
            return Optional.ofNullable(tag.getAttributeValue("href"));
        }
        return Optional.empty();
    }

    @NotNull
    @SuppressWarnings("UnstableApiUsage")
    public static String resolveWebjarPath(@NotNull String relativeTo, @NotNull String path) {
        return Files.simplifyPath( // TODO: consider to replace this
            WEBJARS_RESOURCE.relativize(
                WEBJARS_RESOURCE.resolve(relativeTo)
                    .resolveSibling(path)).toString());
    }

    public static Optional<String> stripWebjarPath(@NotNull String path) {
        int webjarIndex = path.indexOf(WEBJARS_RESOURCE_PATH);
        if (webjarIndex != -1) {
            return Optional.of(path.substring(webjarIndex + WEBJARS_RESOURCE_PATH.length()));
        }
        return Optional.empty();
    }

    @NotNull
    public static List<String> collectImportPathsRecursively(@NotNull String tagImportPath, @NotNull GlobalSearchScope searchScope) {
        List<String> importingPaths = new ArrayList<>();
        collectImportPaths(tagImportPath, searchScope, importingPaths);
        return importingPaths;
    }

    private static void collectImportPaths(@NotNull String tagImportPath, @NotNull GlobalSearchScope searchScope, @NotNull List<String> importedPaths) {
        List<String> foundImportingPath = FileBasedIndex.getInstance().getValues(HtmlCrossImportIndex.INDEX_NAME, tagImportPath, searchScope);
        foundImportingPath.forEach(importingPath -> {
            if (!importedPaths.contains(importingPath)) {
                importedPaths.add(importingPath);
                collectImportPaths(importingPath, searchScope, importedPaths);
            }
        });
    }

}
