package io.devbench.uibuilder.idea.util;

import com.intellij.ui.IconManager;

import javax.swing.*;

public final class UIBuilderIcons {

    private static Icon load(String path) {
        return IconManager.getInstance().getIcon(path, UIBuilderIcons.class);
    }

    public final static class Gutter {
        public static final Icon EventReference = load("/images/eventReference.svg");
        public static final Icon FlowReference = load("/images/flowReference.svg");
        public static final Icon UIComponent = load("/images/uiComponent.svg");
        public static final Icon TargetDataSource = load("/images/dataSource.svg");
        public static final Icon PropertyBinding = load("/images/propertyBinding.svg");
        public static final Icon Page = load("/images/page.svg");
        public static final Icon FormPropertyBinding = load("/images/formPropertyBinding.svg");
    }

}
