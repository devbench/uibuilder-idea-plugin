package io.devbench.uibuilder.idea.util;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.impl.source.PsiClassReferenceType;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanComponentIdIndex;
import io.devbench.uibuilder.idea.index.TagIndex;
import io.devbench.uibuilder.idea.index.TargetDataSourceIndex;
import io.devbench.uibuilder.idea.references.HtmlControllerBeanReference;
import io.devbench.uibuilder.idea.references.facade.UIBuilderPropertyReference;
import io.devbench.uibuilder.idea.references.visitor.XmlAttributeVisitor;
import io.devbench.uibuilder.idea.references.visitor.XmlTagVisitor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;
import static io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils.*;

public final class UIBuilderFormUtils {

    public static final String CRUD_PANEL_TAG_NAME = "crud-panel";
    public static final String DETAIL_PANEL_TAG_NAME = "detail-panel";
    public static final String DATA_SOURCE_TAG_NAME = "data-source";
    public static final String ITEM_DATA_SOURCE_TAG_NAME = "item-data-source";
    public static final String MASTER_DETAIL_CONTROLLER_TAG_NAME = "master-detail-controller";

    public static final String UIBUILDER_EDITOR_WINDOW_TAG_NAME = "uibuilder-editor-window";
    public static final String UIBUILDER_FORM_TAG_NAME = "uibuilder-form";
    public static final String UIBUILDER_MULTI_VALUE_TAG_NAME = "uibuilder-multi-value";
    public static final String UIBUILDER_COMBOBOX_TAG_NAME = "uibuilder-combobox";
    public static final String UIBUILDER_GRID_TAG_NAME = "vaadin-uibuilder-grid";
    public static final String UIBUILDER_COMMON_FILTER_TAG_NAME = "uibuilder-common-filter";

    public static final String ITEM_BIND_ATTRIBUTE = "item-bind";
    public static final String MASTER_ATTRIBUTE = "master";
    public static final String DETAIL_ATTRIBUTE = "detail";
    public static final String ITEMS_ATTRIBUTE = "items";
    public static final String NAME_ATTRIBUTE = "name";
    public static final String ID_ATTRIBUTE = "id";

    public static final String ITEM_BIND_ITEMS_PREFIX = "items:";
    public static final String ITEM_BIND_FORM_ITEM_PREFIX = "formItem:";

    public static final Set<String> DATA_CONTAINER_TAGS = Set.of(
        UIBUILDER_COMBOBOX_TAG_NAME,
        CRUD_PANEL_TAG_NAME,
        UIBUILDER_GRID_TAG_NAME,
        UIBUILDER_COMMON_FILTER_TAG_NAME,
        UIBUILDER_MULTI_VALUE_TAG_NAME
    );
    public static final Set<String> DATA_CONTAINER_FORM = Set.of(
        DETAIL_PANEL_TAG_NAME,
        UIBUILDER_EDITOR_WINDOW_TAG_NAME,
        UIBUILDER_FORM_TAG_NAME,
        UIBUILDER_MULTI_VALUE_TAG_NAME
    );
    public static final Set<String> DATA_SOURCE_TAGS = Set.of(
        DATA_SOURCE_TAG_NAME,
        ITEM_DATA_SOURCE_TAG_NAME
    );

    private UIBuilderFormUtils() {
        throw new UnsupportedOperationException();
    }

    public static Optional<PsiClass> resolveTargetDataSourceRepositoryClass(@NotNull String dataSourceName, @NotNull Project project) {
        PsiManager psiManager = PsiManager.getInstance(project);

        VirtualFile repositoryFile = FileBasedIndex.getInstance()
            .getContainingFiles(TargetDataSourceIndex.INDEX_NAME, dataSourceName, UIBuilderProjectUtils.createSearchScope(project))
            .stream().findFirst().orElse(null);

        if (repositoryFile != null) {
            PsiJavaFile file = (PsiJavaFile) psiManager.findFile(repositoryFile);
            if (file != null) {
                for (PsiClass psiClass : file.getClasses()) {
                    PsiAnnotation annotation = psiClass.getAnnotation(FQN_TARGET_DATASOURCE_ANNOTATION);
                    if (annotation != null) {
                        String value = UIBuilderPsiTreeUtils.getNameAnnotationValue(annotation).orElse(null);
                        if (dataSourceName.equals(value)) {
                            return Optional.of(psiClass);
                        }
                    }
                }
            }
        }
        return Optional.empty();
    }

    public static Optional<PsiClass> resolveDataSourceEntityClass(@NotNull PsiClass targetDataSourceClass) {
        Project project = targetDataSourceClass.getProject();
        PsiReferenceList extendsList = targetDataSourceClass.getExtendsList();
        if (extendsList != null) {
            for (PsiClassType referencedType : extendsList.getReferencedTypes()) {
                if (referencedType.getName().endsWith("Repository") && referencedType.getParameterCount() == 2) {
                    PsiType entityType = referencedType.getParameters()[0];
                    PsiClass entityClass = JavaPsiFacade
                        .getInstance(project)
                        .findClass(entityType.getCanonicalText(), GlobalSearchScope.allScope(project));
                    return Optional.ofNullable(entityClass);
                }
            }
        }
        return Optional.empty();
    }

    public static Optional<XmlTag> findChildTag(@NotNull XmlTag context, @NotNull String tagName, @Nullable Predicate<XmlTag> stopPredicate) {
        AtomicReference<XmlTag> foundTag = new AtomicReference<>(null);
        context.accept(new XmlRecursiveElementVisitor() {
            @Override
            public void visitXmlTag(XmlTag tag) {
                if (tag.getName().toLowerCase().equals(tagName)) {
                    foundTag.set(tag);
                    return;
                }
                if (stopPredicate == null || !stopPredicate.test(tag)) {
                    super.visitXmlTag(tag);
                }
            }
        });
        return Optional.ofNullable(foundTag.get());
    }

    public static Optional<PsiType> getPropertyType(@NotNull PsiClass memberClass, @NotNull String propertyName) {
        for (PsiField field : memberClass.getAllFields()) {
            if (propertyName.equals(field.getName())) {
                return Optional.of(field.getType());
            }
        }
        for (PsiMethod method : memberClass.getAllMethods()) {
            if (method.getParameterList().isEmpty()) {
                String name = method.getName();
                if (name.startsWith("get")) {
                    name = StringUtil.decapitalize(name.substring(3));
                } else if (name.startsWith("is")) {
                    name = StringUtil.decapitalize(name.substring(2));
                } else {
                    continue;
                }
                if (name.equals(propertyName)) {
                    return Optional.ofNullable(method.getReturnType());
                }
            }
        }

        return Optional.empty();
    }

    public static Optional<PsiClass> findMemberClassByPropertyPath(@NotNull Project project,
                                                                   @NotNull String propertyPath,
                                                                   @Nullable PsiClass formItemClass,
                                                                   boolean resolveFirst) {

        return findMemberClassByPropertyPath(project, "item", propertyPath, formItemClass, resolveFirst);
    }

    public static Optional<PsiClass> findMemberClassByPropertyPath(@NotNull Project project,
                                                                   @NotNull String templateBeanName,
                                                                   @NotNull String propertyPath,
                                                                   @Nullable PsiClass formItemClass,
                                                                   boolean resolveFirst) {
        boolean first = resolveFirst;
        PsiClass previousPartClass = resolveFirst ? null : formItemClass;

        JavaPsiFacade javaPsiFacade = JavaPsiFacade.getInstance(project);
        GlobalSearchScope scope = GlobalSearchScope.allScope(project);

        for (String pathPart : propertyPath.split("\\.")) {
            if (first) {
                first = false;
                if (pathPart.equals(templateBeanName) && formItemClass != null) {
                    previousPartClass = formItemClass;
                } else {
                    previousPartClass = HtmlControllerBeanReference.getControllerBeanClass(project, pathPart).orElse(null);
                }
            } else {
                if (previousPartClass != null) {
                    previousPartClass = UIBuilderFormUtils.getPropertyType(previousPartClass, pathPart)
                        .map(nextClass -> javaPsiFacade.findClass(extractTypeParameter(nextClass).getCanonicalText(), scope))
                        .orElse(null);
                }
            }
        }

        return Optional.ofNullable(previousPartClass);
    }

    @NotNull
    private static PsiType extractTypeParameter(@NotNull PsiType type) {
        if (type instanceof PsiClassReferenceType) {
            PsiClassReferenceType nextClassReferenceType = (PsiClassReferenceType) type;
            if (nextClassReferenceType.getParameterCount() == 1) {
                return nextClassReferenceType.getParameters()[0];
            }
        }
        return type;
    }

    public static Optional<PsiClass> findMasterItemPropertyClassByMDC(PsiElement element) {
        Optional<XmlTag> foundFormTag = findEnclosingTag(element)
            .flatMap(tag -> UIBuilderPsiTreeUtils.findMatchingParentTag(tag,
                xmlTag -> isTagInCollection(xmlTag, DATA_CONTAINER_FORM)));

        if (foundFormTag.isPresent()) {
            XmlTag formTag = foundFormTag.get();
            String formId = StringUtil.trim(formTag.getAttributeValue(ID_ATTRIBUTE));
            if (formId != null) {

                PsiFile containingFile = formTag.getContainingFile();
                if (containingFile instanceof XmlFile) {
                    XmlTagVisitor mdcTagVisitor = new XmlTagVisitor(MASTER_DETAIL_CONTROLLER_TAG_NAME, true, false);
                    containingFile.accept(mdcTagVisitor);
                    List<String> masterIds = mdcTagVisitor.getFoundTags()
                        .stream()
                        .filter(mdcTag -> formId.equals(StringUtil.trim(mdcTag.getAttributeValue(DETAIL_ATTRIBUTE))))
                        .map(mdcTag -> StringUtil.trim(mdcTag.getAttributeValue(MASTER_ATTRIBUTE)))
                        .filter(Objects::nonNull)
                        .distinct()
                        .collect(Collectors.toList());

                    if (masterIds.size() == 1) {
                        String masterId = masterIds.get(0);

                        XmlAttributeVisitor masterIdVisitor = new XmlAttributeVisitor(ID_ATTRIBUTE, masterId, true);
                        containingFile.accept(masterIdVisitor);
                        if (masterIdVisitor.isFound()) {
                            return findFirstChildTag(masterIdVisitor.getFoundAttribute())
                                .flatMap(UIBuilderFormUtils::findMasterItemPropertyClass);
                        }
                    }
                }
            }
        }

        return Optional.empty();
    }

    public static Optional<PsiClass> findMasterItemPropertyClassByInjectedForm(PsiElement element) {
        Project project = element.getProject();
        JavaPsiFacade javaPsiFacade = JavaPsiFacade.getInstance(project);
        GlobalSearchScope scope = GlobalSearchScope.allScope(project);

        Optional<XmlTag> foundFormTag = findEnclosingTag(element)
            .flatMap(tag -> UIBuilderPsiTreeUtils.findMatchingParentTag(tag,
                xmlTag -> isTagInCollection(xmlTag, DATA_CONTAINER_FORM)));

        if (foundFormTag.isPresent()) {
            XmlTag formTag = foundFormTag.get();
            String formId = StringUtil.trim(formTag.getAttributeValue(ID_ATTRIBUTE));
            if (formId != null) {

                Optional<PsiClass> tagClass = findTagClassName(formTag);
                if (tagClass.isPresent()) {
                    List<PsiType> types = resolveFormTypesByInjectedComponentId(project, formId, tagClass.get());
                    if (types.stream().distinct().count() == 1) {
                        return Optional.ofNullable(javaPsiFacade.findClass(types.get(0).getCanonicalText(), scope));
                    }
                }
            }

            String itemBind = StringUtil.trim(formTag.getAttributeValue(ITEM_BIND_ATTRIBUTE));
            if (itemBind != null) {
                return findItemPropertyClassByItemsBind(formTag, itemBind);
            }
        }

        return Optional.empty();
    }

    public static Optional<PsiClass> findTagClassName(@NotNull XmlTag tag) {
        Project project = tag.getProject();
        PsiManager psiManager = PsiManager.getInstance(project);
        GlobalSearchScope scope = GlobalSearchScope.allScope(project);

        return FileBasedIndex.getInstance()
            .getContainingFiles(TagIndex.INDEX_NAME, tag.getName().toLowerCase(), scope)
            .stream()
            .map(psiManager::findFile)
            .filter(Objects::nonNull)
            .map(PsiJavaFile.class::cast)
            .map(PsiClassOwner::getClasses)
            .flatMap(Arrays::stream)
            .findFirst();
    }

    @NotNull
    public static List<PsiClass> findClassesByInjectedComponentId(@NotNull Project project, @NotNull String componentId) {
        PsiManager psiManager = PsiManager.getInstance(project);
        return FileBasedIndex.getInstance()
            .getContainingFiles(ControllerBeanComponentIdIndex.INDEX_NAME, componentId, UIBuilderProjectUtils.createSearchScope(project))
            .stream()
            .map(psiManager::findFile)
            .filter(Objects::nonNull)
            .map(PsiJavaFile.class::cast)
            .map(PsiClassOwner::getClasses)
            .flatMap(Arrays::stream)
            .collect(Collectors.toList());
    }

    private static List<PsiType> resolveFormTypesByInjectedComponentId(@NotNull Project project,
                                                                       @NotNull String componentId,
                                                                       @NotNull PsiClass tagClassName) {
        List<PsiType> formTypes = new ArrayList<>();
        for (PsiClass psiClass : findClassesByInjectedComponentId(project, componentId)) {
            for (PsiMethod psiMethod : psiClass.getMethods()) {
                PsiParameterList parameterList = psiMethod.getParameterList();
                if (!parameterList.isEmpty()) {
                    for (PsiParameter parameter : parameterList.getParameters()) {
                        if (parameter.hasAnnotation(FQN_UI_COMPONENT_ANNNOTATION)) {
                            PsiAnnotation annotation = parameter.getAnnotation(FQN_UI_COMPONENT_ANNNOTATION);
                            getValueAnnotationValue(annotation).ifPresent(value -> {
                                if (componentId.equals(value)) {
                                    PsiType type = parameter.getType();
                                    if (type instanceof PsiClassReferenceType) {
                                        PsiClassReferenceType typeReference = (PsiClassReferenceType) type;
                                        if (typeReference.getClassName().equals(tagClassName.getName())
                                            && typeReference.getParameterCount() >= 1) {
                                            formTypes.add(typeReference.getParameters()[0]);
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        }
        return formTypes;
    }

    public static Optional<PsiClass> findMasterItemPropertyClass(@NotNull PsiElement element) {
        return findDataSourceContainer(element)
            .flatMap(containerTag ->
                findDataSourceTagByContainer(containerTag, DATA_SOURCE_TAG_NAME)
                    .flatMap(UIBuilderFormUtils::findItemPropertyClassByDataSource)
                    .or(() ->
                        findDataSourceTagByContainer(containerTag, ITEM_DATA_SOURCE_TAG_NAME)
                            .flatMap(UIBuilderFormUtils::findItemPropertyClassByItemDataSource)
                            .or(() -> findDataSourceTagByContainer(containerTag, ITEM_DATA_SOURCE_TAG_NAME)
                                .filter(dataSourceTag -> isNotTheSameItemDataSourceValue(element, dataSourceTag))
                                .flatMap(UIBuilderFormUtils::findItemBindString)
                                .flatMap(itemBind -> findItemPropertyClassByItemsBind(containerTag, itemBind)))
                    )
                    .or(() ->
                        findOuterDataSourceTagByContainer(containerTag, DATA_SOURCE_TAG_NAME)
                            .flatMap(UIBuilderFormUtils::findItemPropertyClassByDataSource))
                    .or(() ->
                        findOuterDataSourceTagByContainer(containerTag, ITEM_DATA_SOURCE_TAG_NAME)
                            .flatMap(UIBuilderFormUtils::findItemPropertyClassByItemDataSource)
                            .or(() -> findOuterDataSourceTagByContainer(containerTag, ITEM_DATA_SOURCE_TAG_NAME)
                                .flatMap(UIBuilderFormUtils::findItemBindString)
                                .flatMap(itemBind -> findItemPropertyClassByItemsBind(containerTag, itemBind)))));
    }

    private static boolean isNotTheSameItemDataSourceValue(@NotNull PsiElement element, @NotNull XmlTag foundItemBindTag) {
        XmlAttribute foundItemBindAttribute = foundItemBindTag.getAttribute(ITEM_BIND_ATTRIBUTE);
        return foundItemBindAttribute == null || !element.equals(foundItemBindAttribute.getValueElement());
    }

    public static Optional<PsiClass> findItemPropertyClass(PsiElement element) {
        Optional<XmlTag> foundContainerTag = findDataSourceContainer(element);
        if (foundContainerTag.isPresent()) {
            XmlTag containerTag = foundContainerTag.get();

            Optional<PsiClass> propertyClass;

            // Find item-bind on container tag (for example the grid)
            propertyClass = findItemBindString(containerTag)
                .flatMap(itemBind -> findItemPropertyClassByItemsBind(containerTag, itemBind));

            if (propertyClass.isPresent()) {
                return propertyClass;
            }

            // Find item-bind on item-data-source under the container tag (for example the grid)
            propertyClass = findDataSourceTagByContainer(containerTag, ITEM_DATA_SOURCE_TAG_NAME)
                .flatMap(UIBuilderFormUtils::findItemBindString)
                .flatMap(itemBind -> findItemPropertyClassByItemsBind(containerTag, itemBind));

            if (propertyClass.isPresent()) {
                return propertyClass;
            }

            // Find by the DB data-source tag under the container tag
            propertyClass = findDataSourceTagByContainer(containerTag, DATA_SOURCE_TAG_NAME)
                .flatMap(UIBuilderFormUtils::findItemPropertyClassByDataSource);

            if (propertyClass.isPresent()) {
                return propertyClass;
            }

            // Find by the item-data-source tag using the items binding under the container tag
            propertyClass = findDataSourceTagByContainer(containerTag, ITEM_DATA_SOURCE_TAG_NAME)
                .flatMap(UIBuilderFormUtils::findItemPropertyClassByItemDataSource);

            if (propertyClass.isPresent()) {
                return propertyClass;
            }

            // Find by the data-source tag on the outer container if exists
            propertyClass = findOuterDataSourceTagByContainer(containerTag, DATA_SOURCE_TAG_NAME)
                .flatMap(UIBuilderFormUtils::findItemPropertyClassByDataSource);

            if (propertyClass.isPresent()) {
                return propertyClass;
            }

            // Find by the item-data-source tag on the outer container if exists
            propertyClass = findOuterDataSourceTagByContainer(containerTag, ITEM_DATA_SOURCE_TAG_NAME)
                .flatMap(UIBuilderFormUtils::findItemPropertyClassByItemDataSource);

            if (propertyClass.isPresent()) {
                return propertyClass;
            }

            XmlTag containerParentTag = containerTag.getParentTag();
            if (containerParentTag != null) {

                // Find item-bind on item-data-source under the container's parent tag (for example nexted crud-panel)
                propertyClass = findDataSourceTagByContainer(containerParentTag, ITEM_DATA_SOURCE_TAG_NAME)
                    .flatMap(UIBuilderFormUtils::findItemBindString)
                    .flatMap(itemBind -> findItemPropertyClassByItemsBind(containerParentTag, itemBind));

                if (propertyClass.isPresent()) {
                    return propertyClass;
                }
            }

        }

        return UIBuilderFormUtils.findMasterItemPropertyClassByInjectedForm(element);
    }

    public static String findTemplateBeanName(@NotNull PsiElement element) {
        Optional<XmlTag> enclosingTag = findEnclosingTag(element);
        if (enclosingTag.isPresent()) {
            XmlTag currentTag = enclosingTag.get();
            while (currentTag != null) {
                XmlAttribute asAttribute = currentTag.getAttribute("as");
                if (asAttribute != null) {
                    return asAttribute.getValue();
                }
                currentTag = currentTag.getParentTag();
            }
        }
        return "item";
    }

    private static Optional<PsiClass> findItemPropertyClassByItemDataSource(XmlTag itemDataSourceTag) {
        Project project = itemDataSourceTag.getProject();
        return Optional.ofNullable(itemDataSourceTag.getAttributeValue(ITEMS_ATTRIBUTE))
            .map(UIBuilderPropertyReference::parse)
            .flatMap(propertyReference ->
                findMemberClassByPropertyPath(project, propertyReference.getPropertyPathWithControllerBean(), null, true));
    }

    private static Optional<XmlTag> findDataSourceTagByContainer(XmlTag containerTag, String dataSourceTagName) {
        return findChildTag(
            containerTag, dataSourceTagName, tag -> isTagInCollection(tag, DATA_CONTAINER_FORM));
    }

    private static Optional<XmlTag> findOuterDataSourceTagByContainer(XmlTag containerTag, String dataSourceTagName) {
        return UIBuilderPsiTreeUtils
            .findMatchingParentTag(containerTag, xmlTag -> isTagInCollection(xmlTag, DATA_CONTAINER_TAGS))
            .flatMap(crudPanelTag ->
                findChildTag(crudPanelTag, dataSourceTagName,
                    tag -> isTagInCollection(tag, DATA_CONTAINER_FORM)));
    }

    private static Optional<PsiClass> findItemPropertyClassByDataSource(@NotNull XmlTag dataSourceTag) {
        Project project = dataSourceTag.getProject();
        return Optional.ofNullable(dataSourceTag.getAttributeValue(NAME_ATTRIBUTE))
            .flatMap(dataSourceName -> resolveTargetDataSourceRepositoryClass(dataSourceName, project))
            .flatMap(UIBuilderFormUtils::resolveDataSourceEntityClass);
    }

    private static Optional<String> findItemBindString(@NotNull XmlTag tag) {
        return Optional.ofNullable(StringUtil.trim(tag.getAttributeValue(ITEM_BIND_ATTRIBUTE)));
    }

    private static Optional<PsiClass> findItemPropertyClassByItemsBind(@NotNull XmlTag containerTag, @NotNull String itemBindString) {
        String propertyPath = null;
        if (itemBindString.startsWith(ITEM_BIND_ITEMS_PREFIX)) {
            propertyPath = itemBindString.substring(ITEM_BIND_ITEMS_PREFIX.length());
        } else if (itemBindString.startsWith(ITEM_BIND_FORM_ITEM_PREFIX)) {
            propertyPath = itemBindString.substring(ITEM_BIND_FORM_ITEM_PREFIX.length());
        }

        if (propertyPath != null) {
            String nonNullPropertyPath = propertyPath;
            return findOuterDataSourceTagByContainer(containerTag, DATA_SOURCE_TAG_NAME)
                .flatMap(UIBuilderFormUtils::findItemPropertyClassByDataSource)
                .or(() -> findOuterDataSourceTagByContainer(containerTag, ITEM_DATA_SOURCE_TAG_NAME)
                    .flatMap(UIBuilderFormUtils::findItemPropertyClassByItemDataSource))
                .or(() -> findMasterItemPropertyClassByInjectedForm(containerTag))
                .flatMap(masterPsiClass -> findMemberClassByPropertyPath(containerTag.getProject(), nonNullPropertyPath, masterPsiClass, false));
        }
        return Optional.empty();
    }

    private static Optional<XmlTag> findDataSourceContainer(PsiElement element) {
        return findEnclosingTag(element)
            .flatMap(tag ->
                UIBuilderPsiTreeUtils.findMatchingParentTag(tag, xmlTag -> isTagInCollection(xmlTag, DATA_CONTAINER_TAGS)));
    }

    private static boolean isTagInCollection(@Nullable XmlTag tag, @NotNull Set<String> collection) {
        return
            Optional.ofNullable(tag)
                .map(XmlTag::getName)
                .map(String::toLowerCase)
                .map(collection::contains)
                .orElse(false);
    }

    private static Optional<XmlTag> findEnclosingTag(@NotNull PsiElement element) {
        PsiElement tag = element;
        while (tag != null && !(tag instanceof XmlTag)) {
            tag = tag.getParent();
        }
        return Optional.ofNullable((XmlTag) tag);
    }

    private static Optional<XmlTag> findFirstChildTag(@NotNull PsiElement element) {
        return findEnclosingTag(element)
            .flatMap(xmlTag -> Arrays.stream(xmlTag.getChildren())
                .filter(XmlTag.class::isInstance)
                .map(XmlTag.class::cast)
                .findFirst());
    }

}
