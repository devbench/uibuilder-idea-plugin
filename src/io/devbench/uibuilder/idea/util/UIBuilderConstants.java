package io.devbench.uibuilder.idea.util;

public final class UIBuilderConstants {

    public static final String FQN_UI_EVENT_HANDLER_ANNOTATION = "io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler";
    public static final String FQN_UI_COMPONENT_ANNNOTATION = "io.devbench.uibuilder.api.controllerbean.UIComponent";
    public static final String FQN_CONTROLLER_BEAN_ANNOTATION = "io.devbench.uibuilder.annotations.ControllerBean";
    public static final String FQN_PAGE_SCOPE_ANNOTATION = "io.devbench.uibuilder.spring.page.PageScope";
    public static final String FQN_TARGET_DATASOURCE_ANNOTATION = "io.devbench.uibuilder.data.api.annotations.TargetDataSource";
    public static final String FQN_SPRING_REPOSITORY_ANNOTATION = "org.springframework.stereotype.Repository";

    private UIBuilderConstants() {
        throw new UnsupportedOperationException();
    }

}
