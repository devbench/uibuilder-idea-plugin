package io.devbench.uibuilder.idea.util;

import com.intellij.ide.highlighter.XmlFileType;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ModuleFileIndex;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public final class UIBuilderFlowUtils {

    private UIBuilderFlowUtils() {
        throw new UnsupportedOperationException();
    }

    public static Map<String, XmlFile> findAllFlowDefinitionXmlPaths(@NotNull Project project) {
        Map<String, XmlFile> flowDefinitionXmlPaths = new HashMap<>();

        PsiManager psiManager = PsiManager.getInstance(project);
        Arrays.stream(ModuleManager.getInstance(project).getModules()).forEach(module -> {
            ModuleRootManager moduleRootManager = ModuleRootManager.getInstance(module);
            Arrays.stream(moduleRootManager.getSourceRoots()).forEach(moduleSourceRoot -> {
                ModuleFileIndex moduleFileIndex = moduleRootManager.getFileIndex();
                moduleFileIndex.iterateContentUnderDirectory(moduleSourceRoot, file -> {
                    XmlFile xmlFile = (XmlFile) psiManager.findFile(file);
                    if (xmlFile != null && xmlFile.getDocument() != null) {
                        XmlTag rootTag = xmlFile.getDocument().getRootTag();
                        if (rootTag != null && "flows".equals(rootTag.getName())) {
                            flowDefinitionXmlPaths.put(file.getPath().substring(moduleSourceRoot.getPath().length()), xmlFile);
                        }
                    }
                    return true;
                }, fileOrDir -> !moduleFileIndex.isInTestSourceContent(fileOrDir) && !fileOrDir.isDirectory() && fileOrDir.getFileType() == XmlFileType.INSTANCE);
            });

        });

        return flowDefinitionXmlPaths;
    }

    public static List<VirtualFile> findProjectRootFlowDefinitionXml(@NotNull Project project) {
        List<VirtualFile> flowDefinitionXmls = new ArrayList<>();

        for (Module module : ModuleManager.getInstance(project).getModules()) {
            ModuleRootManager moduleRootManager = ModuleRootManager.getInstance(module);
            ModuleFileIndex moduleFileIndex = moduleRootManager.getFileIndex();

            AtomicReference<String> flowDefinitionConfigPath = new AtomicReference<>("flow-definition.xml");

            moduleFileIndex.iterateContent(conf -> {
                try {
                    Config config = ConfigFactory.parseString(new String(conf.contentsToByteArray()));
                    flowDefinitionConfigPath.set(config.getString("flowDefinition"));
                    return false;
                } catch (IOException e) {
                    Logger.getInstance(UIBuilderFlowUtils.class).error("Could not load file " + conf.getPath(), e);
                } catch (ConfigException.Missing e) {
                    Logger.getInstance(UIBuilderFlowUtils.class).error("Could not find flowDefinition value in config file " + conf.getPath(), e);
                }
                return true;
            }, fileOrDir -> !fileOrDir.isDirectory() && "application.conf".equals(fileOrDir.getName()));

            Arrays.stream(moduleRootManager.getSourceRoots())
                .map(virtualFile -> virtualFile.findFileByRelativePath(flowDefinitionConfigPath.get()))
                .filter(Objects::nonNull)
                .forEach(flowDefinitionXmls::add);
        }

        return flowDefinitionXmls;
    }

    public static Map<String, PsiElement> collectFlowIds(@NotNull Project project, @NotNull List<VirtualFile> flowDefinitionXmls) {
        Map<String, PsiElement> flowIdElements = new HashMap<>();
        PsiManager psiManager = PsiManager.getInstance(project);
        flowDefinitionXmls.stream()
            .map(psiManager::findFile)
            .filter(XmlFile.class::isInstance)
            .map(XmlFile.class::cast)
            .forEach(flowDefinitionXml -> flowDefinitionXml.accept(new XmlRecursiveElementVisitor() {
                @Override
                public void visitXmlAttribute(XmlAttribute attribute) {
                    if ("id".equals(attribute.getName()) && "flow".equals(attribute.getParent().getName())) {
                        flowIdElements.put(attribute.getValue(), attribute.getValueElement());
                    }
                    if ("path".equals(attribute.getName()) && "include".equals(attribute.getParent().getName()) && attribute.getValue() != null) {
                        collectFlowIdsFromIncluded(project, attribute.getValue(), flowIdElements);
                    }
                    super.visitXmlAttribute(attribute);
                }
            }));
        return flowIdElements;
    }

    private static void collectFlowIdsFromIncluded(@NotNull Project project, @NotNull String path, @NotNull Map<String, PsiElement> flowIdElements) {
        Arrays.stream(ModuleManager.getInstance(project).getModules()).forEach(module -> {
            List<VirtualFile> includedFlowDefinitionXmls = Arrays.stream(ModuleRootManager.getInstance(module).getSourceRoots())
                .map(virtualFile -> virtualFile.findFileByRelativePath(path))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
            collectFlowIds(project, includedFlowDefinitionXmls).forEach(flowIdElements::put);
        });
    }

    public static void withAllFlowDefinitions(@NotNull Project project, @NotNull FlowDefinitionConsumer flowDefinitionConsumer) {
        UIBuilderFlowUtils.collectFlowIds(project, UIBuilderFlowUtils.findProjectRootFlowDefinitionXml(project))
            .forEach((flowId, flowIdAttributeValue) -> flowDefinitionConsumer
                .withData(flowId, findRoute(flowIdAttributeValue), findHtml(flowIdAttributeValue)));
    }

    private static String findHtml(@NotNull PsiElement flowIdAttributeValue) {
        return findAttribute(flowIdAttributeValue)
            .map(attribute -> {
                XmlAttribute html = attribute.getParent().getAttribute("html");
                if (html == null) {
                    return "html ?";
                }
                return html.getValue();
            })
            .orElse("no html has been found");
    }

    private static String findRoute(@NotNull PsiElement flowIdAttributeValue) {
        return findAttribute(flowIdAttributeValue)
            .map(attribute -> {
                XmlAttribute route = attribute.getParent().getAttribute("route");
                if (route == null) {
                    return "route ?";
                }
                return route.getValue();
            })
            .orElse("no route has been found");
    }

    private static Optional<XmlAttribute> findAttribute(@NotNull PsiElement flowIdAttributeValue) {
        if (flowIdAttributeValue instanceof XmlAttributeValue) {
            XmlAttributeValue attributeValue = (XmlAttributeValue) flowIdAttributeValue;
            PsiElement parent = attributeValue.getParent();
            if (parent instanceof XmlAttribute) {
                return Optional.of((XmlAttribute) parent);
            }
        }
        return Optional.empty();
    }

    public interface FlowDefinitionConsumer extends Serializable {
        void withData(@NotNull String flowId, String route, String html);
    }

}
