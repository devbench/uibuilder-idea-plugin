package io.devbench.uibuilder.idea.util;

import com.intellij.facet.ProjectFacetManager;
import com.intellij.history.core.Paths;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ContentIterator;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.roots.impl.ProjectFileIndexFacade;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileFilter;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.indexing.FileContent;
import io.devbench.uibuilder.idea.facet.UIBuilderFacet;
import io.devbench.uibuilder.idea.facet.UIBuilderFacetConfiguration;
import io.devbench.uibuilder.idea.facet.UIBuilderFacetType;
import io.devbench.uibuilder.idea.index.scope.ExcludedSearchScope;
import org.jetbrains.annotations.NotNull;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public final class UIBuilderProjectUtils {

    private UIBuilderProjectUtils() {
        throw new UnsupportedOperationException();
    }

    public static boolean isInUIBuilderScope(@NotNull PsiElement psiElement) {
        PsiFile containingFile = psiElement.getContainingFile();
        if (containingFile != null && containingFile.getVirtualFile() != null) {
            return isInUIBuilderScope(psiElement.getProject(), containingFile.getVirtualFile());
        } else {
            return false;
        }
    }

    public static boolean isInUIBuilderScope(@NotNull FileContent fileContent) {
        return isInUIBuilderScope(fileContent.getProject(), fileContent.getFile());
    }

    public static boolean isInUIBuilderScope(@NotNull Project project, @NotNull VirtualFile file) {
        Module module = ProjectFileIndexFacade.getInstance(project).getModuleForFile(file);

        if (module == null) {
            return true;
        }

        String filePath = getRelativePathInModule(file, module);

        List<String> excludedPaths = ProjectFacetManager.getInstance(project)
            .getFacets(UIBuilderFacetType.UIBUILDER_FACET_TYPE_ID, new Module[]{module})
            .stream()
            .filter(uiBuilderFacet -> uiBuilderFacet.getModule().equals(module))
            .map(UIBuilderFacet::getConfiguration)
            .map(UIBuilderFacetConfiguration::ensureState)
            .map(UIBuilderFacetConfiguration.State::getExcludePaths)
            .findFirst()
            .orElse(Collections.emptyList());

        return excludedPaths.stream().filter(s -> !s.isBlank()).noneMatch(filePath::startsWith);
    }

    public static String getRelativePathInModule(@NotNull VirtualFile virtualFile, @NotNull Module module) {
        String basePath = Objects.requireNonNullElse(ModuleUtil.getModuleDirPath(module), "");
        String path = virtualFile.getPath();
        return StringUtil.trimLeading(
            path.startsWith(basePath) ? path.substring(basePath.length()) : path,
            Paths.DELIM);
    }

    public static GlobalSearchScope createSearchScope(@NotNull Project project) {
        return GlobalSearchScope.projectScope(project).intersectWith(new ExcludedSearchScope(project));
    }

    public static void iterateProjectFileIndexInUIBuilderScope(@NotNull Project project, @NotNull ContentIterator contentIterator,
                                                                  @NotNull VirtualFileFilter virtualFileFilter) {
        ProjectFileIndex
            .getInstance(project)
            .iterateContent(contentIterator, virtualFile -> isInUIBuilderScope(project, virtualFile) && virtualFileFilter.accept(virtualFile));
    }

    public static Comparator<PsiFile> inProjectFileComparator(@NotNull Project project) {
        return (file1, file2) -> {
            Boolean f1 = ProjectFileIndex.getInstance(project).isInContent(file1.getVirtualFile());
            Boolean f2 = ProjectFileIndex.getInstance(project).isInContent(file2.getVirtualFile());
            return f2.compareTo(f1);
        };
    }

}
