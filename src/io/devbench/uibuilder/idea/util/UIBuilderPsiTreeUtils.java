package io.devbench.uibuilder.idea.util;

import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlTag;
import io.devbench.uibuilder.idea.facet.UIBuilderFacetConfiguration;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;

public final class UIBuilderPsiTreeUtils {

    private UIBuilderPsiTreeUtils() {
        throw new UnsupportedOperationException();
    }

    @SuppressWarnings("unchecked")
    public static <T extends PsiElement> Optional<T> findDirectChildOfType(PsiElement element, Class<T> childType) {
        for (PsiElement child : element.getChildren()) {
            if (childType.isInstance(child)) {
                return (Optional<T>) Optional.of(child);
            }
        }
        return Optional.empty();
    }

    public static Optional<String> resolveReferenceExpression(PsiReferenceExpression referenceExpression) {
        PsiElement resolvedElement = referenceExpression.resolve();
        if (resolvedElement != null) {
            PsiLiteralExpression resolvedLiteralExpression = PsiTreeUtil.findChildOfType(resolvedElement, PsiLiteralExpression.class);
            if (resolvedLiteralExpression != null) {
                return Optional.ofNullable((String) resolvedLiteralExpression.getValue());
            }
        }
        return Optional.empty();
    }

    @NotNull
    public static Optional<String> getValueAnnotationValue(PsiAnnotation annotation) {
        return getValueAnnotationValue(annotation, false);
    }

    @NotNull
    public static Optional<String> getValueAnnotationValue(PsiAnnotation annotation, boolean forceResolveConstants) {
        Collection<PsiNameValuePair> nameValuePairs = PsiTreeUtil.findChildrenOfType(annotation, PsiNameValuePair.class);
        for (PsiNameValuePair nameValuePair : nameValuePairs) {
            PsiIdentifier identifier = findDirectChildOfType(nameValuePair, PsiIdentifier.class).orElse(null);
            PsiLiteralExpression literalExpression = PsiTreeUtil.findChildOfType(nameValuePair, PsiLiteralExpression.class);
            if (literalExpression != null && (identifier == null || "value".equals(identifier.getText()))) {
                return Optional.ofNullable((String) literalExpression.getValue());
            }
            if (UIBuilderUtils.findConfigurationState(annotation)
                .map(UIBuilderFacetConfiguration.State::isResolveConstantsEnabled)
                .orElse(false) || forceResolveConstants) {

                PsiReferenceExpression referenceExpression = PsiTreeUtil.findChildOfType(nameValuePair, PsiReferenceExpression.class);
                if (referenceExpression != null && (identifier == null || "value".equals(identifier.getText()))) {
                    return resolveReferenceExpression(referenceExpression);
                }
            }
        }
        return Optional.empty();
    }

    @NotNull
    public static Optional<String> getNameAnnotationValue(PsiAnnotation annotation) {
        return getNameAnnotationValue(annotation, false);
    }

    @NotNull
    public static Optional<String> getNameAnnotationValue(PsiAnnotation annotation, boolean forceResolveConstants) {
        Collection<PsiNameValuePair> nameValuePairs = PsiTreeUtil.findChildrenOfType(annotation, PsiNameValuePair.class);
        for (PsiNameValuePair nameValuePair : nameValuePairs) {
            PsiIdentifier identifier = findDirectChildOfType(nameValuePair, PsiIdentifier.class).orElse(null);
            if (identifier != null) {
                PsiLiteralExpression literalExpression = PsiTreeUtil.findChildOfType(nameValuePair, PsiLiteralExpression.class);
                if (literalExpression != null && "name".equals(identifier.getText())) {
                    return Optional.ofNullable((String) literalExpression.getValue());
                }
                if (UIBuilderUtils.findConfigurationState(annotation)
                    .map(UIBuilderFacetConfiguration.State::isResolveConstantsEnabled)
                    .orElse(false) || forceResolveConstants) {

                    PsiReferenceExpression referenceExpression = PsiTreeUtil.findChildOfType(nameValuePair, PsiReferenceExpression.class);
                    if (referenceExpression != null && "name".equals(identifier.getText())) {
                        return resolveReferenceExpression(referenceExpression);
                    }
                }
            }
        }
        return Optional.empty();
    }

    @SafeVarargs
    public static <PSI extends PsiElement> Optional<String> getPresentationText(PsiAnnotation annotation, Class<? extends PSI>... psiClasses) {
        for (Class<? extends PSI> psiClass : psiClasses) {
            PSI psi = PsiTreeUtil.getParentOfType(annotation, psiClass, true);
            if (psi != null) {
                PsiTypeElement psiType = PsiTreeUtil.getChildOfType(psi, PsiTypeElement.class);
                PsiIdentifier psiName = PsiTreeUtil.getChildOfType(psi, PsiIdentifier.class);

                if (psiType != null && psiName != null) {
                    return Optional.of(psiType.getText() + " " + psiName.getText() + getTrailingPresentationMarker(psi));
                }
            }
        }
        return Optional.empty();
    }

    private static String getTrailingPresentationMarker(PsiElement element) {
        if (element instanceof PsiMethod) {
            return "(...) {}";
        } else if (element instanceof PsiField) {
            return ";";
        }
        return "";
    }

    public static Optional<XmlTag> findMatchingParentTag(@NotNull PsiElement context, Predicate<XmlTag> predicate) {
        return findMatchingParentTag(context, predicate, tag -> false);
    }

    public static Optional<XmlTag> findMatchingParentTag(@NotNull PsiElement context,
                                                         Predicate<XmlTag> predicate,
                                                         Predicate<XmlTag> stopCriteria) {

        PsiElement parent = context.getParent();
        while (parent != null) {
            if (parent instanceof XmlTag) {
                XmlTag tag = (XmlTag) parent;
                if (stopCriteria.test(tag)) {
                    return Optional.empty();
                }
                if (predicate.test(tag)) {
                    return Optional.of(tag);
                }
            }
            parent = parent.getParent();
        }
        return Optional.empty();
    }

}
