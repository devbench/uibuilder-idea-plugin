package io.devbench.uibuilder.idea.util;

import com.intellij.facet.FacetManager;
import com.intellij.facet.ProjectFacetManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import io.devbench.uibuilder.idea.facet.UIBuilderFacet;
import io.devbench.uibuilder.idea.facet.UIBuilderFacetConfiguration;
import io.devbench.uibuilder.idea.facet.UIBuilderFacetType;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public final class UIBuilderUtils {

    private UIBuilderUtils() {
        throw new UnsupportedOperationException();
    }

    public static Optional<String> findUIBuilderVersion(@NotNull Module module) {
        return ProjectFacetManager.getInstance(module.getProject()).getFacets(UIBuilderFacetType.UIBUILDER_FACET_TYPE_ID).stream()
            .filter(uiBuilderFacet -> module.equals(uiBuilderFacet.getModule()))
            .map(UIBuilderFacet::getConfiguration)
            .map(UIBuilderFacetConfiguration::ensureState)
            .map(UIBuilderFacetConfiguration.State::getUibuilderVersion)
            .findFirst();
    }

    public static Optional<String> findUIBuilderVersion(@NotNull PsiElement element) {
        Project project = element.getProject();
        VirtualFile elementContainingFile = element.getContainingFile().getOriginalFile().getVirtualFile();
        if (elementContainingFile == null) {
            return Optional.empty();
        }
        Module module = ProjectFileIndex.getInstance(project).getModuleForFile(elementContainingFile);
        if (module != null) {
            return ProjectFacetManager.getInstance(project).getFacets(UIBuilderFacetType.UIBUILDER_FACET_TYPE_ID).stream()
                .filter(uiBuilderFacet -> module.equals(uiBuilderFacet.getModule()))
                .map(UIBuilderFacet::getConfiguration)
                .map(UIBuilderFacetConfiguration::ensureState)
                .map(UIBuilderFacetConfiguration.State::getUibuilderVersion)
                .findFirst();
        }
        return Optional.empty();
    }

    public static boolean isBowerVersion(@NotNull PsiElement element) {
        return findUIBuilderVersion(element).orElse("2.0.0").startsWith("1.");
    }

    public static Optional<UIBuilderFacetConfiguration.State> findConfigurationState(@NotNull PsiElement context) {
        PsiFile containingFile = context.getContainingFile();
        if (containingFile != null) {
            VirtualFile virtualFile = containingFile.getOriginalFile().getVirtualFile();
            if (virtualFile != null) {
                Module module = ProjectFileIndex.getInstance(context.getProject()).getModuleForFile(virtualFile);
                if (module != null) {
                    UIBuilderFacet uiBuilderFacet = FacetManager.getInstance(module).getFacetByType(UIBuilderFacetType.UIBUILDER_FACET_TYPE_ID);
                    if (uiBuilderFacet != null) {
                        return Optional.of(uiBuilderFacet.getConfiguration().ensureState());
                    }
                }
            }
        }

        return Optional.empty();
    }

}
