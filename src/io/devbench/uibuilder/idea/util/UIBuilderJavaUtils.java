package io.devbench.uibuilder.idea.util;

import com.intellij.framework.detection.FileContentPattern;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.patterns.PatternCondition;
import com.intellij.util.ProcessingContext;
import com.intellij.util.indexing.FileContent;
import com.intellij.util.indexing.FileContentImpl;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;

public final class UIBuilderJavaUtils {

    public static final FileContentPattern JAVA_FILE_CONTENT_PATTERN = FileContentPattern.fileContent()
        .with(new PatternCondition<>("uibuilderControllerBean") {
            @Override
            public boolean accepts(@NotNull final FileContent fileContent, final ProcessingContext context) {
                return fileContent.getContentAsText().toString().contains("@ControllerBean") ||
                    fileContent.getContentAsText().toString().contains("@UIEventHandler") ||
                    fileContent.getContentAsText().toString().contains("@UIComponent");
            }
        });

    private UIBuilderJavaUtils() {
        throw new UnsupportedOperationException();
    }

    public static boolean isUIBuilderJavaFile(@NotNull VirtualFile virtualFile) {
        try {
            return virtualFile.getFileType() == JavaFileType.INSTANCE && JAVA_FILE_CONTENT_PATTERN
                .accepts(FileContentImpl.createByFile(virtualFile));
        } catch (IOException e) {
            throw new RuntimeException("Could not get file content of " + virtualFile.getPath(), e);
        }
    }

}
