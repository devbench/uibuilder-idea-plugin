package io.devbench.uibuilder.idea.renderer;

import com.intellij.ide.util.PsiElementListCellRenderer;
import com.intellij.psi.PsiElement;
import com.intellij.psi.presentation.java.SymbolPresentationUtil;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlTag;
import org.jetbrains.annotations.Nullable;

public class WholeTagTextCellRenderer<T extends PsiElement> extends PsiElementListCellRenderer<T> {

    @Override
    public String getElementText(T element) {
        if (element instanceof XmlAttributeValue || element instanceof XmlAttribute) {
            XmlTag tag = PsiTreeUtil.getParentOfType(element, XmlTag.class);
            if (tag != null) {
                return tag.getText();
            }
        }
        return SymbolPresentationUtil.getSymbolPresentableText(element);
    }

    @Nullable
    @Override
    protected String getContainerText(T element, String s) {
        return SymbolPresentationUtil.getSymbolContainerText(element);
    }

    @Override
    protected int getIconFlags() {
        return 1;
    }
}
