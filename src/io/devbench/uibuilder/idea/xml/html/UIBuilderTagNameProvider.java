package io.devbench.uibuilder.idea.xml.html;

import com.intellij.codeInsight.completion.XmlTagInsertHandler;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.impl.source.xml.XmlElementDescriptorProvider;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.indexing.FileBasedIndex;
import com.intellij.xml.XmlElementDescriptor;
import com.intellij.xml.XmlTagNameProvider;
import io.devbench.uibuilder.idea.index.TagIndex;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class UIBuilderTagNameProvider implements XmlElementDescriptorProvider, XmlTagNameProvider {

    public static Optional<PsiElement> findUIBuilderSupportedTagDeclaration(@NotNull Project project, @NotNull String tagName) {
        PsiManager psiManager = PsiManager.getInstance(project);
        return FileBasedIndex.getInstance()
            .getContainingFiles(TagIndex.INDEX_NAME, tagName, GlobalSearchScope.allScope(project))
            .stream()
            .map(psiManager::findFile)
            .filter(Objects::nonNull)
            .map(PsiElement.class::cast)
            .findFirst();
    }

    @Nullable
    @Override
    public XmlElementDescriptor getDescriptor(XmlTag tag) {
        return findUIBuilderSupportedTagDeclaration(tag.getProject(), tag.getName())
            .map(declaration -> new UIBuilderSupportedElementDescriptor(tag, declaration))
            .orElse(null);
    }

    @Override
    public void addTagNameVariants(List<LookupElement> elements, @NotNull XmlTag tag, String prefix) {
        findUIBuilderSupportedTagDeclaration(tag.getProject(), tag.getName())
            .ifPresent(declaration -> elements.add(LookupElementBuilder
                .create(tag.getName())
                .withInsertHandler(XmlTagInsertHandler.INSTANCE)));
    }
}
