package io.devbench.uibuilder.idea.xml.html.descriptor;

import com.intellij.psi.xml.XmlTag;

public class UIBuilderComponentEventAttributeDescriptor extends UIBuilderTagAttributeDescriptor {

    public UIBuilderComponentEventAttributeDescriptor(XmlTag tag, String attributeName) {
        super(tag, attributeName);
    }

}
