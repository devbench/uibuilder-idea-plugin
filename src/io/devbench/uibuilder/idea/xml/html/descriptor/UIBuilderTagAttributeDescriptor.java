package io.devbench.uibuilder.idea.xml.html.descriptor;

import com.intellij.psi.xml.XmlTag;

public class UIBuilderTagAttributeDescriptor extends UIBuilderCommonAttributeDescriptor {

    private final XmlTag tag;

    public UIBuilderTagAttributeDescriptor(XmlTag tag, String attributeName) {
        super(attributeName);
        this.tag = tag;
    }

    public UIBuilderTagAttributeDescriptor(XmlTag tag, String attributeName, String... enumValues) {
        super(attributeName, enumValues);
        this.tag = tag;
    }

}
