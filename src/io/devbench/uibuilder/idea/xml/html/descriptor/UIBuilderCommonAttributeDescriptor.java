package io.devbench.uibuilder.idea.xml.html.descriptor;

import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlElement;
import com.intellij.xml.XmlAttributeDescriptor;
import org.jetbrains.annotations.Nullable;

public class UIBuilderCommonAttributeDescriptor implements XmlAttributeDescriptor {

    private final String[] enumValues;
    private final String attributeName;

    public UIBuilderCommonAttributeDescriptor(String attributeName) {
        this.attributeName = attributeName;
        this.enumValues = new String[0];
    }

    public UIBuilderCommonAttributeDescriptor(String attributeName, String... enumValues) {
        this.attributeName = attributeName;
        this.enumValues = enumValues;
    }

    @Override
    public boolean isRequired() {
        return false;
    }

    @Override
    public boolean isFixed() {
        return false;
    }

    @Override
    public boolean hasIdType() {
        return false;
    }

    @Override
    public boolean hasIdRefType() {
        return false;
    }

    @Nullable
    @Override
    public String getDefaultValue() {
        return null;
    }

    @Override
    public boolean isEnumerated() {
        return enumValues.length > 0;
    }

    @Nullable
    @Override
    public String[] getEnumeratedValues() {
        return enumValues;
    }

    @Nullable
    @Override
    public String validateValue(XmlElement context, String value) {
        return null;
    }

    @Override
    public PsiElement getDeclaration() {
        return null;
    }

    @Override
    public String getName(PsiElement context) {
        return attributeName;
    }

    @Override
    public String getName() {
        return attributeName;
    }

    @Override
    public void init(PsiElement element) {

    }
}
