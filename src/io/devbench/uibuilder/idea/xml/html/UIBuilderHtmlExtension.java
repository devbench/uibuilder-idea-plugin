package io.devbench.uibuilder.idea.xml.html;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.xml.HtmlXmlExtension;
import com.intellij.xml.util.XmlUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class UIBuilderHtmlExtension extends HtmlXmlExtension {

    @Override
    public boolean isAvailable(PsiFile file) {
        return super.isAvailable(file);
    }

    @NotNull
    @Override
    public List<TagInfo> getAvailableTagNames(@NotNull XmlFile file, @NotNull XmlTag context) {
        List<TagInfo> availableTagNames = new ArrayList<>();
        PsiElement parent = context.getParent();
        if (parent instanceof XmlTag && ((XmlTag) parent).getName().equals("uibuilder-common-filter")) {
            availableTagNames.add(createTagInfo("filter-types"));
        } else if (parent instanceof XmlTag && ((XmlTag) parent).getName().equals("filter-types")) {
            availableTagNames.add(createTagInfo("filter-type"));
        } else if (parent instanceof XmlTag && ((XmlTag) parent).getName().equals("uibuilder-rich-text-editor")) {
            availableTagNames.add(createTagInfo("theme-config"));
            availableTagNames.add(createTagInfo("toolbar-config"));
        } else {
            availableTagNames.add(createTagInfo("uibuilder-page"));
            availableTagNames.add(createTagInfo("uibuilder-form"));
            availableTagNames.add(createTagInfo("vaadin-button"));
            availableTagNames.add(createTagInfo("vaadin-text-field"));
            availableTagNames.add(createTagInfo("vaadin-text-area"));
            availableTagNames.add(createTagInfo("vaadin-date-picker"));
            availableTagNames.add(createTagInfo("vaadin-date-time-picker"));
            availableTagNames.add(createTagInfo("master-detail-controller"));
            availableTagNames.add(createTagInfo("detail-panel"));
            availableTagNames.add(createTagInfo("crud-panel"));
            availableTagNames.add(createTagInfo("crud-toolbar"));
            availableTagNames.add(createTagInfo("uibuilder-common-filter"));
            availableTagNames.add(createTagInfo("data-source"));
            availableTagNames.add(createTagInfo("item-data-source"));
            availableTagNames.add(createTagInfo("confirm-dialog"));
            availableTagNames.add(createTagInfo("error-handler-dialog"));
            availableTagNames.add(createTagInfo("vaadin-uibuilder-grid"));
            availableTagNames.add(createTagInfo("vaadin-grid-column"));
            availableTagNames.add(createTagInfo("vaadin-grid-tree-toggle"));
            availableTagNames.add(createTagInfo("vaadin-grid-sorter"));
            availableTagNames.add(createTagInfo("uibuilder-item-editable"));
            availableTagNames.add(createTagInfo("uibuilder-editor-window"));
            availableTagNames.add(createTagInfo("uibuilder-window"));
            availableTagNames.add(createTagInfo("uibuilder-tooltip"));
            availableTagNames.add(createTagInfo("uibuilder-checkbox"));
            availableTagNames.add(createTagInfo("uibuilder-combobox"));
            availableTagNames.add(createTagInfo("menu-bar"));
            availableTagNames.add(createTagInfo("menu-item"));
            availableTagNames.add(createTagInfo("uibuilder-login-panel"));
            availableTagNames.add(createTagInfo("hideable-layout"));
            availableTagNames.add(createTagInfo("uibuilder-multi-value"));
            availableTagNames.add(createTagInfo("uibuilder-rich-text-editor"));
            availableTagNames.add(createTagInfo("uibuilder-grid-doubleclick"));
            availableTagNames.addAll(super.getAvailableTagNames(file, context));
        }
        return availableTagNames;
    }

    private TagInfo createTagInfo(String tagName) {
        return new TagInfo(tagName, XmlUtil.HTML_URI);
    }
}
