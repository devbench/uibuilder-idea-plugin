package io.devbench.uibuilder.idea.xml.html;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.source.html.dtd.HtmlNSDescriptorImpl;
import com.intellij.psi.impl.source.xml.XmlDescriptorUtil;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.xml.XmlAttributeDescriptor;
import com.intellij.xml.XmlElementDescriptor;
import com.intellij.xml.XmlElementsGroup;
import com.intellij.xml.XmlNSDescriptor;
import io.devbench.uibuilder.idea.xml.html.descriptor.UIBuilderCommonAttributeDescriptor;
import io.devbench.uibuilder.idea.xml.html.descriptor.UIBuilderTagAttributeDescriptor;
import io.devbench.uibuilder.idea.xml.html.descriptor.UIBuilderComponentEventAttributeDescriptor;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UIBuilderSupportedElementDescriptor implements XmlElementDescriptor {

    private final PsiElement declaration;
    private final XmlTag xmlTag;

    public UIBuilderSupportedElementDescriptor(XmlTag xmlTag, PsiElement declaration) {
        this.xmlTag = xmlTag;
        this.declaration = declaration;
    }

    public Project getProject() {
        return xmlTag.getProject();
    }

    @Override
    public String getQualifiedName() {
        return xmlTag.getName();
    }

    @Override
    public String getDefaultName() {
        return xmlTag.getName();
    }

    @Override
    public XmlElementDescriptor[] getElementsDescriptors(XmlTag context) {
        return XmlDescriptorUtil.getElementsDescriptors(context);
    }

    @Override
    public XmlElementDescriptor getElementDescriptor(XmlTag childTag, XmlTag contextTag) {
        return XmlDescriptorUtil.getElementDescriptor(childTag, contextTag);
    }

    private List<XmlAttributeDescriptor> createDefaultAttributeDescriptors(@Nullable XmlTag context) {
        ArrayList<XmlAttributeDescriptor> descriptors = new ArrayList<>(Arrays.asList(HtmlNSDescriptorImpl.getCommonAttributeDescriptors(context)));
        descriptors.add(new UIBuilderCommonAttributeDescriptor("id"));
        return descriptors;
    }

    private void addDisabledDescriptor(@NotNull XmlTag tag, @NotNull List<XmlAttributeDescriptor> descriptors) {
        descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "disabled", "true", "false"));
    }

    private void addControlDefaultDescriptors(@NotNull XmlTag tag, @NotNull List<XmlAttributeDescriptor> descriptors) {
        addDisabledDescriptor(tag, descriptors);
        addThemeWithDefaultsDescriptor(tag, descriptors, "theme");
        descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "label"));
    }

    private void addFieldDefaultDescriptors(@NotNull XmlTag tag, @NotNull List<XmlAttributeDescriptor> descriptors) {
        addControlDefaultDescriptors(tag, descriptors);
        descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "value"));
        descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "item-bind"));
        descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "readonly", "true", "false"));
    }
    private void addThemeWithDefaultsDescriptor(@NotNull XmlTag tag, @NotNull List<XmlAttributeDescriptor> descriptors, @NotNull String attributeName) {
        descriptors.add(new UIBuilderTagAttributeDescriptor(tag, attributeName,
            "small", "icon", "primary", "secondary", "tertiary", "contrast", "success", "error", "warning"));
    }

    @Override
    public XmlAttributeDescriptor[] getAttributesDescriptors(@Nullable XmlTag context) {
        XmlTag tag = context != null ? context : xmlTag;
        List<XmlAttributeDescriptor> descriptors = createDefaultAttributeDescriptors(context);

        if (tag.getName().equals("uibuilder-page")) {
            addDisabledDescriptor(tag, descriptors);
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "contextId"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-before-load"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-after-load"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("vaadin-text-field") || tag.getName().equals("vaadin-text-area")) {
            addFieldDefaultDescriptors(tag, descriptors);
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "placeholder"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("uibuilder-rich-text-editor")) {
            addFieldDefaultDescriptors(tag, descriptors);
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "value"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "value-mode", "plain", "html", "ops", "formatted"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "html-render-mode", "frontend", "backend"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "formatter", "HTML", "Markdown"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "formatter", "HTML", "Markdown"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("theme-config")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "theme", "snow", "bubble"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("toolbar-config")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "mode", "no", "minimal", "default", "extended", "full", "custom"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "custom"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("vaadin-button")) {
            addControlDefaultDescriptors(tag, descriptors);
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-click"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "form-control", "save", "reset", "cancel"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("uibuilder-window")) {
            addControlDefaultDescriptors(tag, descriptors);
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "opened", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "closeable", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "header-text"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "width"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "height"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "resize", "horizontal", "vertical", "both", "none", "inherit", "initial"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("crud-panel")) {
            addDisabledDescriptor(tag, descriptors);
            addThemeWithDefaultsDescriptor(tag, descriptors, "theme");
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "manage-nested", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "has-separator", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "show-border", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "controller-bean"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "master-percent"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "width"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "height"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "orientation", "horizontal", "vertical"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-save"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-delete"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-refresh"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-create"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-edit"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-reset"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-cancel"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-item-selected"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "item-supplier"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("crud-toolbar")) {
            addDisabledDescriptor(tag, descriptors);
            addThemeWithDefaultsDescriptor(tag, descriptors, "theme");
            addThemeWithDefaultsDescriptor(tag, descriptors, "all-theme");
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "icons-only", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "has-separator", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "master-detail-controller"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("master-detail-controller")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "master"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "detail"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("detail-panel")) {
            addDisabledDescriptor(tag, descriptors);
            addThemeWithDefaultsDescriptor(tag, descriptors, "theme");
            addThemeWithDefaultsDescriptor(tag, descriptors, "refresh-theme");
            addThemeWithDefaultsDescriptor(tag, descriptors, "create-theme");
            addThemeWithDefaultsDescriptor(tag, descriptors, "delete-theme");
            addThemeWithDefaultsDescriptor(tag, descriptors, "edit-theme");

            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "legend"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "readonly", "true", "false"));

            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "refresh-disabled", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "refresh-hidden", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "edit-disabled", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "edit-hidden", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "create-disabled", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "create-hidden", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "delete-disabled", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "delete-hidden", "true", "false"));

            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "hide-all-default-buttons", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "hide-default-refresh", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "hide-default-edit", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "hide-default-create", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "hide-default-delete", "true", "false"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("uibuilder-form")) {
            addDisabledDescriptor(tag ,descriptors);
            addThemeWithDefaultsDescriptor(tag, descriptors, "theme");
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "legend"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "readonly", "true", "false"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("data-source")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "name"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "datasource-id"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "default-query"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "hierarchical", "true", "false"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("item-data-source")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "name"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "datasource-id"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "items"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "hierarchical", "true", "false"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("uibuilder-common-filter")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "add-button-text"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "auto-search", "true", "false"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("filter-type")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "label"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "filter"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "type"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "value-component"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "validator"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "operators"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("vaadin-date-picker")) {
            addFieldDefaultDescriptors(tag, descriptors);
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("error-handler-dialog")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "header-text"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "error-message"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "opened", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "observed-event", "error"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("confirm-dialog")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "header-text"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "confirm-text"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "cancel-text"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "opened", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "observed-event", "save", "reset", "cancel", "delete", "edit"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("uibuilder-checkbox")) {
            addFieldDefaultDescriptors(tag, descriptors);
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "checked", "true", "false"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("vaadin-checkbox")) {
            addFieldDefaultDescriptors(tag, descriptors);
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "checked", "true", "false"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }

        if (tag.getName().equals("vaadin-uibuilder-grid")) {
            addDisabledDescriptor(tag, descriptors);
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "theme",
                "compact", "column-borders", "no-row-borders", "row-stripes", "no-border", "wrap-cell-content"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "selection-mode", "single", "multi", "none"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "item-bind"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-double-click"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "multi-sort", "true", "false"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("uibuilder-combobox")) {
            addDisabledDescriptor(tag, descriptors);
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "item-bind"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "item-label-path"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "on-item-changed"));
        }
        if (tag.getName().equals("vaadin-grid-sorter")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "path"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }
        if (tag.getName().equals("uibuilder-grid-doubleclick")) {
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "prevent-single-click", "true", "false"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "timeout"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }

        if (tag.getName().equals("uibuilder-multi-value")) {
            addFieldDefaultDescriptors(tag, descriptors);
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "add-button-theme"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "remove-button-theme"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "add-button-hidden"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "remove-button-hidden"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "item-bind"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "item-supplier"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "value-item-path"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "min-no-values"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "max-no-values"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "max-no-values"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "as"));
            descriptors.add(new UIBuilderTagAttributeDescriptor(tag, "value-string"));
            return descriptors.toArray(new XmlAttributeDescriptor[0]);
        }

        return HtmlNSDescriptorImpl.getCommonAttributeDescriptors(context);
    }

    @Nullable
    @Override
    public XmlAttributeDescriptor getAttributeDescriptor(XmlAttribute attribute) {
        return getAttributeDescriptor(attribute.getName(), attribute.getParent());
    }

    @Nullable
    @Override
    public XmlAttributeDescriptor getAttributeDescriptor(@NonNls final String attributeName, @Nullable XmlTag context) {
        XmlAttributeDescriptor xmlAttributeDescriptor = ContainerUtil.find(getAttributesDescriptors(context),
            descriptor1 -> attributeName.equals(descriptor1.getName()));
        if (xmlAttributeDescriptor == null) {
            if (attributeName.startsWith("on-")) {
                XmlTag tag = context != null ? context : xmlTag;
                return new UIBuilderComponentEventAttributeDescriptor(tag, attributeName);
            } else if (attributeName.startsWith("item-bind")) {
                XmlTag tag = context != null ? context : xmlTag;
                return new UIBuilderComponentEventAttributeDescriptor(tag, attributeName);
            }
        }
        return xmlAttributeDescriptor;
    }

    @Override
    public XmlNSDescriptor getNSDescriptor() {
        return null;
    }

    @Nullable
    @Override
    public XmlElementsGroup getTopGroup() {
        return null;
    }

    @Override
    public int getContentType() {
        return CONTENT_TYPE_ANY;
    }

    @Nullable
    @Override
    public String getDefaultValue() {
        return null;
    }

    @Override
    public PsiElement getDeclaration() {
        return declaration;
    }

    @Override
    public String getName(PsiElement context) {
        return getName();
    }

    @Override
    public String getName() {
        return xmlTag.getName();
    }

    @Override
    public void init(PsiElement element) {
        System.out.println("INIT: " + element);
    }
}
