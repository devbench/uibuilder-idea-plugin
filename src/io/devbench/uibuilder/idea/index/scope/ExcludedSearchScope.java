package io.devbench.uibuilder.idea.index.scope;

import com.intellij.facet.Facet;
import com.intellij.facet.ProjectFacetManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.search.GlobalSearchScope;
import io.devbench.uibuilder.idea.facet.UIBuilderFacetType;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ExcludedSearchScope extends GlobalSearchScope {

    public ExcludedSearchScope(@Nullable Project project) {
        super(project);
    }

    @Override
    public boolean isSearchInModuleContent(@NotNull Module module) {
        return ProjectFacetManager
            .getInstance(module.getProject())
            .getFacets(UIBuilderFacetType.UIBUILDER_FACET_TYPE_ID, new Module[]{module})
            .stream()
            .map(Facet::getModule)
            .anyMatch(module::equals);
    }

    @Override
    public boolean isSearchInLibraries() {
        return false;
    }

    @Override
    public boolean contains(@NotNull VirtualFile virtualFile) {
        Project project = getProject();
        if (project != null) {
            Module module = ModuleUtil.findModuleForFile(virtualFile, project);
            if (module != null) {
                return UIBuilderProjectUtils.isInUIBuilderScope(project, virtualFile);
            }
            return true;
        }
        return false;
    }
}
