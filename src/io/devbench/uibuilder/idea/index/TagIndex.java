package io.devbench.uibuilder.idea.index;

import com.intellij.ide.highlighter.JavaClassFileType;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.util.indexing.*;
import com.intellij.util.io.EnumeratorStringDescriptor;
import com.intellij.util.io.KeyDescriptor;
import io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils;
import org.jetbrains.annotations.NotNull;
import java.util.HashMap;
import java.util.Map;

public class TagIndex extends ScalarIndexExtension<String> implements FileBasedIndex.InputFilter, DataIndexer<String, Void, FileContent> {

    public static final ID<String, Void> INDEX_NAME = ID.create("UIBuilderTagIndex");
    private final EnumeratorStringDescriptor keyDescriptor = new EnumeratorStringDescriptor();

    @NotNull
    @Override
    public ID<String, Void> getName() {
        return INDEX_NAME;
    }

    @NotNull
    @Override
    public FileBasedIndex.InputFilter getInputFilter() {
        return this;
    }


    @Override
    public boolean dependsOnFileContent() {
        return true;
    }

    @NotNull
    @Override
    public DataIndexer<String, Void, FileContent> getIndexer() {
        return this;
    }

    @NotNull
    @Override
    public KeyDescriptor<String> getKeyDescriptor() {
        return keyDescriptor;
    }

    @Override
    public int getVersion() {
        return 5;
    }

    @Override
    public boolean acceptInput(@NotNull VirtualFile file) {
        return JavaClassFileType.INSTANCE.equals(file.getFileType()) || JavaFileType.INSTANCE.equals(file.getFileType());
    }

    @NotNull
    @Override
    public Map<String, Void> map(@NotNull FileContent inputData) {
        PsiManager psiManager = PsiManager.getInstance(inputData.getProject());
        Map<String, Void> componentIdMap = new HashMap<>();

        PsiFile psiFile = psiManager.findFile(inputData.getFile());
        if (psiFile != null) {
            psiFile.accept(new PsiRecursiveElementVisitor() {
                @Override
                public void visitElement(@NotNull PsiElement element) {
                    String tagName = null;
                    if (element instanceof PsiAnnotation) {
                        PsiAnnotation annotation = (PsiAnnotation) element;

                        if (JavaClassFileType.INSTANCE.equals(inputData.getFileType())) {
                            if ("com.vaadin.flow.component.Tag".equals(annotation.getQualifiedName())) {
                                tagName = UIBuilderPsiTreeUtils.getValueAnnotationValue(annotation, true).orElse(null);
                            }
                        } else {
                            PsiJavaCodeReferenceElement annotationNameReference = annotation.getNameReferenceElement();
                            if (annotationNameReference != null && "Tag".equals(annotationNameReference.getReferenceName())) {
                                tagName = UIBuilderPsiTreeUtils.getValueAnnotationValue(annotation).orElse(null);
                            }
                        }

                        if (tagName != null) {
                            // filter the div tag to it's specific class due to ambiguous usage of such tag name on multiple class
                            if ("div".equals(tagName) && !psiFile.getName().equals("Div.class")) {
                                tagName = null;
                            } else {
                                componentIdMap.put(tagName, null);
                            }
                        }
                    }
                    if (tagName == null) {
                        super.visitElement(element);
                    }
                }
            });
        }
        return componentIdMap;
    }

}
