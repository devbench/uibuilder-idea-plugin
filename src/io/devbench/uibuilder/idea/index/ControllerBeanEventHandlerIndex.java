package io.devbench.uibuilder.idea.index;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiRecursiveElementVisitor;
import com.intellij.util.indexing.*;
import com.intellij.util.io.DataExternalizer;
import com.intellij.util.io.EnumeratorStringDescriptor;
import com.intellij.util.io.KeyDescriptor;
import io.devbench.uibuilder.idea.index.externalizer.StringDataExternalizer;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.HashMap;
import java.util.Map;
import static io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils.*;

public class ControllerBeanEventHandlerIndex extends FileBasedIndexExtension<String, String> implements FileBasedIndex.InputFilter, DataIndexer<String, String, FileContent> {

    public static final ID<String, String> INDEX_NAME = ID.create("UIBuilderControllerBeanEventHandlerIndex");
    private final EnumeratorStringDescriptor keyDescriptor = new EnumeratorStringDescriptor();

    @NotNull
    @Override
    public ID<String, String> getName() {
        return INDEX_NAME;
    }

    @NotNull
    @Override
    public FileBasedIndex.InputFilter getInputFilter() {
        return this;
    }


    @Override
    public boolean dependsOnFileContent() {
        return true;
    }

    @NotNull
    @Override
    public DataIndexer<String, String, FileContent> getIndexer() {
        return this;
    }

    @NotNull
    @Override
    public KeyDescriptor<String> getKeyDescriptor() {
        return keyDescriptor;
    }

    @Override
    public int getVersion() {
        return 0;
    }

    @Override
    public boolean acceptInput(@NotNull VirtualFile file) {
        return JavaFileType.INSTANCE.equals(file.getFileType());
    }

    @NotNull
    @Override
    public Map<String, String> map(@NotNull FileContent inputData) {
        Map<String, String> componentIdMap = new HashMap<>();
        if (UIBuilderProjectUtils.isInUIBuilderScope(inputData)) {
            inputData.getPsiFile().accept(new PsiRecursiveElementVisitor() {
                @Override
                public void visitElement(@NotNull PsiElement element) {
                    if (element instanceof PsiAnnotation) {
                        PsiAnnotation annotation = (PsiAnnotation) element;
                        if (annotation.getText().contains("UIEventHandler")) {
                            getValueAnnotationValue(annotation)
                                .ifPresent(value -> componentIdMap.put(value, getPresentationText(annotation, PsiMethod.class).orElse("?")));
                        }
                    }
                    super.visitElement(element);
                }
            });
        }
        return componentIdMap;
    }

    @NotNull
    @Override
    public DataExternalizer<String> getValueExternalizer() {
        return new StringDataExternalizer();
    }
}
