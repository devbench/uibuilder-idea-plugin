package io.devbench.uibuilder.idea.index;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiRecursiveElementVisitor;
import com.intellij.util.indexing.*;
import com.intellij.util.io.EnumeratorStringDescriptor;
import com.intellij.util.io.KeyDescriptor;
import io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils;
import org.jetbrains.annotations.NotNull;
import java.util.HashMap;
import java.util.Map;

public class TargetDataSourceIndex extends ScalarIndexExtension<String> implements FileBasedIndex.InputFilter, DataIndexer<String, Void, FileContent> {

    public static final ID<String, Void> INDEX_NAME = ID.create("UIBuilderTargetDataSourceIndex");
    private final EnumeratorStringDescriptor keyDescriptor = new EnumeratorStringDescriptor();

    @NotNull
    @Override
    public ID<String, Void> getName() {
        return INDEX_NAME;
    }

    @NotNull
    @Override
    public FileBasedIndex.InputFilter getInputFilter() {
        return this;
    }


    @Override
    public boolean dependsOnFileContent() {
        return true;
    }

    @NotNull
    @Override
    public DataIndexer<String, Void, FileContent> getIndexer() {
        return this;
    }

    @NotNull
    @Override
    public KeyDescriptor<String> getKeyDescriptor() {
        return keyDescriptor;
    }

    @Override
    public int getVersion() {
        return 0;
    }

    @Override
    public boolean acceptInput(@NotNull VirtualFile file) {
        return JavaFileType.INSTANCE.equals(file.getFileType());
    }

    @NotNull
    @Override
    public Map<String, Void> map(@NotNull FileContent inputData) {
        Map<String, Void> componentIdMap = new HashMap<>();
        inputData.getPsiFile().accept(new PsiRecursiveElementVisitor() {
            @Override
            public void visitElement(@NotNull PsiElement element) {
                String targetDataSourceName = null;
                if (element instanceof PsiAnnotation) {
                    PsiAnnotation annotation = (PsiAnnotation) element;
                    if (annotation.getText().contains("@TargetDataSource")) {
                        targetDataSourceName = UIBuilderPsiTreeUtils.getNameAnnotationValue(annotation).orElse(null);
                        if (targetDataSourceName != null) {
                            componentIdMap.put(targetDataSourceName, null);
                        }
                    }
                }
                if (targetDataSourceName == null) {
                    super.visitElement(element);
                }
            }
        });
        return componentIdMap;
    }

}
