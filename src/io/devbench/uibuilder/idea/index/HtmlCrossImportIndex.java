package io.devbench.uibuilder.idea.index;

import com.intellij.ide.highlighter.HtmlFileType;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.indexing.*;
import com.intellij.util.io.DataExternalizer;
import com.intellij.util.io.EnumeratorStringDescriptor;
import com.intellij.util.io.KeyDescriptor;
import io.devbench.uibuilder.idea.index.externalizer.StringDataExternalizer;
import io.devbench.uibuilder.idea.util.UIBuilderWebjarUtils;
import org.jetbrains.annotations.NotNull;
import java.util.HashMap;
import java.util.Map;

public class HtmlCrossImportIndex extends FileBasedIndexExtension<String, String> implements FileBasedIndex.InputFilter, DataIndexer<String, String, FileContent> {

    public static final ID<String, String> INDEX_NAME = ID.create("UIBuilderHtmlCrossImportIndex");
    private final EnumeratorStringDescriptor keyDescriptor = new EnumeratorStringDescriptor();

    @NotNull
    @Override
    public ID<String, String> getName() {
        return INDEX_NAME;
    }

    @NotNull
    @Override
    public FileBasedIndex.InputFilter getInputFilter() {
        return this;
    }


    @Override
    public boolean dependsOnFileContent() {
        return true;
    }

    @NotNull
    @Override
    public DataIndexer<String, String, FileContent> getIndexer() {
        return this;
    }

    @NotNull
    @Override
    public KeyDescriptor<String> getKeyDescriptor() {
        return keyDescriptor;
    }

    @Override
    public int getVersion() {
        return 1;
    }

    @Override
    public boolean acceptInput(@NotNull VirtualFile file) {
        return HtmlFileType.INSTANCE.equals(file.getFileType());
    }

    @NotNull
    @Override
    public Map<String, String> map(@NotNull FileContent inputData) {
        Map<String, String> componentIdMap = new HashMap<>();
        UIBuilderWebjarUtils.stripWebjarPath(inputData.getFile().getPath())
            .ifPresent(importPath -> inputData.getPsiFile()
                .accept(new XmlRecursiveElementVisitor() {
                    @Override
                    public void visitXmlTag(XmlTag tag) {
                        if (tag.getName().equals("link")) {
                            UIBuilderWebjarUtils.findLinkTagHref(tag)
                                .ifPresent(linkHref -> {
                                    String resolvedLinkHref = UIBuilderWebjarUtils.resolveWebjarPath(importPath, linkHref);
                                    componentIdMap.put(resolvedLinkHref, importPath);
                                });
                        }
                        super.visitXmlTag(tag);
                    }
                }));
        return componentIdMap;
    }

    @NotNull
    @Override
    public DataExternalizer<String> getValueExternalizer() {
        return new StringDataExternalizer();
    }
}
