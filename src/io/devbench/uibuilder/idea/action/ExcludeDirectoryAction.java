package io.devbench.uibuilder.idea.action;

import com.intellij.facet.FacetManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.pom.Navigatable;
import com.intellij.psi.PsiDirectory;
import io.devbench.uibuilder.idea.facet.UIBuilderFacet;
import io.devbench.uibuilder.idea.facet.UIBuilderFacetConfiguration;
import io.devbench.uibuilder.idea.facet.UIBuilderFacetType;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;

public class ExcludeDirectoryAction extends AnAction {

    @Override
    public void update(@NotNull AnActionEvent anActionEvent) {
        boolean actionAvailable = false;
        Project project = anActionEvent.getProject();
        if (project != null) {
            Navigatable navigatable = anActionEvent.getDataContext().getData(CommonDataKeys.NAVIGATABLE);
            if (navigatable instanceof PsiDirectory) {
                VirtualFile virtualFile = ((PsiDirectory) navigatable).getVirtualFile();
                Module module = ModuleUtil.findModuleForFile(virtualFile, project);
                if (module != null) {
                    UIBuilderFacet facet = FacetManager.getInstance(module)
                        .getFacetByType(UIBuilderFacetType.UIBUILDER_FACET_TYPE_ID);
                    if (facet != null) {
                        actionAvailable = true;
                    }
                }
            }
        }
        anActionEvent.getPresentation().setEnabled(actionAvailable);
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent anActionEvent) {
        Project project = anActionEvent.getProject();
        if (project != null) {
            Navigatable navigatable = anActionEvent.getDataContext().getData(CommonDataKeys.NAVIGATABLE);
            if (navigatable instanceof PsiDirectory) {
                VirtualFile virtualFile = ((PsiDirectory) navigatable).getVirtualFile();
                Module module = ModuleUtil.findModuleForFile(virtualFile, project);
                if (module != null) {
                    UIBuilderFacet facet = FacetManager.getInstance(module)
                        .getFacetByType(UIBuilderFacetType.UIBUILDER_FACET_TYPE_ID);
                    if (facet != null) {
                        UIBuilderFacetConfiguration configuration = facet
                            .getConfiguration();
                        UIBuilderFacetConfiguration.State state = configuration.ensureState();

                        String relativePathInModule = UIBuilderProjectUtils.getRelativePathInModule(virtualFile, module);
                        List<String> excludePaths = new ArrayList<>(state.getExcludePaths());
                        if (excludePaths.stream().noneMatch(relativePathInModule::equals)) {
                            excludePaths.add(relativePathInModule);
                        }
                        state.setExcludePaths(excludePaths);

                        ApplicationManager.getApplication().invokeLater(() -> virtualFile.refresh(true, true));
                    }
                }
            }
        }
    }

}
