package io.devbench.uibuilder.idea.codeinsight;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import io.devbench.uibuilder.idea.references.UIEventHandlerReference;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;
import static io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils.*;

public class EventHandlerMethodLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    public String getName() {
        return "UIBuilder event handler method";
    }

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {
        if (element instanceof PsiAnnotation && UIBuilderProjectUtils.isInUIBuilderScope(element)) {
            PsiAnnotation annotation = (PsiAnnotation) element;
            if (annotation.hasQualifiedName(FQN_UI_EVENT_HANDLER_ANNOTATION)) {
                Optional<String> foundEventName = getValueAnnotationValue(annotation);
                if (foundEventName.isPresent()) {
                    String eventName = foundEventName.get();
                    Optional<String> foundEventReference = UIEventHandlerReference.findEventReference(eventName, annotation);
                    if (foundEventReference.isPresent()) {
                        String eventReference = foundEventReference.get();
                        List<XmlAttribute> attributes = UIEventHandlerReference.resolveEventReferenceXmlAttributes(eventReference, element.getProject());

                        if (!attributes.isEmpty()) {
                            NavigationGutterIconBuilder<PsiElement> builder =
                                NavigationGutterIconBuilder.create(UIBuilderIcons.Gutter.EventReference)
                                    .setTargets(attributes)
                                    .setTooltipText("Navigate to a event handler usage");

                            result.add(builder.createLineMarkerInfo(PsiTreeUtil.firstChild(annotation)));
                        }
                    }
                }
            }
        }
    }

}
