package io.devbench.uibuilder.idea.codeinsight;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.ResolveResult;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.references.FlowIdReference;
import io.devbench.uibuilder.idea.renderer.WholeTagTextCellRenderer;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class FlowIdReferenceLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    public String getName() {
        return "UIBuilder flow reference";
    }

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {
        if (element instanceof XmlAttributeValue && UIBuilderProjectUtils.isInUIBuilderScope(element)) {
            XmlAttributeValue attributeValue = (XmlAttributeValue) element;
            PsiElement attributeElement = attributeValue.getParent();
            if (attributeElement instanceof XmlAttribute) {
                XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
                if (attribute.getName().startsWith("id") && attribute.getParent().getName().equalsIgnoreCase("flow")) {
                    String flowId = attribute.getValue();
                    if (flowId != null) {
                        FlowIdReference flowIdReference = new FlowIdReference(attributeValue);
                        List<PsiElement> resolvedElements = Arrays.stream(flowIdReference.multiResolve(false))
                            .map(ResolveResult::getElement)
                            .collect(Collectors.toList());

                        if (!resolvedElements.isEmpty()) {
                            NavigationGutterIconBuilder<PsiElement> builder =
                                NavigationGutterIconBuilder.create(UIBuilderIcons.Gutter.FlowReference)
                                    .setTargets(resolvedElements)
                                    .setCellRenderer(new WholeTagTextCellRenderer<>())
                                    .setTooltipText("Navigate to flow usages");
                            result.add(builder.createLineMarkerInfo(PsiTreeUtil.firstChild(element)));
                        }
                    }
                }
            }
        }
    }

}
