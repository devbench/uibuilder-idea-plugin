package io.devbench.uibuilder.idea.codeinsight;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlTag;
import io.devbench.uibuilder.idea.references.HtmlBeanPropertyReferenceProvider;
import io.devbench.uibuilder.idea.references.facade.UIBuilderReferenceFacade;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public class WebPropertyBindingLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    public String getName() {
        return "UIBuilder property binding";
    }

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {
        if ((element instanceof XmlAttributeValue || element instanceof XmlTag)
            && UIBuilderProjectUtils.isInUIBuilderScope(element)) {

            UIBuilderReferenceFacade referenceFacade = new UIBuilderReferenceFacade(element);
            if (referenceFacade.isPropertyReference()) {
                HtmlBeanPropertyReferenceProvider
                    .buildPropertyReferences(referenceFacade)
                    .reduce((one, two) -> two)
                    .ifPresent(propertyReference -> {
                        PsiElement resolvedElement = propertyReference.resolve();
                        if (resolvedElement != null) {
                            NavigationGutterIconBuilder<PsiElement> builder =
                                NavigationGutterIconBuilder.create(UIBuilderIcons.Gutter.PropertyBinding)
                                    .setTargets(resolvedElement)
                                    .setTooltipText("Navigate to the bound property");
                            result.add(builder.createLineMarkerInfo(referenceFacade.findPsiElementChild(element)));
                        }
                    });
            }
        }
    }

}
