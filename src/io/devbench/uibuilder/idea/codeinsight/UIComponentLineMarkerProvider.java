package io.devbench.uibuilder.idea.codeinsight;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.references.UIComponentIdReference;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;
import static io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils.*;

public class UIComponentLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    public String getName() {
        return "UIBuilder component";
    }

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {
        if (element instanceof PsiAnnotation && UIBuilderProjectUtils.isInUIBuilderScope(element)) {
            PsiAnnotation annotation = (PsiAnnotation) element;
            if (annotation.hasQualifiedName(FQN_UI_COMPONENT_ANNNOTATION)) {
                Optional<String> foundComponentId = getValueAnnotationValue(annotation);
                if (foundComponentId.isPresent()) {
                    String componentId = foundComponentId.get();
                    List<XmlAttributeValue> xmlAttributeValues = UIComponentIdReference.resolveComponentIdAttributeValue(componentId, element.getProject());
                    if (!xmlAttributeValues.isEmpty()) {
                        NavigationGutterIconBuilder<PsiElement> builder =
                            NavigationGutterIconBuilder.create(UIBuilderIcons.Gutter.UIComponent)
                                .setTargets(xmlAttributeValues)
                                .setTooltipText("Navigate to a html component");

                        result.add(builder.createLineMarkerInfo(PsiTreeUtil.firstChild(annotation)));
                    }
                }
            }
        }
    }

}
