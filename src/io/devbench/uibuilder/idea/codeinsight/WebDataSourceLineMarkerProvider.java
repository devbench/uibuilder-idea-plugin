package io.devbench.uibuilder.idea.codeinsight;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import static io.devbench.uibuilder.idea.util.UIBuilderFormUtils.*;

public class WebDataSourceLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    public String getName() {
        return "UIBuilder web target datasource";
    }

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {
        if (element instanceof XmlAttributeValue && UIBuilderProjectUtils.isInUIBuilderScope(element)) {
            XmlAttributeValue attributeValue = (XmlAttributeValue) element;
            XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
            if ("data-source".equals(attribute.getParent().getName()) && "name".equals(attribute.getName())) {
                String dataSourceName = attribute.getValue();
                if (dataSourceName != null) {
                    resolveTargetDataSourceRepositoryClass(attribute.getValue(), element.getProject())
                        .ifPresent(psiClass -> {
                            NavigationGutterIconBuilder<PsiElement> builder =
                                NavigationGutterIconBuilder.create(UIBuilderIcons.Gutter.TargetDataSource)
                                    .setTargets(psiClass)
                                    .setTooltipText("Navigate to repository");
                            result.add(builder.createLineMarkerInfo(PsiTreeUtil.firstChild(element)));
                        });
                }
            }
        }
    }

}
