package io.devbench.uibuilder.idea.codeinsight;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.references.HtmlEventReference;
import io.devbench.uibuilder.idea.references.HtmlFlowDefinitionReference;
import io.devbench.uibuilder.idea.references.facade.UIBuilderEventReference;
import io.devbench.uibuilder.idea.references.facade.UIBuilderFlowReference;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

public class WebEventHandlerReferenceLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    public String getName() {
        return "UIBuilder web event handler method";
    }

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {
        if (element instanceof XmlAttributeValue && UIBuilderProjectUtils.isInUIBuilderScope(element)) {
            XmlAttributeValue attributeValue = (XmlAttributeValue) element;
            XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
            if (attribute.getName().startsWith("on-")) {
                String eventReferenceValue = attribute.getValue();
                if (eventReferenceValue != null) {
                    UIBuilderEventReference eventReference = UIBuilderEventReference.parse(eventReferenceValue);
                    if (eventReference != null) {
                        List<PsiMethod> methods = HtmlEventReference.resolveEventReferencedMethods(eventReference, element.getProject());
                        if (!methods.isEmpty()) {
                            NavigationGutterIconBuilder<PsiElement> builder =
                                NavigationGutterIconBuilder.create(UIBuilderIcons.Gutter.EventReference)
                                    .setTargets(methods)
                                    .setTooltipText("Navigate to a event handler method");
                            result.add(builder.createLineMarkerInfo(PsiTreeUtil.firstChild(element)));
                        }
                    } else {
                        UIBuilderFlowReference flowReference = UIBuilderFlowReference.parse(eventReferenceValue);
                        if (flowReference != null) {
                            XmlAttributeValue xmlAttributeValue =
                                HtmlFlowDefinitionReference.resolveFlowDefinitionAttributeValue(element.getProject(), flowReference.getFlowId());
                            if (xmlAttributeValue != null) {
                                NavigationGutterIconBuilder<PsiElement> builder =
                                    NavigationGutterIconBuilder.create(UIBuilderIcons.Gutter.FlowReference)
                                        .setTargets(xmlAttributeValue)
                                        .setTooltipText("Navigate to flow definition");
                                result.add(builder.createLineMarkerInfo(PsiTreeUtil.firstChild(element)));
                            }
                        }
                    }
                }
            }
        }
    }

}
