package io.devbench.uibuilder.idea.codeinsight;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlTag;
import io.devbench.uibuilder.idea.references.HtmlFormItemPropertyReferenceProvider;
import io.devbench.uibuilder.idea.references.facade.UIBuilderReferenceFacade;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;

public class WebFormPropertyBindingLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    public String getName() {
        return "UIBuilder form property binding";
    }

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {
        if (isFormProperty(element)) {
            UIBuilderReferenceFacade referenceFacade = new UIBuilderReferenceFacade(element, false);
            if (referenceFacade.isPropertyReference()) {
                HtmlFormItemPropertyReferenceProvider
                    .buildPropertyReferences(element, referenceFacade)
                    .reduce((one, two) -> two)
                    .ifPresent(propertyReference -> {
                        PsiElement resolvedElement = propertyReference.resolve();
                        if (resolvedElement != null) {
                            NavigationGutterIconBuilder<PsiElement> builder =
                                NavigationGutterIconBuilder.create(UIBuilderIcons.Gutter.FormPropertyBinding)
                                    .setTargets(resolvedElement)
                                    .setTooltipText("Navigate to the form property");
                            result.add(builder.createLineMarkerInfo(PsiTreeUtil.firstChild(element)));
                        }
                    });
            }
        }
    }

    private boolean isFormProperty(@NotNull PsiElement element) {
        boolean formProperty = false;
        if (element instanceof XmlAttributeValue) {
            XmlAttribute attribute = (XmlAttribute) element.getParent();
            String attributeName = attribute.getName();
            if ("item-bind".equals(attributeName)) {
                formProperty = true;
            } else {
                XmlTag tag = attribute.getParent();
                String tagName = tag.getName();
                if ("filter-type".equalsIgnoreCase(tagName) && "filter".equals(attributeName)) {
                    formProperty = true;
                } else if ("vaadin-grid-sorter".equals(tagName) && "path".equals(attributeName)) {
                    formProperty = true;
                } else if ("uibuilder-item-editable".equals(tagName) && "path".equals(attributeName)) {
                    formProperty = true;
                }
            }
        }
        return formProperty && UIBuilderProjectUtils.isInUIBuilderScope(element);
    }

}
