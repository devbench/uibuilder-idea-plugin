package io.devbench.uibuilder.idea.codeinsight;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.references.HtmlComponentIdReference;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import java.util.List;

public class WebComponentLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    public String getName() {
        return "UIBuilder web component";
    }

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {
        if (element instanceof XmlAttributeValue && UIBuilderProjectUtils.isInUIBuilderScope(element)) {
            XmlAttributeValue attributeValue = (XmlAttributeValue) element;
            XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
            if ("id".equals(attribute.getName())) {
                String componentId = attribute.getValue();
                if (componentId != null) {
                    List<PsiElement> psiElement = HtmlComponentIdReference.resolveComponentIdInjection(attribute.getValue(), element.getProject());

                    if (!psiElement.isEmpty()) {
                        NavigationGutterIconBuilder<PsiElement> builder =
                            NavigationGutterIconBuilder.create(UIBuilderIcons.Gutter.UIComponent)
                                .setTargets(psiElement)
                                .setTooltipText("Navigate to component injection");

                        result.add(builder.createLineMarkerInfo(PsiTreeUtil.firstChild(element)));
                    }
                }
            }
        }
    }

}
