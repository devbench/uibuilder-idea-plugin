package io.devbench.uibuilder.idea.codeinsight;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import io.devbench.uibuilder.idea.references.TargetDataSourceReference;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;
import static io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils.*;

public class TargetDataSourceRepositoryLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    public String getName() {
        return "UIBuilder target datasource";
    }

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {
        if (element instanceof PsiAnnotation && UIBuilderProjectUtils.isInUIBuilderScope(element)) {
            PsiAnnotation annotation = (PsiAnnotation) element;
            if (annotation.hasQualifiedName(FQN_TARGET_DATASOURCE_ANNOTATION)) {
                Optional<String> foundDataSourceName = getNameAnnotationValue(annotation);
                if (foundDataSourceName.isPresent()) {
                    String dataSourceName = foundDataSourceName.get();
                    List<XmlAttribute> attributes = TargetDataSourceReference.resolveDataSourceNameXmlAttributes(dataSourceName, element.getProject());
                    if (!attributes.isEmpty()) {
                        NavigationGutterIconBuilder<PsiElement> builder =
                            NavigationGutterIconBuilder.create(UIBuilderIcons.Gutter.TargetDataSource)
                                .setTargets(attributes)
                                .setTooltipText("Navigate to a data-source usage");
                        result.add(builder.createLineMarkerInfo(PsiTreeUtil.firstChild(annotation)));
                    }
                }
            }
        }
    }

}
