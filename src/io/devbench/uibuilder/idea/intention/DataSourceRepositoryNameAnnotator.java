package io.devbench.uibuilder.idea.intention;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.TargetDataSourceIndex;
import io.devbench.uibuilder.idea.intention.actions.CreateTargetDataSourceRepositoryIntentionAction;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;

public class DataSourceRepositoryNameAnnotator extends UIBuilderAnnotator {

    @Override
    protected void annotateElement(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        if (element instanceof XmlAttribute) {
            XmlAttribute attribute = (XmlAttribute) element;
            if (attribute.getParent().getName().equals("data-source") && attribute.getName().equals("name")) {
                String value = attribute.getValue();
                if (value != null) {
                    Project project = element.getProject();
                    GlobalSearchScope projectScope = UIBuilderProjectUtils.createSearchScope(project);
                    VirtualFile repositoryFile = FileBasedIndex.getInstance()
                        .getContainingFiles(TargetDataSourceIndex.INDEX_NAME, value, projectScope)
                        .stream().findFirst().orElse(null);

                    if (repositoryFile == null) {
                        XmlAttributeValue attributeValueElement = attribute.getValueElement();
                        if (attributeValueElement != null) {
                            holder.newAnnotation(HighlightSeverity.ERROR, "Missing repository: " + value)
                                .range(attributeValueElement.getTextRange())
                                .withFix(new CreateTargetDataSourceRepositoryIntentionAction(value))
                                .create();
                        }
                    }
                }
            }
        }
    }

}
