package io.devbench.uibuilder.idea.intention;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.HtmlDomModuleIndex;
import io.devbench.uibuilder.idea.intention.actions.AddTagLinkImportIntentionAction;
import io.devbench.uibuilder.idea.util.UIBuilderUtils;
import io.devbench.uibuilder.idea.util.UIBuilderWebjarUtils;
import org.jetbrains.annotations.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class UIBuilderTagLinkAnnotator extends UIBuilderAnnotator {

    @Override
    protected void annotateElement(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        if (!UIBuilderUtils.isBowerVersion(element)) {
            return;
        }

        if (element instanceof XmlTag) {
            XmlTag tag = (XmlTag) element;
            String tagName = tag.getName();
            if (!"uibuilder-page".equals(tagName) && tagName.contains("-")) {

                GlobalSearchScope searchScope = GlobalSearchScope.allScope(tag.getProject());
                List<String> importPaths = FileBasedIndex.getInstance().getValues(HtmlDomModuleIndex.INDEX_NAME, tagName, searchScope);

                if (!importPaths.isEmpty()) {
                    // collect importPaths importing tag related importPath
                    Set<String> tagValidImportHrefs = new HashSet<>();
                    importPaths.forEach(tagImportPath -> {
                        tagValidImportHrefs.add(UIBuilderWebjarUtils.FRONTEND_BOWER_COMPONENTS + tagImportPath);
                        UIBuilderWebjarUtils.collectImportPathsRecursively(tagImportPath, searchScope)
                            .stream()
                            .map(importPath -> UIBuilderWebjarUtils.FRONTEND_BOWER_COMPONENTS + importPath)
                            .forEach(tagValidImportHrefs::add);
                    });

                    for (String tagValidImportHref : tagValidImportHrefs) {
                        if (importReferencePresent(tag.getContainingFile(), tagValidImportHref)) {
                            return;
                        }
                    }

                    holder.newAnnotation(HighlightSeverity.ERROR, "Add missing import for " + tagName)
                        .range(tag)
                        .withFix(new AddTagLinkImportIntentionAction(tagName, UIBuilderWebjarUtils.FRONTEND_BOWER_COMPONENTS + importPaths.get(0)))
                        .create();
                } else {
                    holder.newAnnotation(HighlightSeverity.WARNING, "Could not find tag import reference")
                        .range(tag)
                        .create();
                }
            }
        }
    }

    private boolean importReferencePresent(@NotNull PsiFile containingFile, @NotNull String tagImportReference) {
        AtomicBoolean present = new AtomicBoolean(false);
        containingFile.accept(new XmlRecursiveElementVisitor() {
            @Override
            public void visitXmlTag(XmlTag tag) {
                if (tag.getName().equals("link")) {
                    UIBuilderWebjarUtils.findLinkTagHref(tag)
                        .filter(tagImportReference::equals)
                        .ifPresent(href -> present.set(true));
                }
                if (present.get()) {
                    return;
                }
                super.visitXmlTag(tag);
            }
        });
        return present.get();
    }

}
