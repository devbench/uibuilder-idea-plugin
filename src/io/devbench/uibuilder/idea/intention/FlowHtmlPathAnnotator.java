package io.devbench.uibuilder.idea.intention;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlTag;
import io.devbench.uibuilder.idea.intention.actions.FixFlowHtmlPathAction;
import io.devbench.uibuilder.idea.references.FlowHtmlReference;
import org.jetbrains.annotations.NotNull;

public class FlowHtmlPathAnnotator extends UIBuilderAnnotator {

    @Override
    protected void annotateElement(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        if (element instanceof XmlAttribute) {
            XmlAttribute attribute = (XmlAttribute) element;
            XmlAttributeValue attributeValue = attribute.getValueElement();
            if (isFlowHtmlAttribute(attribute) && attributeValue != null) {
                String htmlPath = attributeValue.getValue();
                String correctedHtmlPath = !htmlPath.trim().startsWith("/") ? "/" + htmlPath.trim() : htmlPath;
                Project project = attribute.getProject();
                PsiElement resolvedHtmlFile = FlowHtmlReference.resolveHtmlPath(correctedHtmlPath, project);
                if (resolvedHtmlFile == null) {
                    holder.newAnnotation(HighlightSeverity.ERROR, "Referenced HTML path does not exist")
                        .range(attributeValue)
                        .create();
                } else if (!htmlPath.trim().startsWith("/")) {
                    holder.newAnnotation(HighlightSeverity.WARNING, "Referenced HTML path must start with a slash")
                        .range(attributeValue)
                        .withFix(new FixFlowHtmlPathAction(attribute, correctedHtmlPath))
                        .create();
                }
            }
        }
    }

    private boolean isFlowHtmlAttribute(XmlAttribute attribute) {
        if (attribute.getParent() != null && "html".equals(attribute.getName()) && "flow".equals(attribute.getParent().getName())) {
            XmlTag tag = attribute.getParent();
            while (tag != null && !"flows".equals(tag.getName())) {
                tag = tag.getParentTag();
            }
            return tag != null;
        }
        return false;
    }

}
