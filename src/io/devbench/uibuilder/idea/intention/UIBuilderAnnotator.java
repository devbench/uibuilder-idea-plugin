package io.devbench.uibuilder.idea.intention;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.psi.PsiElement;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;

public abstract class UIBuilderAnnotator implements Annotator {

    @Override
    public void annotate(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        if (UIBuilderProjectUtils.isInUIBuilderScope(element)) {
            annotateElement(element, holder);
        }
    }

    protected abstract void annotateElement(@NotNull PsiElement element, @NotNull AnnotationHolder holder);

}
