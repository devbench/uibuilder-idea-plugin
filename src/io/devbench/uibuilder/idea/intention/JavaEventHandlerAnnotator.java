package io.devbench.uibuilder.idea.intention;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiModifier;
import com.intellij.psi.PsiModifierList;
import io.devbench.uibuilder.idea.intention.actions.MakeEventHandlerMethodPublicIntentionAction;
import io.devbench.uibuilder.idea.intention.actions.RemoveStaticFromEventHandlerMethodIntentionAction;
import io.devbench.uibuilder.idea.util.UIBuilderConstants;
import org.jetbrains.annotations.NotNull;

public class JavaEventHandlerAnnotator extends UIBuilderAnnotator {

    @Override
    protected void annotateElement(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        if (element instanceof PsiMethod) {
            PsiMethod method = (PsiMethod) element;
            if (method.hasAnnotation(UIBuilderConstants.FQN_UI_EVENT_HANDLER_ANNOTATION)) {
                PsiModifierList modifierList = method.getModifierList();
                if (!modifierList.hasExplicitModifier(PsiModifier.PUBLIC)) {
                    holder.newAnnotation(HighlightSeverity.ERROR, "Event handler method must be public")
                        .range(method.getTextRange())
                        .withFix(new MakeEventHandlerMethodPublicIntentionAction(method))
                        .create();
                }
                if (modifierList.hasExplicitModifier(PsiModifier.STATIC)) {
                    holder.newAnnotation(HighlightSeverity.ERROR, "Event handler method cannot be static")
                        .range(method.getTextRange())
                        .withFix(new RemoveStaticFromEventHandlerMethodIntentionAction(method))
                        .create();
                }
            }
        }
    }

}
