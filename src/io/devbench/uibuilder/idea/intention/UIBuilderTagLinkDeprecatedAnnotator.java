package io.devbench.uibuilder.idea.intention;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlTag;
import io.devbench.uibuilder.idea.intention.actions.RemoveAllTagLinkImportIntentionAction;
import io.devbench.uibuilder.idea.intention.actions.RemoveTagLinkImportIntentionAction;
import io.devbench.uibuilder.idea.util.UIBuilderUtils;
import org.jetbrains.annotations.NotNull;

public class UIBuilderTagLinkDeprecatedAnnotator extends UIBuilderAnnotator {

    @Override
    protected void annotateElement(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        if (UIBuilderUtils.isBowerVersion(element)) {
            return;
        }

        if (element instanceof XmlTag) {
            XmlTag tag = (XmlTag) element;
            if ("link".equals(tag.getName())) {
                XmlAttribute relAttribute = tag.getAttribute("rel");
                XmlAttribute hrefAttribute = tag.getAttribute("href");
                if (relAttribute != null
                    && relAttribute.getValue() != null
                    && "import".equals(relAttribute.getValue())
                    && hrefAttribute != null
                    && hrefAttribute.getValue() != null) {
                    holder.newAnnotation(HighlightSeverity.WARNING, "HTML imports are deprecated after UIBuilder version 2.0")
                        .range(tag)
                        .withFix(new RemoveTagLinkImportIntentionAction(hrefAttribute.getValue()))
                        .withFix(new RemoveAllTagLinkImportIntentionAction())
                        .create();
                }
            }
        }
    }

}
