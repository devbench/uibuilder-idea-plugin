package io.devbench.uibuilder.idea.intention.actions;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.XmlElementFactory;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddTagLinkImportIntentionAction extends BaseIntentionAction {

    private String importReference;

    public AddTagLinkImportIntentionAction(String tagName, String importReference) {
        this.importReference = importReference;
        setText("Add import reference for " + tagName);
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "UIBuilder";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        if (file instanceof XmlFile) {
            XmlFile xmlFile = (XmlFile) file;
            XmlTag linkTag = XmlElementFactory.getInstance(project).createTagFromText("<link rel=\"import\">");
            linkTag.setAttribute("href", importReference);

            List<XmlTag> links = Stream.of(xmlFile.getDocument().getChildren())
                .filter(XmlTag.class::isInstance)
                .map(XmlTag.class::cast)
                .filter(xmlTag -> "link".equals(xmlTag.getName()))
                .collect(Collectors.toList());
            PsiElement anchor = links.isEmpty() ? null : links.get(links.size() - 1);

            xmlFile.addAfter(linkTag, anchor);
        }
    }

}
