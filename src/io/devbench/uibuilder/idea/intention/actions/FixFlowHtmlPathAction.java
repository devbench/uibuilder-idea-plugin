package io.devbench.uibuilder.idea.intention.actions;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

public class FixFlowHtmlPathAction extends BaseIntentionAction {

    private XmlAttribute attribute;
    private String correctedHtmlPath;

    public FixFlowHtmlPathAction(XmlAttribute attribute, String correctedHtmlPath) {
        this.attribute = attribute;
        this.correctedHtmlPath = correctedHtmlPath;
        setText("Fix flow HTML path");
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "UIBuilder";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        attribute.setValue(correctedHtmlPath);
    }

}
