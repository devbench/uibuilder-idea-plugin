package io.devbench.uibuilder.idea.intention.actions;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class RemoveAllTagLinkImportIntentionAction extends BaseIntentionAction {

    public RemoveAllTagLinkImportIntentionAction() {
        setText("Remove all import references");
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "UIBuilder";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        if (file instanceof XmlFile) {
            XmlFile xmlFile = (XmlFile) file;

            List<PsiElement> importElements = new ArrayList<>();
            xmlFile.accept(new XmlRecursiveElementVisitor() {
                @Override
                public void visitXmlAttribute(XmlAttribute attribute) {
                    XmlTag tag = attribute.getParent();
                    XmlAttribute rel = tag.getAttribute("rel");
                    if ("href".equals(attribute.getName())
                        && "link".equals(tag.getName())
                        && rel != null
                        && "import".equals(rel.getValue())) {
                        importElements.add(tag);
                    }
                    super.visitXmlAttribute(attribute);
                }
            });

            ApplicationManager.getApplication().invokeLater(() -> {
                WriteCommandAction.writeCommandAction(project).withName("Remove all HTML import tags").run(() -> {
                    importElements.forEach(PsiElement::delete);
                });
            });
        }
    }

}
