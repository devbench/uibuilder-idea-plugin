package io.devbench.uibuilder.idea.intention.actions;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiKeyword;
import com.intellij.psi.PsiMethod;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class RemoveStaticFromEventHandlerMethodIntentionAction extends BaseIntentionAction {

    private PsiMethod method;

    public RemoveStaticFromEventHandlerMethodIntentionAction(PsiMethod method) {
        this.method = method;
        setText("Remove static modifier from the event handler method");
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "UIBuilder";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        Arrays.stream(method.getModifierList().getChildren())
            .filter(PsiKeyword.class::isInstance)
            .map(PsiKeyword.class::cast)
            .filter(psiKeyword -> psiKeyword.getText().equals("static"))
            .findFirst()
            .ifPresent(PsiElement::delete);
    }
}
