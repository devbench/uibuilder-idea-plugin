package io.devbench.uibuilder.idea.intention.actions;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.PsiNavigateUtil;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.stream.Stream;

public class CreateEventHandlerIntentionAction extends BaseIntentionAction {

    private VirtualFile conrtollerBeanFile;
    private String eventName;

    public CreateEventHandlerIntentionAction(VirtualFile controllerBeanFile, String eventName) {
        this.conrtollerBeanFile = controllerBeanFile;
        this.eventName = eventName;
        setText("Create event handler \"" + eventName + "\"");
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "UIBuilder";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        PsiJavaFile psiFile = (PsiJavaFile) PsiManager.getInstance(project).findFile(conrtollerBeanFile);
        if (psiFile != null) {
            PsiClass psiClass = psiFile.getClasses()[0];

            Stream.of("on", "do", "onEvent", "doEvent")
                .map(variant -> variant + StringUtil.capitalize(eventName))
                .filter(methodName -> isMethodNameCanBeUSed(psiClass, methodName))
                .findFirst()
                .ifPresent(methodName -> {
                    JavaPsiFacade javaPsiFacade = JavaPsiFacade.getInstance(project);
                    PsiMethod method = javaPsiFacade.getParserFacade().createMethodFromText("" +
                        "@UIEventHandler(\"" + eventName + "\")\n" +
                        "public void " + methodName + "() {\n" +
                        "   // TODO: method body\n" +
                        "}\n", psiClass);

                    psiClass.add(method);

                    PsiNavigateUtil.navigate(method, true);
                });
        }
    }

    private boolean isMethodNameCanBeUSed(PsiClass psiClass, String methodName) {
        return Arrays.stream(psiClass.getAllMethods()).noneMatch(psiMethod -> psiMethod.getName().equals(methodName));
    }
}
