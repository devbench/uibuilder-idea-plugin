package io.devbench.uibuilder.idea.intention.actions;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.atomic.AtomicReference;

public class RemoveTagLinkImportIntentionAction extends BaseIntentionAction {

    private String href;

    public RemoveTagLinkImportIntentionAction(String href) {
        this.href = href;
        setText("Remove import reference for " + href);
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "UIBuilder";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        if (file instanceof XmlFile) {
            XmlFile xmlFile = (XmlFile) file;

            AtomicReference<PsiElement> importElement = new AtomicReference<>(null);
            xmlFile.accept(new XmlRecursiveElementVisitor() {
                @Override
                public void visitXmlAttribute(XmlAttribute attribute) {
                    XmlTag tag = attribute.getParent();
                    XmlAttribute rel = tag.getAttribute("rel");
                    if ("href".equals(attribute.getName())
                        && href.equals(attribute.getValue())
                        && "link".equals(tag.getName())
                        && rel != null
                        && "import".equals(rel.getValue())) {
                        importElement.set(tag);
                    } else {
                        super.visitXmlAttribute(attribute);
                    }
                }
            });

            if (importElement.get() != null) {
                ApplicationManager.getApplication().invokeLater(() -> {
                    WriteCommandAction.writeCommandAction(project).withName("Remove HTML import tag").run(() -> {
                        importElement.get().delete();
                    });
                });
            }
        }
    }

}
