package io.devbench.uibuilder.idea.intention.actions;

import com.intellij.codeInsight.daemon.impl.quickfix.CreateClassKind;
import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.codeInsight.intention.impl.CreateClassDialog;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.impl.scopes.ModulesScope;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.roots.impl.ProjectFileIndexFacade;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.pom.Navigatable;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.CodeStyleManager;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.TargetDataSourceIndex;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.jps.model.java.JavaSourceRootType;

import java.util.*;

public class CreateTargetDataSourceRepositoryIntentionAction extends BaseIntentionAction {

    private String dataSourceName;

    public CreateTargetDataSourceRepositoryIntentionAction(@NotNull String dataSourceName) {
        this.dataSourceName = dataSourceName;
        setText("Create TargetDataSource repository for name: " + dataSourceName);
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "UIBuilder";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        String repositoryBeanClassName = StringUtil.capitalize(dataSourceName) + "Repository";
        Module module = ProjectFileIndexFacade.getInstance(project).getModuleForFile(file.getVirtualFile());
        String guessedRepositoryPackage = findRepositoryPackageByIndex(module)
            .orElseGet(() -> findRepositoryPackageByName(module)
                .orElse(""));

        CreateClassDialog createClassDialog = new CreateClassDialog(project, "Create target datasource repository",
            repositoryBeanClassName, guessedRepositoryPackage, CreateClassKind.INTERFACE, true, module);

        ApplicationManager.getApplication().invokeLater(() -> {
            if (createClassDialog.showAndGet()) {

                String selectedRepositoryClassName = createClassDialog.getClassName();
                PsiDirectory targetDirectory = createClassDialog.getTargetDirectory();

                GlobalSearchScope searchScope = GlobalSearchScope.allScope(project);
                PsiClass repositoryPsiClass = JavaPsiFacade.getInstance(project)
                    .findClass("org.springframework.data.jpa.repository.JpaRepository", searchScope);
                if (repositoryPsiClass == null) {
                    repositoryPsiClass = JavaPsiFacade.getInstance(project)
                        .findClass("org.springframework.data.ebean.repository.EbeanRepository", searchScope);
                }
                if (repositoryPsiClass == null) {
                    Messages.showWarningDialog(project, "Could not find suitable spring repository interface", "Repository");
                    return;
                }

                String repositoryClassQualifiedName = repositoryPsiClass.getQualifiedName();

                WriteCommandAction.writeCommandAction(project).withName("Create target datasource repository").run(() -> {
                    Map<String, String> templateParameters = new HashMap<>();
                    templateParameters.put("DS", dataSourceName);
                    templateParameters.put("REPO", repositoryClassQualifiedName);
                    templateParameters.put("ENTITY", "ENTITY_CLASS");
                    templateParameters.put("ID", "ID_TYPE");
                    PsiClass psiClass = JavaDirectoryService.getInstance().createClass(targetDirectory, selectedRepositoryClassName,
                        "TargetDataSourceRepository.java", false, templateParameters);

                    // createModifierAnnotation(project, psiClass, FQN_TARGET_DATASOURCE_ANNOTATION, dataSourceName);
                    // createModifierAnnotation(project, psiClass, FQN_SPRING_REPOSITORY_ANNOTATION, null);

                    // JavaPsiFacade javaPsiFacade = JavaPsiFacade.getInstance(project);
                    // PsiElementFactory elementFactory = javaPsiFacade.getElementFactory();
                    // PsiJavaCodeReferenceElement referenceElement = elementFactory
                    //     .createFQClassNameReferenceElement(repositoryClassQualifiedName + "<ENTITY, ID>", searchScope);
                    // psiClass.getExtendsList().add(referenceElement);

                    JavaCodeStyleManager javaCodeStyleManager = JavaCodeStyleManager.getInstance(project);
                    javaCodeStyleManager.shortenClassReferences(psiClass);
                    CodeStyleManager.getInstance(project).reformat(psiClass);

                    ((Navigatable) psiClass.getNavigationElement()).navigate(true);
                });
            }
        });
    }

    // TODO: abstract this -> CreateControllerBeanAndEventHandlerIntentionAction
    private Optional<String> findRepositoryPackageByName(Module module) {
        Project project = module.getProject();
        ProjectRootManager projectRootManager = ProjectRootManager.getInstance(project);
        PsiManager psiManager = PsiManager.getInstance(project);
        JavaDirectoryService javaDirectoryService = JavaDirectoryService.getInstance();

        List<String> possiblePackages = new ArrayList<>();
        List<VirtualFile> moduleSourceRoots = projectRootManager.getModuleSourceRoots(Collections.singleton(JavaSourceRootType.SOURCE));
        for (VirtualFile moduleSourceRoot : moduleSourceRoots) {
            VfsUtil.processFilesRecursively(moduleSourceRoot, virtualFile -> {
                if (virtualFile.isDirectory()) {
                    if (virtualFile.getName().toLowerCase().contains("repository")) {
                        PsiDirectory directory = psiManager.findDirectory(virtualFile);
                        if (directory != null) {
                            PsiPackage psiPackage = javaDirectoryService.getPackageInSources(directory);
                            if (psiPackage != null) {
                                possiblePackages.add(psiPackage.getQualifiedName());
                                return false;
                            }
                        }
                    }
                }
                return true;
            });
        }

        return possiblePackages.isEmpty() ? Optional.empty() : Optional.of(possiblePackages.get(0));
    }

    private Optional<String> findRepositoryPackageByIndex(Module module) {
        Project project = module.getProject();
        FileBasedIndex index = FileBasedIndex.getInstance();
        GlobalSearchScope moduleScope = ModulesScope.moduleScope(module);
        PsiManager psiManager = PsiManager.getInstance(project);
        JavaDirectoryService javaDirectoryService = JavaDirectoryService.getInstance();

        for (String targetDataSourceName : index.getAllKeys(TargetDataSourceIndex.INDEX_NAME, project)) {
            for (VirtualFile repositoryFile : index.getContainingFiles(TargetDataSourceIndex.INDEX_NAME, targetDataSourceName, moduleScope)) {
                VirtualFile parent = repositoryFile.getParent();
                if (parent.isDirectory()) {
                    PsiDirectory directory = psiManager.findDirectory(parent);
                    if (directory != null) {
                        PsiPackage psiPackage = javaDirectoryService.getPackageInSources(directory);
                        if (psiPackage != null) {
                            return Optional.of(psiPackage.getQualifiedName());
                        }
                    }
                }
            }
        }

        return Optional.empty();
    }

    private PsiAnnotation createModifierAnnotation(Project project, PsiModifierListOwner element, String fqdn, String literal) {
        PsiAnnotation annotation = null;
        PsiModifierList modifierList = element.getModifierList();
        if (modifierList != null) {
            annotation = element.getModifierList().addAnnotation(fqdn);
            PsiElementFactory elementFactory = JavaPsiFacade.getInstance(project).getElementFactory();
            PsiAnnotation tempAnnotation;
            if (literal != null) {
                tempAnnotation = elementFactory.createAnnotationFromText("@A(name = \"" + literal + "\")", null);
                annotation.setDeclaredAttributeValue("name", tempAnnotation.findDeclaredAttributeValue("name"));
            }
        }
        return annotation;
    }

}
