package io.devbench.uibuilder.idea.intention.actions;

import com.intellij.codeInsight.daemon.impl.quickfix.CreateClassKind;
import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.codeInsight.intention.impl.CreateClassDialog;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.impl.scopes.ModulesScope;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.roots.impl.ProjectFileIndexFacade;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.CodeStyleManager;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.references.facade.UIBuilderEventReference;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.jps.model.java.JavaSourceRootType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;

public class CreateControllerBeanAndEventHandlerIntentionAction extends BaseIntentionAction {

    private UIBuilderEventReference eventReference;

    public CreateControllerBeanAndEventHandlerIntentionAction(@NotNull UIBuilderEventReference eventReference) {
        this.eventReference = eventReference;
        setText("Create controller bean and event handler \"" + eventReference.getReference() + "\"");
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "UIBuilder";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        String controllerBeanClassName = StringUtil.capitalize(eventReference.getControllerBeanName()) + "Bean";
        Module module = ProjectFileIndexFacade.getInstance(project).getModuleForFile(file.getVirtualFile());
        String guessedControllerBeanPackage = findControllerBeanPackageByIndex(module)
            .orElseGet(() -> findControllerBeanPackageByName(module)
                .orElse(""));

        CreateClassDialog createClassDialog = new CreateClassDialog(project, "Create controller bean",
            controllerBeanClassName, guessedControllerBeanPackage, CreateClassKind.CLASS, true, module);

        ApplicationManager.getApplication().invokeLater(() -> {
            if (createClassDialog.showAndGet()) {

                String selectedControllerBeanClassName = createClassDialog.getClassName();
                PsiDirectory targetDirectory = createClassDialog.getTargetDirectory();

                WriteCommandAction.writeCommandAction(project).withName("Create controller bean with event handler").run(() -> {
                    JavaPsiFacade javaPsiFacade = JavaPsiFacade.getInstance(project);
                    PsiElementFactory elementFactory = javaPsiFacade.getElementFactory();

                    PsiClass psiClass = JavaDirectoryService.getInstance().createClass(targetDirectory, selectedControllerBeanClassName);
                    createModifierAnnotation(
                        project, psiClass, FQN_CONTROLLER_BEAN_ANNOTATION, eventReference.getControllerBeanName());

                    PsiMethod method = elementFactory.createMethodFromText(
                        "public void on" + StringUtil.capitalize(eventReference.getEventName()) + "() {\n" +
                            "   // TODO: method body\n" +
                            "}", psiClass);

                    createModifierAnnotation(
                        project, method, FQN_UI_EVENT_HANDLER_ANNOTATION, eventReference.getEventName());

                    psiClass.add(method);

                    JavaCodeStyleManager javaCodeStyleManager = JavaCodeStyleManager.getInstance(project);
                    javaCodeStyleManager.shortenClassReferences(psiClass);
                    CodeStyleManager.getInstance(project).reformat(psiClass);
                });
            }
        });
    }

    private Optional<String> findControllerBeanPackageByName(Module module) {
        Project project = module.getProject();
        ProjectRootManager projectRootManager = ProjectRootManager.getInstance(project);
        PsiManager psiManager = PsiManager.getInstance(project);
        JavaDirectoryService javaDirectoryService = JavaDirectoryService.getInstance();

        List<String> possiblePackages = new ArrayList<>();
        List<VirtualFile> moduleSourceRoots = projectRootManager.getModuleSourceRoots(Collections.singleton(JavaSourceRootType.SOURCE));
        for (VirtualFile moduleSourceRoot : moduleSourceRoots) {
            VfsUtil.processFilesRecursively(moduleSourceRoot, virtualFile -> {
                if (virtualFile.isDirectory()) {
                    if (virtualFile.getName().toLowerCase().contains("controller")) {
                        PsiDirectory directory = psiManager.findDirectory(virtualFile);
                        if (directory != null) {
                            PsiPackage psiPackage = javaDirectoryService.getPackageInSources(directory);
                            if (psiPackage != null) {
                                possiblePackages.add(psiPackage.getQualifiedName());
                                return false;
                            }
                        }
                    }
                }
                return true;
            });
        }

        return possiblePackages.isEmpty() ? Optional.empty() : Optional.of(possiblePackages.get(0));
    }

    private Optional<String> findControllerBeanPackageByIndex(Module module) {
        Project project = module.getProject();
        FileBasedIndex index = FileBasedIndex.getInstance();
        GlobalSearchScope moduleScope = ModulesScope.moduleScope(module);
        PsiManager psiManager = PsiManager.getInstance(project);
        JavaDirectoryService javaDirectoryService = JavaDirectoryService.getInstance();

        for (String controllerBeanName : index.getAllKeys(ControllerBeanIndex.INDEX_NAME, project)) {
            for (VirtualFile controllerBeanFile : index.getContainingFiles(ControllerBeanIndex.INDEX_NAME, controllerBeanName, moduleScope)) {
                VirtualFile parent = controllerBeanFile.getParent();
                if (parent.isDirectory()) {
                    PsiDirectory directory = psiManager.findDirectory(parent);
                    if (directory != null) {
                        PsiPackage psiPackage = javaDirectoryService.getPackageInSources(directory);
                        if (psiPackage != null) {
                            return Optional.of(psiPackage.getQualifiedName());
                        }
                    }
                }
            }
        }

        return Optional.empty();
    }

    private PsiAnnotation createModifierAnnotation(Project project, PsiModifierListOwner element, String fqdn, String literal) {
        PsiAnnotation annotation = null;
        PsiModifierList modifierList = element.getModifierList();
        if (modifierList != null) {
            annotation = element.getModifierList().addAnnotation(fqdn);
            PsiElementFactory elementFactory = JavaPsiFacade.getInstance(project).getElementFactory();
            PsiAnnotation tempAnnotation = elementFactory.createAnnotationFromText("@A(\"" + literal + "\")", null);
            annotation.setDeclaredAttributeValue(null, tempAnnotation.findDeclaredAttributeValue(null));
        }
        return annotation;
    }

}
