package io.devbench.uibuilder.idea.intention.actions;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class MakeEventHandlerMethodPublicIntentionAction extends BaseIntentionAction {

    private PsiMethod method;

    public MakeEventHandlerMethodPublicIntentionAction(PsiMethod method) {
        this.method = method;
        setText("Make method public");
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "UIBuilder";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        boolean isPublic = Arrays.stream(method.getModifierList().getChildren())
            .anyMatch(psiKeyword -> psiKeyword.getText().equals("public"));

        if (!isPublic) {
            Arrays.stream(method.getModifierList().getChildren())
                .filter(PsiKeyword.class::isInstance)
                .map(PsiKeyword.class::cast)
                .filter(psiKeyword -> psiKeyword.getText().equals("private") || psiKeyword.getText().equals("protected"))
                .findFirst()
                .ifPresent(PsiElement::delete);
            method.getModifierList().add(PsiElementFactory.getInstance(project).createKeyword("public"));
        }
    }
}
