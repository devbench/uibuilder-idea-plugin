package io.devbench.uibuilder.idea.intention;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanEventHandlerIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.intention.actions.CreateControllerBeanAndEventHandlerIntentionAction;
import io.devbench.uibuilder.idea.intention.actions.CreateEventHandlerIntentionAction;
import io.devbench.uibuilder.idea.references.HtmlEventReference;
import io.devbench.uibuilder.idea.references.facade.UIBuilderEventReference;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class HtmlEventHandlerAnnotator extends UIBuilderAnnotator {

    @Override
    protected void annotateElement(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        if (element instanceof XmlAttribute) {
            XmlAttribute attribute = (XmlAttribute) element;
            if (attribute.getName().startsWith("on-")) {
                String value = attribute.getValue();
                if (value != null) {
                    UIBuilderEventReference eventReference = UIBuilderEventReference.parse(value);
                    if (eventReference != null) {
                        Project project = element.getProject();
                        VirtualFile controllerBeanFile = getControllerBeanFile(project, eventReference.getControllerBeanName());

                        @SuppressWarnings("ConstantConditions")
                        TextRange elementTextRange = attribute.getValueElement().getTextRange();

                        if (controllerBeanFile != null) {
                            if (!isEventHandlerExistsInFile(project, controllerBeanFile, eventReference)) {
                                holder.newAnnotation(HighlightSeverity.ERROR, "Event handler is missing in controller bean: " + eventReference.getControllerBeanName())
                                    .range(new TextRange(elementTextRange.getStartOffset() + value.indexOf("::") + 3, elementTextRange.getEndOffset() - 1))
                                    .withFix(new CreateEventHandlerIntentionAction(controllerBeanFile, eventReference.getEventName()))
                                    .create();
                            }
                        } else {
                                holder.newAnnotation(HighlightSeverity.ERROR, "Controller bean and event handler is missing")
                                    .range(new TextRange(elementTextRange.getStartOffset() + 1, elementTextRange.getEndOffset() - 1))
                                    .withFix(new CreateControllerBeanAndEventHandlerIntentionAction(eventReference))
                                    .create();
                        }
                    }
                }
            }
        }
    }

    private VirtualFile getControllerBeanFile(Project project, String controllerBeanName) {
        return ControllerBeanIndex.getVirtualFileControllerBeanName(UIBuilderProjectUtils.createSearchScope(project), controllerBeanName);
    }

    private boolean isEventHandlerExistsInFile(Project project, VirtualFile controllerBeanFile, UIBuilderEventReference eventReference) {
        Map<String, String> data = FileBasedIndex.getInstance().getFileData(ControllerBeanEventHandlerIndex.INDEX_NAME, controllerBeanFile, project);
        if (!data.containsKey(eventReference.getEventName())) {
            return !HtmlEventReference.resolveEventReferencedMethods(eventReference, project).isEmpty();
        }
        return true;
    }
}
