package io.devbench.uibuilder.idea.references;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.index.HtmlEventIndex;
import io.devbench.uibuilder.idea.references.facade.UIBuilderEventReference;
import io.devbench.uibuilder.idea.references.visitor.XmlAttributeVisitor;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;

public class UIEventHandlerReference extends PsiPolyVariantReferenceBase<PsiElement> {

    public UIEventHandlerReference(@NotNull PsiElement psiElement) {
        // The UIEventHandler annotated method is only called if there is a frontend binding, it does not hurt if there is no such binding in any html
        super(psiElement, true);
    }

    public static List<XmlAttribute> resolveEventReferenceXmlAttributes(@NotNull String eventReference, Project project) {
        List<XmlAttribute> attributes = new ArrayList<>();
        PsiManager psiManager = PsiManager.getInstance(project);

        List<PsiFile> files = FileBasedIndex.getInstance()
            .getContainingFiles(HtmlEventIndex.INDEX_NAME, eventReference, UIBuilderProjectUtils.createSearchScope(project))
            .stream()
            .map(psiManager::findFile)
            .collect(Collectors.toList());

        for (PsiFile psiFile : files) {
            XmlAttributeVisitor visitor = new XmlAttributeVisitor("on-", eventReference, false, false);
            psiFile.accept(visitor);
            if (visitor.isFound()) {
                attributes.addAll(visitor.getFoundAttributes());
            }
        }

        return attributes;
    }

    public static Optional<PsiClass> findParentControllerBeanClass(@NotNull PsiElement context) {
        PsiClass psiClass = PsiTreeUtil.getParentOfType(context, PsiClass.class, true);
        if (psiClass != null) {
            if (psiClass.hasAnnotation(FQN_CONTROLLER_BEAN_ANNOTATION)) {
                return Optional.of(psiClass);
            } else {
                FileBasedIndex index = FileBasedIndex.getInstance();
                Project project = context.getProject();
                PsiManager psiManager = PsiManager.getInstance(project);
                for (String cbName : index.getAllKeys(ControllerBeanIndex.INDEX_NAME, project)) {
                    List<PsiJavaFile> cbPsiJavaFiles =
                        index.getContainingFiles(ControllerBeanIndex.INDEX_NAME, cbName, UIBuilderProjectUtils.createSearchScope(project))
                            .stream()
                            .map(psiManager::findFile)
                            .filter(Objects::nonNull)
                            .filter(psiFile -> psiFile.getFileType().equals(JavaFileType.INSTANCE))
                            .map(PsiJavaFile.class::cast)
                            .collect(Collectors.toList());

                    for (PsiJavaFile javaFile : cbPsiJavaFiles) {
                        for (PsiClass cbClass : javaFile.getClasses()) {
                            if (cbClass.hasAnnotation(FQN_CONTROLLER_BEAN_ANNOTATION) && cbClass.isInheritor(psiClass, true)) {
                                return Optional.of(cbClass);
                            }
                        }
                    }
                }
            }
        }
        return Optional.empty();
    }

    public static Optional<String> findEventReference(@NotNull String eventHandlerName, @NotNull PsiElement context) {
        return findParentControllerBeanClass(context)
            .filter(psiClass -> psiClass.hasAnnotation(FQN_CONTROLLER_BEAN_ANNOTATION))
            .map(psiClass -> psiClass.getAnnotation(FQN_CONTROLLER_BEAN_ANNOTATION))
            .map(UIBuilderPsiTreeUtils::getValueAnnotationValue)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(cbName -> UIBuilderEventReference.create(cbName, eventHandlerName).getReference());
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean incompleteCode) {
        return getEventReference()
            .map(eventReference -> resolveEventReferenceXmlAttributes(eventReference, myElement.getProject())
                .stream()
                .map(PsiElementResolveResult::new)
                .toArray(ResolveResult[]::new))
            .orElse(ResolveResult.EMPTY_ARRAY);
    }

    private Optional<String> getEventReference() {
        Object value = ((PsiLiteralValue) myElement).getValue();
        if (value instanceof String) {
            return findEventReference((String) value, myElement);
        }
        return Optional.empty();
    }

    @Override
    public PsiElement handleElementRename(@NotNull String newElementName) throws IncorrectOperationException {
        throw new IncorrectOperationException("Rename is not supported yet");
    }

}
