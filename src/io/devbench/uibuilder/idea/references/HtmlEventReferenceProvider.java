package io.devbench.uibuilder.idea.references;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.util.ProcessingContext;
import io.devbench.uibuilder.idea.references.facade.UIBuilderReferenceFacade;
import org.jetbrains.annotations.NotNull;

public class HtmlEventReferenceProvider extends UIBuilderPsiReferenceProvider {

    @NotNull
    @Override
    protected PsiReference[] getUIBuilderReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
        UIBuilderReferenceFacade referenceFacade = new UIBuilderReferenceFacade(element);
        if (referenceFacade.isEventReference()) {
            return new PsiReference[]{
                new HtmlControllerBeanReference(element, referenceFacade.getControllerBeanTextRange(), referenceFacade.getControllerBeanName()),
                new HtmlEventReference(element, referenceFacade.getEventTextRange(), referenceFacade.getEventReference())
            };
        } else if (referenceFacade.isFlowReference()) {
            return new PsiReference[]{
                new HtmlFlowDefinitionReference(element, referenceFacade.getFlowIdTextRange(), referenceFacade.getFlowReference())
            };
        }
        return PsiReference.EMPTY_ARRAY;
    }

}
