package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.util.UIBuilderFlowUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class FlowFallbackIdReference extends PsiReferenceBase<PsiElement> {

    public FlowFallbackIdReference(@NotNull PsiElement psiElement) {
        // the referenced flow ID has to exists, so this has to be a hard reference
        super(psiElement, false);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        Project project = myElement.getProject();
        String fallbackId = ((XmlAttributeValue) myElement).getValue();

        return UIBuilderFlowUtils.collectFlowIds(project, UIBuilderFlowUtils.findProjectRootFlowDefinitionXml(project)).entrySet().stream()
            .filter(flowIdPsiElement -> fallbackId.equals(flowIdPsiElement.getKey()))
            .map(Map.Entry::getValue)
            .findFirst()
            .orElse(null);
    }

}
