package io.devbench.uibuilder.idea.references;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.util.ProcessingContext;
import io.devbench.uibuilder.idea.references.facade.UIBuilderReferenceFacade;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class HtmlFormItemPropertyReferenceProvider extends UIBuilderPsiReferenceProvider {

    @NotNull
    @Override
    protected PsiReference[] getUIBuilderReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
        UIBuilderReferenceFacade referenceFacade = new UIBuilderReferenceFacade(element, false);
        if (referenceFacade.isPropertyReference()) {
            List<PsiReference> references = new ArrayList<>();
            buildPropertyReferences(element, referenceFacade).forEach(references::add);
            return references.toArray(new PsiReference[0]);
        }
        return PsiReference.EMPTY_ARRAY;
    }

    public static Stream<HtmlFormItemPropertyReference> buildPropertyReferences(@NotNull PsiElement element,
                                                                                @NotNull UIBuilderReferenceFacade referenceFacade) {
        Stream.Builder<HtmlFormItemPropertyReference> referenceStreamBuilder = Stream.builder();
        HtmlFormItemPropertyReference parentReference = null;
        for (int level = 0; level < referenceFacade.getPropertyPathCount(); level++) {
            HtmlFormItemPropertyReference propertyReference = new HtmlFormItemPropertyReference(
                element,
                referenceFacade.getPropertyPathTextRange(level),
                parentReference,
                referenceFacade.getPropertyPath(level));
            referenceStreamBuilder.add(propertyReference);
            parentReference = propertyReference;
        }
        return referenceStreamBuilder.build();
    }

}
