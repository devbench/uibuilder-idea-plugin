package io.devbench.uibuilder.idea.references.visitor;

import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.xml.XmlTag;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class XmlTagVisitor extends XmlRecursiveElementVisitor {

    private String targetName;
    private boolean strictTargetName;
    private boolean firstOnly;
    private List<XmlTag> foundTags;

    public XmlTagVisitor(@NotNull String targetName) {
        this(targetName, true);
    }

    public XmlTagVisitor(@NotNull String targetName, boolean strictTargetName) {
        this(targetName, strictTargetName, true);
    }

    public XmlTagVisitor(@NotNull String targetName, boolean strictTargetName, boolean firstOnly) {
        this.targetName = targetName;
        this.strictTargetName = strictTargetName;

        this.firstOnly = firstOnly;
        this.foundTags = new ArrayList<>();
    }

    private boolean targetNameMatch(@NotNull String name) {
        if (strictTargetName) {
            return name.equals(targetName);
        } else {
            return name.startsWith(targetName);
        }
    }

    @Override
    public void visitXmlTag(XmlTag tag) {
        if (targetNameMatch(tag.getName())) {
            foundTags.add(tag);
            if (!firstOnly) {
                super.visitXmlTag(tag);
            }
        } else {
            super.visitXmlTag(tag);
        }
    }

    public XmlTag getFoundTag() {
        return foundTags.get(0);
    }

    public List<XmlTag> getFoundTags() {
        return foundTags;
    }

    public boolean isFound() {
        return !foundTags.isEmpty();
    }
}
