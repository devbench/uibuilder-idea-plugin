package io.devbench.uibuilder.idea.references.visitor;

import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.xml.XmlAttribute;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class XmlAttributeVisitor extends XmlRecursiveElementVisitor {

    private String targetName;
    private String targetValue;
    private boolean strictTargetName;
    private boolean firstOnly;
    private List<XmlAttribute> foundAttributes;

    public XmlAttributeVisitor(@NotNull String targetName, @NotNull String targetValue) {
        this(targetName, targetValue, true);
    }

    public XmlAttributeVisitor(@NotNull String targetName, @NotNull String targetValue, boolean strictTargetName) {
        this(targetName, targetValue, strictTargetName, true);
    }

    public XmlAttributeVisitor(@NotNull String targetName, @NotNull String targetValue, boolean strictTargetName, boolean firstOnly) {
        this.targetValue = targetValue;
        this.targetName = targetName;
        this.strictTargetName = strictTargetName;

        this.firstOnly = firstOnly;
        this.foundAttributes = new ArrayList<>();
    }

    private boolean targetNameMatch(@NotNull String name) {
        if (strictTargetName) {
            return name.equals(targetName);
        } else {
            return name.startsWith(targetName);
        }
    }

    @Override
    public void visitXmlAttribute(XmlAttribute attribute) {
        if (targetNameMatch(attribute.getName()) && targetValue.equals(attribute.getValue())) {
            foundAttributes.add(attribute);
            if (!firstOnly) {
                super.visitXmlAttribute(attribute);
            }
        } else {
            super.visitXmlAttribute(attribute);
        }
    }

    public XmlAttribute getFoundAttribute() {
        return foundAttributes.get(0);
    }

    public List<XmlAttribute> getFoundAttributes() {
        return foundAttributes;
    }

    public boolean isFound() {
        return !foundAttributes.isEmpty();
    }
}
