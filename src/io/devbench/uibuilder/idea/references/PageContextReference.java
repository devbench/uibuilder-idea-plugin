package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.util.UIBuilderConstants;
import io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;

public class PageContextReference extends PsiPolyVariantReferenceBase<PsiElement> {

    public PageContextReference(@NotNull PsiElement psiElement) {
        // there should be a controller bean with the PageScope tag with the correct value
        super(psiElement, false);
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean incompleteCode) {
        String contextId = ((XmlAttributeValue) myElement).getValue();

        Project project = myElement.getProject();
        PsiManager psiManager = PsiManager.getInstance(project);
        GlobalSearchScope searchScope = GlobalSearchScope.projectScope(project);
        FileBasedIndex fileBasedIndex = FileBasedIndex.getInstance();
        return fileBasedIndex.getAllKeys(ControllerBeanIndex.INDEX_NAME, project).stream()
            .flatMap(indexKey -> fileBasedIndex.getContainingFiles(ControllerBeanIndex.INDEX_NAME, indexKey, searchScope).stream())
            .map(psiManager::findFile)
            .filter(Objects::nonNull)
            .filter(PsiJavaFile.class::isInstance)
            .map(PsiJavaFile.class::cast)
            .flatMap(psiJavaFile -> Arrays.stream(psiJavaFile.getClasses()))
            .filter(psiClass -> isPageScopedControllerBeanMatches(psiClass, contextId))
            .map(PsiElementResolveResult::new)
            .toArray(ResolveResult[]::new);
    }

    private boolean isPageScopedControllerBeanMatches(@NotNull PsiClass psiClass, @NotNull String contextId) {
        if (psiClass.hasAnnotation(UIBuilderConstants.FQN_PAGE_SCOPE_ANNOTATION)) {
            return UIBuilderPsiTreeUtils.getValueAnnotationValue(psiClass.getAnnotation(UIBuilderConstants.FQN_PAGE_SCOPE_ANNOTATION))
                .filter(contextId::equals)
                .isPresent();
        }
        return false;
    }

}
