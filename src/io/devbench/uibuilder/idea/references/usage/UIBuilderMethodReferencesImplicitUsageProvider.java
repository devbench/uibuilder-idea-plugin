package io.devbench.uibuilder.idea.references.usage;

import com.intellij.codeInsight.daemon.ImplicitUsageProvider;
import com.intellij.psi.*;
import com.intellij.psi.xml.XmlAttribute;
import io.devbench.uibuilder.idea.references.UIEventHandlerReference;
import io.devbench.uibuilder.idea.util.UIBuilderConstants;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import java.util.Optional;

public class UIBuilderMethodReferencesImplicitUsageProvider implements ImplicitUsageProvider {

    @Override
    public boolean isImplicitUsage(@NotNull PsiElement psiElement) {
        if (psiElement instanceof PsiMethod) {
            PsiMethod method = (PsiMethod) psiElement;
            if (method.hasAnnotation(UIBuilderConstants.FQN_UI_EVENT_HANDLER_ANNOTATION)) {
                PsiAnnotation eventHandlerAnnotation = method.getAnnotation(UIBuilderConstants.FQN_UI_EVENT_HANDLER_ANNOTATION);
                @SuppressWarnings("ConstantConditions")
                PsiAnnotationMemberValue value = eventHandlerAnnotation.findAttributeValue("value");
                if (value instanceof PsiLiteralExpression) {
                    String eventName = (String) ((PsiLiteralExpression) value).getValue();
                    Optional<String> eventReference = eventName == null ? Optional.empty() : UIEventHandlerReference.findEventReference(eventName, method);
                    if (eventReference.isPresent()) {
                        List<XmlAttribute> xmlAttributes =
                            UIEventHandlerReference.resolveEventReferenceXmlAttributes(eventReference.get(), psiElement.getProject());
                        return !xmlAttributes.isEmpty();
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean isImplicitRead(@NotNull PsiElement psiElement) {
        return false;
    }

    @Override
    public boolean isImplicitWrite(@NotNull PsiElement psiElement) {
        return false;
    }
}
