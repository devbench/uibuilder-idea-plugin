package io.devbench.uibuilder.idea.references.usage;

import com.intellij.codeInsight.daemon.ImplicitUsageProvider;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.search.ProjectScope;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.references.UIEventHandlerReference;
import io.devbench.uibuilder.idea.references.facade.UIBuilderPropertyReference;
import io.devbench.uibuilder.idea.references.search.UIBuilderBeanPropertyReferencesSearch;
import io.devbench.uibuilder.idea.util.UIBuilderConstants;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class UIBuilderBeanPropertyReferencesImplicitUsageProvider implements ImplicitUsageProvider {

    @Override
    public boolean isImplicitUsage(@NotNull PsiElement psiElement) {
        if (psiElement instanceof PsiField) {
            Optional<UIBuilderPropertyReference> propertyReference =
                UIEventHandlerReference.findParentControllerBeanClass(psiElement).flatMap(controllerBeanClass -> {
                    PsiAnnotation controllerBeanAnnotation = controllerBeanClass.getAnnotation(UIBuilderConstants.FQN_CONTROLLER_BEAN_ANNOTATION);
                    if (controllerBeanAnnotation != null) {
                        return ControllerBeanIndex
                            .getControllerBeanName(controllerBeanAnnotation)
                            .map(controllerBeanName -> UIBuilderPropertyReference.create(controllerBeanName, ((PsiField) psiElement).getName()));
                    }
                    return Optional.empty();
                });

            if (propertyReference.isPresent()) {
                AtomicBoolean used = new AtomicBoolean(false);
                UIBuilderBeanPropertyReferencesSearch.processSearch(
                    psiElement, ProjectScope.getProjectScope(psiElement.getProject()),
                    propertyReference.get(), psiReference -> {
                        used.set(true);
                        return false;
                    });
                return used.get();
            }
        }
        return false;
    }

    @Override
    public boolean isImplicitRead(@NotNull PsiElement psiElement) {
        return isImplicitUsage(psiElement);
    }

    @Override
    public boolean isImplicitWrite(@NotNull PsiElement psiElement) {
        return isImplicitUsage(psiElement);
    }
}
