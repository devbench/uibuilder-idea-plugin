package io.devbench.uibuilder.idea.references;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.util.ProcessingContext;
import io.devbench.uibuilder.idea.util.UIBuilderUtils;
import org.jetbrains.annotations.NotNull;

public class HtmlImportHrefReferenceProvider extends UIBuilderPsiReferenceProvider {

    @NotNull
    @Override
    protected PsiReference[] getUIBuilderReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
        if (UIBuilderUtils.isBowerVersion(element)) {
            return new PsiReference[]{new HtmlImportHrefReference(element)};
        }
        return new PsiReference[0];
    }

}
