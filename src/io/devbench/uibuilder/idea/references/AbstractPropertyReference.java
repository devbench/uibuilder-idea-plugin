package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.*;
import com.intellij.psi.search.GlobalSearchScope;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractPropertyReference extends PsiReferenceBase<PsiElement> {

    protected AbstractPropertyReference parentReference;
    protected String propertyName;

    public AbstractPropertyReference(@NotNull PsiElement element, TextRange rangeInElement, boolean soft,
                                     @Nullable AbstractPropertyReference parentReference, @NotNull String propertyName) {
        super(element, rangeInElement, soft);
        this.parentReference = parentReference;
        this.propertyName = propertyName;
    }

    @Nullable
    protected PsiClass resolveFromParentReference(Project project) {
        PsiClass memberPsiClass = null;
        PsiElement resolve = parentReference.resolve();
        if (resolve instanceof PsiField) {
            PsiType type = ((PsiField) resolve).getType();
            memberPsiClass = JavaPsiFacade.getInstance(project)
                .findClass(type.getCanonicalText(), GlobalSearchScope.allScope(project));
        } else if (resolve instanceof PsiMethod) {
            PsiType type = ((PsiMethod) resolve).getReturnType();
            if (type != null) {
                memberPsiClass = JavaPsiFacade.getInstance(project)
                    .findClass(type.getCanonicalText(), GlobalSearchScope.allScope(project));
            }
        }
        return memberPsiClass;
    }

    @Nullable
    protected PsiJvmMember resolveFieldOrGetter(PsiClass memberPsiClass) {
        if (memberPsiClass != null) {
            for (PsiField field : memberPsiClass.getAllFields()) {
                if (propertyName.equals(field.getName())) {
                    return field;
                }
            }

            String propertyGetter = "get" + StringUtil.capitalize(propertyName);
            String propertyGetterIs = "is" + StringUtil.capitalize(propertyName);
            for (PsiMethod method : memberPsiClass.getAllMethods()) {
                if (propertyGetter.equals(method.getName()) || propertyGetterIs.equals(method.getName())) {
                    return method;
                }
            }
        }

        return null;
    }
}
