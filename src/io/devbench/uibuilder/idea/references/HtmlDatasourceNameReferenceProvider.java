package io.devbench.uibuilder.idea.references;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

public class HtmlDatasourceNameReferenceProvider extends UIBuilderPsiReferenceProvider {

    @NotNull
    @Override
    protected PsiReference[] getUIBuilderReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
        return new PsiReference[]{new HtmlDatasourceNameReference(element)};
    }

}
