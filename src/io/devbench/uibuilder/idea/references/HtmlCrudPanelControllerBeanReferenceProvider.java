package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

public class HtmlCrudPanelControllerBeanReferenceProvider extends UIBuilderPsiReferenceProvider {

    @NotNull
    @Override
    protected PsiReference[] getUIBuilderReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
        if (element instanceof XmlAttributeValue) {
            if (element.getParent() instanceof XmlAttribute) {
                XmlAttribute attribute = (XmlAttribute) element.getParent();
                String value = attribute.getValue();
                if (value != null && !value.trim().isEmpty()) {
                    TextRange textRange = attribute.getValueTextRange();
                    return new PsiReference[]{new HtmlControllerBeanReference(element, textRange, value)};
                }
            }
        }
        return PsiReference.EMPTY_ARRAY;
    }

}
