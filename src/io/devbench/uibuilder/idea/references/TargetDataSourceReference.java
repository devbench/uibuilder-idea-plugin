package io.devbench.uibuilder.idea.references;

import com.intellij.ide.highlighter.HtmlFileType;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.IncorrectOperationException;
import io.devbench.uibuilder.idea.references.visitor.XmlTagVisitor;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TargetDataSourceReference extends PsiPolyVariantReferenceBase<PsiElement> {

    public TargetDataSourceReference(@NotNull PsiElement psiElement) {
        // The UIEventHandler annotated method is only called if there is a frontend binding, it does not hurt if there is no such binding in any html
        super(psiElement, true);
    }

    public static List<XmlAttribute> resolveDataSourceNameXmlAttributes(@NotNull String dataSourceName, Project project) {
        List<XmlAttribute> attributes = new ArrayList<>();
        PsiManager psiManager = PsiManager.getInstance(project);

        List<PsiFile> files = new ArrayList<>();
        UIBuilderProjectUtils.iterateProjectFileIndexInUIBuilderScope(project, htmlFile -> {
            PsiFile psiFile = psiManager.findFile(htmlFile);
            if (psiFile != null) {
                files.add(psiFile);
            }
            return true;
        }, file -> HtmlFileType.INSTANCE.equals(file.getFileType()));

        for (PsiFile psiFile : files) {
            XmlTagVisitor visitor = new XmlTagVisitor("data-source", true, false);
            psiFile.accept(visitor);
            visitor.getFoundTags()
                .forEach(xmlTag -> getMatchingDataSourceNameAttribute(xmlTag, dataSourceName)
                    .ifPresent(attributes::add));
        }

        return attributes;
    }

    private static Optional<XmlAttribute> getMatchingDataSourceNameAttribute(@NotNull XmlTag tag, @NotNull String dataSourceName) {
        XmlAttribute nameAttribute = tag.getAttribute("name");
        if (nameAttribute != null && dataSourceName.equals(nameAttribute.getValue())) {
            return Optional.of(nameAttribute);
        }
        return Optional.empty();
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean incompleteCode) {
        return getDataSourceName()
            .map(dataSourceName -> resolveDataSourceNameXmlAttributes(dataSourceName, myElement.getProject())
                .stream()
                .map(PsiElementResolveResult::new)
                .toArray(ResolveResult[]::new))
            .orElse(ResolveResult.EMPTY_ARRAY);
    }

    private Optional<String> getDataSourceName() {
        String dataSourceNameValue = (String) ((PsiLiteralValue) myElement).getValue();
        return Optional.ofNullable(dataSourceNameValue);
    }

    @Override
    public PsiElement handleElementRename(@NotNull String newElementName) throws IncorrectOperationException {
        throw new IncorrectOperationException("Rename is not supported yet");
    }

}
