package io.devbench.uibuilder.idea.references.search;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileTypes.FileTypeManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Computable;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiReference;
import com.intellij.psi.search.PsiSearchHelper;
import com.intellij.psi.search.SearchScope;
import com.intellij.psi.search.TextOccurenceProcessor;
import com.intellij.psi.search.UsageSearchContext;
import com.intellij.psi.search.searches.ReferencesSearch;
import com.intellij.util.Processor;
import com.intellij.util.QueryExecutor;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.references.HtmlBeanPropertyReference;
import io.devbench.uibuilder.idea.references.UIEventHandlerReference;
import io.devbench.uibuilder.idea.references.facade.UIBuilderPropertyReference;
import io.devbench.uibuilder.idea.util.UIBuilderConstants;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import static io.devbench.uibuilder.idea.references.search.UIBuilderReferencesSearch.*;

public class UIBuilderBeanPropertyReferencesSearch implements QueryExecutor<PsiReference, ReferencesSearch.SearchParameters> {

    public static boolean processSearch(@NotNull PsiElement target,
                                 @NotNull SearchScope originalSearchScope,
                                 @NotNull UIBuilderPropertyReference propertyReference,
                                 @NotNull Processor<? super PsiReference> consumer) {
        Project project = target.getProject();
        SearchScope searchScope =
            uiBuilderHtmlScopeFilesScope(project, originalSearchScope, FileTypeManager.getInstance().getStdFileType("HTML"));

        TextOccurenceProcessor processor = (element, offsetInElement) -> {
            final PsiReference[] refs = element.getReferences();
            for (PsiReference ref : refs) {
                if (ref instanceof HtmlBeanPropertyReference) {
                    if (ref.isReferenceTo(target)) {
                        return consumer.process(ref);
                    }
                }
            }
            return true;
        };

        return PsiSearchHelper
            .getInstance(project)
            .processElementsWithWord(processor, searchScope, propertyReference.getPropertyPathWithControllerBean(), UsageSearchContext.ANY, true);
    }

    @Override
    public boolean execute(ReferencesSearch.SearchParameters params, @NotNull Processor<? super PsiReference> consumer) {
        PsiElement target = params.getElementToSearch();
        Optional<UIBuilderPropertyReference> propertyReference = Optional.empty();

        if (target instanceof PsiField) {
            propertyReference = ApplicationManager.getApplication().runReadAction((Computable<Optional<UIBuilderPropertyReference>>) () -> {
                return UIEventHandlerReference.findParentControllerBeanClass(target).flatMap(controllerBeanClass -> {
                    PsiAnnotation controllerBeanAnnotation = controllerBeanClass.getAnnotation(UIBuilderConstants.FQN_CONTROLLER_BEAN_ANNOTATION);
                    if (controllerBeanAnnotation != null) {
                        return ControllerBeanIndex
                            .getControllerBeanName(controllerBeanAnnotation)
                            .map(controllerBeanName -> UIBuilderPropertyReference.create(controllerBeanName, ((PsiField) target).getName()));
                    }
                    return Optional.empty();
                });
            });
        }

        return propertyReference
            .map(text -> processSearch(target, params.getScopeDeterminedByUser(), text, consumer))
            .orElse(true);
    }
}
