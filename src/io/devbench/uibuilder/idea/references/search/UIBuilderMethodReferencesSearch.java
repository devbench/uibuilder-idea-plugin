package io.devbench.uibuilder.idea.references.search;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.util.Computable;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiReference;
import com.intellij.psi.search.searches.MethodReferencesSearch;
import com.intellij.util.Processor;
import com.intellij.util.QueryExecutor;
import io.devbench.uibuilder.idea.util.UIBuilderConstants;
import io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils;
import org.jetbrains.annotations.NotNull;
import java.util.Optional;
import static io.devbench.uibuilder.idea.references.search.UIBuilderReferencesSearch.*;

public class UIBuilderMethodReferencesSearch implements QueryExecutor<PsiReference, MethodReferencesSearch.SearchParameters> {

    @Override
    public boolean execute(MethodReferencesSearch.SearchParameters params, @NotNull Processor<? super PsiReference> consumer) {
        PsiMethod target = params.getMethod();
        return ApplicationManager
            .getApplication()
            .runReadAction((Computable<Optional<String>>) () -> {
                if (target.hasAnnotation(UIBuilderConstants.FQN_UI_EVENT_HANDLER_ANNOTATION)) {
                    PsiAnnotation eventHandlerAnnotation = target.getAnnotation(UIBuilderConstants.FQN_UI_EVENT_HANDLER_ANNOTATION);
                    return UIBuilderPsiTreeUtils.getValueAnnotationValue(eventHandlerAnnotation);
                }
                return Optional.empty();
            })
            .map(searchText -> processSearch(target, params.getScopeDeterminedByUser(), searchText, consumer))
            .orElse(true);
    }
}
