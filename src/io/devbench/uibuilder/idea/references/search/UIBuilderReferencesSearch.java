package io.devbench.uibuilder.idea.references.search;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.fileTypes.FileTypeManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Computable;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.*;
import com.intellij.psi.search.searches.ReferencesSearch;
import com.intellij.util.Processor;
import com.intellij.util.QueryExecutor;
import io.devbench.uibuilder.idea.util.UIBuilderConstants;
import io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class UIBuilderReferencesSearch implements QueryExecutor<PsiReference, ReferencesSearch.SearchParameters> {

    static SearchScope uiBuilderHtmlScopeFilesScope(@NotNull Project project, SearchScope scope, final @NotNull FileType type) {
        return scope.intersectWith(new GlobalSearchScope(project) {
            public boolean contains(@NotNull VirtualFile file) {
                return file.getFileType() == type;
            }

            public int compare(@NotNull VirtualFile file1, @NotNull VirtualFile file2) {
                return 0; // irrelevant
            }

            public boolean isSearchInModuleContent(@NotNull Module aModule) {
                return true;
            }

            public boolean isSearchInLibraries() {
                return false;
            }
        });
    }

    static boolean processSearch(@NotNull PsiElement target,
                                 @NotNull SearchScope originalSearchScope,
                                 @NotNull String searchText,
                                 @NotNull Processor<? super PsiReference> consumer) {
        if (searchText.trim().isEmpty()) {
            return true;
        }

        Project project = target.getProject();
        SearchScope searchScope =
            uiBuilderHtmlScopeFilesScope(project, originalSearchScope, FileTypeManager.getInstance().getStdFileType("HTML"));

        TextOccurenceProcessor processor = (element, offsetInElement) -> {
            final PsiReference[] refs = element.getReferences();
            for (PsiReference ref : refs) {
                if (ref.getRangeInElement().contains(offsetInElement)) {
                    if (ref.isReferenceTo(target)) {
                        return consumer.process(ref);
                    }
                }
            }
            return true;
        };

        return PsiSearchHelper
            .getInstance(project)
            .processElementsWithWord(processor, searchScope, searchText, UsageSearchContext.ANY, true);
    }

    @Override
    public boolean execute(ReferencesSearch.SearchParameters params, @NotNull Processor<? super PsiReference> consumer) {
        PsiElement target = params.getElementToSearch();
        Optional<String> searchText = Optional.empty();

        if (target instanceof PsiClass) {
            PsiClass targetClass = (PsiClass) target;
            searchText = ApplicationManager.getApplication()
                .runReadAction((Computable<Optional<String>>) () -> {
                    if (targetClass.hasAnnotation(UIBuilderConstants.FQN_CONTROLLER_BEAN_ANNOTATION)) {
                        PsiAnnotation controllerBeanAnnotation = targetClass.getAnnotation(UIBuilderConstants.FQN_CONTROLLER_BEAN_ANNOTATION);
                        return UIBuilderPsiTreeUtils.getValueAnnotationValue(controllerBeanAnnotation);
                    }
                    return Optional.empty();
                });
        } else if (target instanceof PsiField || target instanceof PsiParameter) {
            PsiJvmModifiersOwner targetVariable = (PsiJvmModifiersOwner) target;
            searchText = ApplicationManager.getApplication()
                .runReadAction((Computable<Optional<String>>) () -> {
                    if (targetVariable.hasAnnotation(UIBuilderConstants.FQN_UI_COMPONENT_ANNNOTATION)) {
                        PsiAnnotation controllerBeanAnnotation = targetVariable.getAnnotation(UIBuilderConstants.FQN_UI_COMPONENT_ANNNOTATION);
                        return UIBuilderPsiTreeUtils.getValueAnnotationValue(controllerBeanAnnotation);
                    }
                    return Optional.empty();
                });
        }

        return searchText
            .map(text -> processSearch(target, params.getScopeDeterminedByUser(), text, consumer))
            .orElse(true);
    }

}
