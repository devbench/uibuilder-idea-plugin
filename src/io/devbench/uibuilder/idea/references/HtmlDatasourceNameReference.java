package io.devbench.uibuilder.idea.references;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import static io.devbench.uibuilder.idea.util.UIBuilderFormUtils.*;

public class HtmlDatasourceNameReference extends PsiReferenceBase<PsiElement> {

    public HtmlDatasourceNameReference(@NotNull PsiElement psiElement) {
        // reference is hard, the tag must define a datasource name that exists
        super(psiElement, false);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        String attributeValue = ((XmlAttributeValue) myElement).getValue();
        if (attributeValue.trim().isEmpty()) {
            return null;
        }

        return resolveTargetDataSourceRepositoryClass(attributeValue, myElement.getProject()).orElse(null);
    }

    @Override
    public PsiElement handleElementRename(@NotNull String newElementName) throws IncorrectOperationException {
        throw new IncorrectOperationException("Rename is not supported yet");
    }

}
