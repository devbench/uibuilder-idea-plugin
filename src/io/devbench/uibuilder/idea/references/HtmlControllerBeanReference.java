package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.util.UIBuilderFormUtils;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.Optional;
import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;

public class HtmlControllerBeanReference extends PsiReferenceBase<PsiElement> {

    private final String controllerBeanName;

    public HtmlControllerBeanReference(@NotNull PsiElement psiElement, @NotNull TextRange rangeInElement, @NotNull String controllerBeanName) {
        // the controller bean reference defined in the html should have a class with the right @ControllerBean annotation
        super(psiElement, rangeInElement, false);
        this.controllerBeanName = controllerBeanName;
    }

    public static Optional<PsiClass> getControllerBeanClass(@NotNull Project project, @NotNull String controllerBeanName) {
        GlobalSearchScope projectScope = UIBuilderProjectUtils.createSearchScope(project);
        PsiManager psiManager = PsiManager.getInstance(project);

        VirtualFile controllerBeanFile = FileBasedIndex.getInstance()
            .getContainingFiles(ControllerBeanIndex.INDEX_NAME, controllerBeanName, projectScope)
            .stream().findFirst().orElse(null);

        if (controllerBeanFile != null) {
            PsiJavaFile file = (PsiJavaFile) psiManager.findFile(controllerBeanFile);
            if (file != null) {
                for (PsiClass psiClass : file.getClasses()) {
                    PsiAnnotation annotation = psiClass.getAnnotation(FQN_CONTROLLER_BEAN_ANNOTATION);
                    if (annotation != null) {
                        String value = UIBuilderPsiTreeUtils.getValueAnnotationValue(annotation).orElse(null);
                        if (controllerBeanName.equals(value)) {
                            return Optional.of(psiClass);
                        }
                    }
                }
            }
        }

        return Optional.empty();
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        if ("item".equals(controllerBeanName)) {
            return UIBuilderFormUtils.findItemPropertyClass(myElement).orElse(null);
        }
        return HtmlControllerBeanReference.getControllerBeanClass(myElement.getProject(), controllerBeanName).orElse(null);
    }

    @Override
    public PsiElement handleElementRename(@NotNull String newElementName) throws IncorrectOperationException {
        throw new IncorrectOperationException("Rename is not supported yet");
    }

}
