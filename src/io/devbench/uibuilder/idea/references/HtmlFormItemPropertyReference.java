package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.util.IncorrectOperationException;
import io.devbench.uibuilder.idea.util.UIBuilderFormUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.Optional;

public class HtmlFormItemPropertyReference extends AbstractPropertyReference {

    public HtmlFormItemPropertyReference(@NotNull PsiElement psiElement,
                                         @NotNull TextRange rangeInElement,
                                         @Nullable HtmlFormItemPropertyReference parentReference,
                                         @NotNull String propertyName) {
        // the controller bean property reference defined in the html should be a valid property in the controller bean class
        super(psiElement, rangeInElement, false, parentReference, propertyName);
    }

    public static Optional<PsiClass> findInitialMemberPsiClass(PsiElement element) {
        return UIBuilderFormUtils.findMasterItemPropertyClass(element)
            .or(() -> UIBuilderFormUtils.findMasterItemPropertyClassByMDC(element))
            .or(() -> UIBuilderFormUtils.findMasterItemPropertyClassByInjectedForm(element)
            .or(() -> UIBuilderFormUtils.findItemPropertyClass(element)));
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        PsiClass memberPsiClass;
        if (parentReference == null) {
            memberPsiClass = findInitialMemberPsiClass(myElement).orElse(null);
        } else {
            memberPsiClass = resolveFromParentReference(myElement.getProject());
        }
        return resolveFieldOrGetter(memberPsiClass);
    }

    @Override
    public PsiElement handleElementRename(@NotNull String newElementName) throws IncorrectOperationException {
        throw new IncorrectOperationException("Rename is not supported yet");
    }

}
