package io.devbench.uibuilder.idea.references;

import com.intellij.patterns.ElementPattern;
import com.intellij.patterns.PsiJavaPatterns;
import com.intellij.patterns.StandardPatterns;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiLiteralExpression;
import com.intellij.psi.PsiReferenceContributor;
import com.intellij.psi.PsiReferenceRegistrar;

public class UIBuilderJavaReferenceContributor extends PsiReferenceContributor {

    @Override
    public void registerReferenceProviders(PsiReferenceRegistrar registrar) {
        ElementPattern<PsiLiteralExpression> uiComponentPattern = PsiJavaPatterns
            .psiElement(PsiLiteralExpression.class)
            .withAncestor(4, PsiJavaPatterns.psiElement(PsiAnnotation.class)
                .withText(StandardPatterns.string().contains("@UIComponent")));
        registrar.registerReferenceProvider(uiComponentPattern, new UIComponentIdReferenceProvider());

        ElementPattern<PsiLiteralExpression> uiEventHandlerPattern = PsiJavaPatterns
            .psiElement(PsiLiteralExpression.class)
            .withAncestor(4, PsiJavaPatterns.psiElement(PsiAnnotation.class)
                .withText(StandardPatterns.string().contains("@UIEventHandler")));
        registrar.registerReferenceProvider(uiEventHandlerPattern, new UIEventHandlerReferenceProvider());

        ElementPattern<PsiLiteralExpression> targetDataSourcePattern = PsiJavaPatterns
            .psiElement(PsiLiteralExpression.class)
            .withAncestor(4, PsiJavaPatterns.psiElement(PsiAnnotation.class)
                .withText(StandardPatterns.string().contains("@TargetDataSource")));
        registrar.registerReferenceProvider(targetDataSourcePattern, new TargetDataSourceReferenceProvider());

        ElementPattern<PsiLiteralExpression> pageScopePattern = PsiJavaPatterns
            .psiElement(PsiLiteralExpression.class)
            .withAncestor(4, PsiJavaPatterns.psiElement(PsiAnnotation.class)
                .withText(StandardPatterns.string().contains("@PageScope")));
        registrar.registerReferenceProvider(pageScopePattern, new PageScopeReferenceProvider());
    }

}
