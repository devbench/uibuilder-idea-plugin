package io.devbench.uibuilder.idea.references;

import com.intellij.psi.*;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.references.facade.UIBuilderFlowReference;
import io.devbench.uibuilder.idea.util.UIBuilderHtmlUtils;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;

public class FlowIdReference extends PsiPolyVariantReferenceBase<PsiElement> {

    public FlowIdReference(@NotNull PsiElement psiElement) {
        // there may be a flow navigation reference in a html file, but it is not necessary, so this should be a soft reference
        super(psiElement, true);
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean incompleteCode) {
        String id = ((XmlAttributeValue) myElement).getValue();

        List<XmlAttributeValue> eventAttributeValues = new ArrayList<>();

        UIBuilderHtmlUtils.findInHtml(myElement.getProject(), new XmlRecursiveElementVisitor() {
            @Override
            public void visitXmlAttribute(XmlAttribute attribute) {
                String reference = attribute.getValue();
                if (attribute.getName().startsWith("on-")
                    && reference != null
                    && reference.startsWith("flow:")) {

                    UIBuilderFlowReference flowReference = UIBuilderFlowReference.parse(reference);
                    if (flowReference != null && flowReference.isFlowIdMatch(id)) {
                        eventAttributeValues.add(attribute.getValueElement());
                    }
                }
                super.visitXmlAttribute(attribute);
            }
        });

        return eventAttributeValues.stream()
            .map(PsiElementResolveResult::new)
            .toArray(ResolveResult[]::new);
    }

}
