package io.devbench.uibuilder.idea.references;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.util.ProcessingContext;
import io.devbench.uibuilder.idea.references.facade.UIBuilderReferenceFacade;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class HtmlBeanPropertyReferenceProvider extends UIBuilderPsiReferenceProvider {

    @NotNull
    @Override
    protected PsiReference[] getUIBuilderReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
        UIBuilderReferenceFacade referenceFacade = new UIBuilderReferenceFacade(element);
        if (referenceFacade.isPropertyReference() || referenceFacade.isItemAsProperty()) {
            List<PsiReference> references = new ArrayList<>();
            //TODO Refactor is needed, because this naming is confusing, since this also can mean a simple binding not related to any controller bean
            references.add(new HtmlControllerBeanReference(element, referenceFacade.getControllerBeanTextRange(), referenceFacade.getControllerBeanName()));
            if (referenceFacade.isPropertyReference()) {
                buildPropertyReferences(referenceFacade).forEach(references::add);
            }
            return references.toArray(new PsiReference[0]);
        }
        return PsiReference.EMPTY_ARRAY;
    }

    public static Stream<HtmlBeanPropertyReference> buildPropertyReferences(@NotNull UIBuilderReferenceFacade referenceFacade) {
        Stream.Builder<HtmlBeanPropertyReference> referenceStreamBuilder = Stream.builder();
        HtmlBeanPropertyReference parentReference = null;
        for (int level = 0; level < referenceFacade.getPropertyPathCount(); level++) {
            HtmlBeanPropertyReference propertyReference = new HtmlBeanPropertyReference(
                referenceFacade.getElement(),
                referenceFacade.getPropertyPathTextRange(level),
                parentReference,
                referenceFacade.getControllerBeanName(),
                referenceFacade.getPropertyPath(level));
            referenceStreamBuilder.add(propertyReference);
            parentReference = propertyReference;
        }
        return referenceStreamBuilder.build();
    }

}
