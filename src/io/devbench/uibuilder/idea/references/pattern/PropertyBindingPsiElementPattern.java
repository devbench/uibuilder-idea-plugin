package io.devbench.uibuilder.idea.references.pattern;

import com.intellij.patterns.ElementPattern;
import com.intellij.patterns.ElementPatternCondition;
import com.intellij.patterns.InitialPatternCondition;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlTag;
import com.intellij.psi.xml.XmlText;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public class PropertyBindingPsiElementPattern implements ElementPattern<PsiElement> {

    @Override
    public boolean accepts(@Nullable Object o) {
        return accepts(o, null);
    }

    @Override
    public boolean accepts(@Nullable Object o, ProcessingContext context) {
        if (o instanceof XmlTag) {
            return Arrays.stream(((XmlTag) o).getChildren())
                .filter(XmlText.class::isInstance)
                .anyMatch(element -> {
                    String text = element.getText();
                    return text != null && text.contains("{{") && text.contains("}}");
                });
        } else if (o instanceof XmlAttributeValue) {
            String text = ((XmlAttributeValue) o).getValue();
            return text.contains("{{") && text.contains("}}");
        }
        return false;
    }

    @Override
    public ElementPatternCondition<PsiElement> getCondition() {
        return new ElementPatternCondition<>(new InitialPatternCondition<PsiElement>(PsiElement.class) {
        });
    }
}
