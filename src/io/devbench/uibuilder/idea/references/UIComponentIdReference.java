package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.HtmlComponentIdIndex;
import io.devbench.uibuilder.idea.references.visitor.XmlAttributeVisitor;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import java.util.stream.Collectors;

public class UIComponentIdReference extends PsiPolyVariantReferenceBase<PsiElement> {

    public UIComponentIdReference(@NotNull PsiElement psiElement) {
        // there should be a tag with the defined ID in a html file to be able to inject
        super(psiElement, false);
    }

    public static List<XmlAttributeValue> resolveComponentIdAttributeValue(@NotNull String componentId, Project project) {
        PsiManager psiManager = PsiManager.getInstance(project);

        List<PsiFile> files = FileBasedIndex.getInstance()
            .getContainingFiles(HtmlComponentIdIndex.INDEX_NAME, componentId, UIBuilderProjectUtils.createSearchScope(project))
            .stream()
            .map(psiManager::findFile)
            .sorted(UIBuilderProjectUtils.inProjectFileComparator(project))
            .collect(Collectors.toList());

        return files.stream()
            .flatMap(psiFile -> {
                XmlAttributeVisitor visitor = new XmlAttributeVisitor("id", componentId);
                psiFile.accept(visitor);
                return visitor.getFoundAttributes().stream();
            })
            .map(XmlAttribute::getValueElement)
            .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean b) {
        String idValue = (String) ((PsiLiteralValue) myElement).getValue();
        if (idValue != null) {
            return resolveComponentIdAttributeValue(idValue, myElement.getProject())
                .stream()
                .map(PsiElementResolveResult::new)
                .toArray(ResolveResult[]::new);
        }
        return new ResolveResult[0];
    }

}
