package io.devbench.uibuilder.idea.references;

import com.intellij.patterns.*;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReferenceContributor;
import com.intellij.psi.PsiReferenceRegistrar;
import io.devbench.uibuilder.idea.references.pattern.PropertyBindingPsiElementPattern;

public class UIBuilderHtmlReferenceContributor extends PsiReferenceContributor {

    @Override
    public void registerReferenceProviders(PsiReferenceRegistrar registrar) {
        XmlAttributeValuePattern componentIdAttributePattern = XmlPatterns.xmlAttributeValue(XmlPatterns.xmlAttribute().withName("id"));
        registrar.registerReferenceProvider(componentIdAttributePattern, new HtmlComponentIdReferenceProvider());

        StringPattern startsWithOnDashPattern = StandardPatterns.string().startsWith("on-");
        XmlAttributeValuePattern eventHandlerAttributePattern = XmlPatterns.xmlAttributeValue(XmlPatterns.xmlAttribute().withName(startsWithOnDashPattern));
        registrar.registerReferenceProvider(eventHandlerAttributePattern, new HtmlEventReferenceProvider());

        ElementPattern<PsiElement> controllerBeanPropertyReferencePattern = new PropertyBindingPsiElementPattern();
        registrar.registerReferenceProvider(controllerBeanPropertyReferencePattern, new HtmlBeanPropertyReferenceProvider());

        XmlAttributeValuePattern itemBindAttributePattern = XmlPatterns.xmlAttributeValue(XmlPatterns.xmlAttribute().withName("item-bind"));
        registrar.registerReferenceProvider(itemBindAttributePattern, new HtmlFormItemPropertyReferenceProvider());

        XmlAttributeValuePattern filterTypeFilterAttributePattern = XmlPatterns.xmlAttributeValue(
            XmlPatterns.xmlAttribute().withName("filter").withAncestor(1, XmlPatterns.xmlTag().withName("filter-type")));
        registrar.registerReferenceProvider(filterTypeFilterAttributePattern, new HtmlFormItemPropertyReferenceProvider());

        XmlAttributeValuePattern gridSorterPathAttributePattern = XmlPatterns.xmlAttributeValue(
            XmlPatterns.xmlAttribute().withName("path").withAncestor(1, XmlPatterns.xmlTag().withName("vaadin-grid-sorter")));
        registrar.registerReferenceProvider(gridSorterPathAttributePattern, new HtmlFormItemPropertyReferenceProvider());

        XmlAttributeValuePattern inlineEditablePathAttributePattern = XmlPatterns.xmlAttributeValue(
            XmlPatterns.xmlAttribute().withName("path").withAncestor(1, XmlPatterns.xmlTag().withName("uibuilder-item-editable")));
        registrar.registerReferenceProvider(inlineEditablePathAttributePattern, new HtmlFormItemPropertyReferenceProvider());

        XmlAttributeValuePattern dataSourceNameAttributePattern = XmlPatterns.xmlAttributeValue(
            XmlPatterns.xmlAttribute().withName("name").withAncestor(1, XmlPatterns.xmlTag().withName("data-source")));
        registrar.registerReferenceProvider(dataSourceNameAttributePattern, new HtmlDatasourceNameReferenceProvider());

        XmlAttributeValuePattern crudPanelControllerBeanAttributePattern = XmlPatterns.xmlAttributeValue(
            XmlPatterns.xmlAttribute().withName("controller-bean").withAncestor(1, XmlPatterns.xmlTag().withName("crud-panel")));
        registrar.registerReferenceProvider(crudPanelControllerBeanAttributePattern, new HtmlCrudPanelControllerBeanReferenceProvider());

        XmlAttributeValuePattern pageContextIdAttributePattern = XmlPatterns.xmlAttributeValue(XmlPatterns.or(
            XmlPatterns.xmlAttribute().withName("contextId").withAncestor(1, XmlPatterns.xmlTag().withName("uibuilder-page")),
            XmlPatterns.xmlAttribute().withName("context-id").withAncestor(1, XmlPatterns.xmlTag().withName("uibuilder-page"))));
        registrar.registerReferenceProvider(pageContextIdAttributePattern, new PageContextReferenceProvider());

        XmlAttributeValuePattern htmlImportHrefAttributePattern = XmlPatterns.xmlAttributeValue(XmlPatterns.and(
            XmlPatterns.xmlAttribute().withName("href").withAncestor(1, XmlPatterns.xmlTag().withName("link")),
            XmlPatterns.xmlAttribute().withName("href").withAncestor(1, XmlPatterns.xmlTag().withAttributeValue("rel", "import"))));
        registrar.registerReferenceProvider(htmlImportHrefAttributePattern, new HtmlImportHrefReferenceProvider());
    }

}
