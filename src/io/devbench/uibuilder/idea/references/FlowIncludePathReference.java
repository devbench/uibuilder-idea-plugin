package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.util.UIBuilderFlowUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class FlowIncludePathReference extends PsiReferenceBase<PsiElement> {

    public FlowIncludePathReference(@NotNull PsiElement psiElement) {
        // there should be a html file on the specified path, so it has to be a hard reference
        super(psiElement, false);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        String includePath = ((XmlAttributeValue) myElement).getValue();
        Project project = myElement.getProject();

        return UIBuilderFlowUtils.findAllFlowDefinitionXmlPaths(project).entrySet().stream()
            .filter(pathXmlFileEntry -> pathXmlFileEntry.getKey().equals(includePath))
            .map(Map.Entry::getValue)
            .findFirst()
            .orElse(null);
    }

}
