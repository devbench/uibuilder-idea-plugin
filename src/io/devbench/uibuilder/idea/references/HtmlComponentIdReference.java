package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.impl.source.PsiClassReferenceType;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.util.IncorrectOperationException;
import io.devbench.uibuilder.idea.util.UIBuilderFormUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;
import static io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils.*;

public class HtmlComponentIdReference extends PsiPolyVariantReferenceBase<PsiElement> {

    public HtmlComponentIdReference(@NotNull PsiElement psiElement) {
        // reference is soft, the tag may define an ID without a java side UIComponent binding
        super(psiElement, true);
    }

    public static List<PsiElement> resolveComponentIdInjection(@NotNull String componentId, @NotNull Project project) {
        List<PsiElement> resolvedPsiElements = new ArrayList<>();
        for (PsiClass psiClass : UIBuilderFormUtils.findClassesByInjectedComponentId(project, componentId)) {
            for (PsiMethod psiMethod : psiClass.getMethods()) {
                PsiParameterList parameterList = psiMethod.getParameterList();
                if (!parameterList.isEmpty()) {
                    for (PsiParameter parameter : parameterList.getParameters()) {
                        if (isComponentIdAnnotationEquals(parameter, componentId)) {
                            resolvedPsiElements.add(parameter);
                        }
                    }
                }
            }
            for (PsiField psiField : psiClass.getFields()) {
                PsiType type = psiField.getType();
                if (type instanceof PsiClassReferenceType) {
                    PsiClassReferenceType psiClassReferenceType = (PsiClassReferenceType) type;
                    if (isProviderType(psiClassReferenceType) && isComponentIdAnnotationEquals(psiField, componentId)) {
                        resolvedPsiElements.add(psiField);
                    }
                }
            }
        }
        return resolvedPsiElements;
    }

    private static boolean isProviderType(PsiClassReferenceType type) {
        String className = type.getClassName();
        return ("Provider".equals(className) || "ObjectProvider".equals(className)) && type.getParameterCount() == 1;
    }

    public static Optional<PsiType> findPsiTypeOfUIComponentInjection(PsiElement element) {
        PsiType type = null;

        PsiTypeElement psiTypeElement = PsiTreeUtil.getChildOfType(element, PsiTypeElement.class);
        if (psiTypeElement != null) {
            type = psiTypeElement.getType();
        }

        if (type instanceof PsiClassReferenceType && isProviderType((PsiClassReferenceType) type)) {
            type = ((PsiClassReferenceType) type).getParameters()[0];
        }

        return Optional.ofNullable(type);
    }

    private static boolean isComponentIdAnnotationEquals(PsiJvmModifiersOwner checkElement, String componentId) {
        if (checkElement.hasAnnotation(FQN_UI_COMPONENT_ANNNOTATION)) {
            PsiAnnotation annotation = checkElement.getAnnotation(FQN_UI_COMPONENT_ANNNOTATION);
            Optional<String> valueAnnotationValue = getValueAnnotationValue(annotation);
            return valueAnnotationValue.isPresent() && valueAnnotationValue.get().equals(componentId);
        }
        return false;
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean incompleteCode) {
        String idValue = ((XmlAttributeValue) myElement).getValue();
        return resolveComponentIdInjection(idValue, myElement.getProject())
            .stream()
            .map(PsiElementResolveResult::new)
            .toArray(ResolveResult[]::new);
    }

    @Override
    public PsiElement handleElementRename(@NotNull String newElementName) throws IncorrectOperationException {
        throw new IncorrectOperationException("Rename is not supported yet");
    }

}
