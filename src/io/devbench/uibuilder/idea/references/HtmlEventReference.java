package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.references.facade.UIBuilderEventReference;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;
import static io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils.*;

public class HtmlEventReference extends PsiPolyVariantReferenceBase<PsiElement> {

    private final UIBuilderEventReference eventReference;

    public HtmlEventReference(@NotNull PsiElement psiElement, @NotNull TextRange rangeInElement, @NotNull UIBuilderEventReference eventReference) {
        // the event reference defined in the html should have a valid UIEventHandler binding
        super(psiElement, rangeInElement, false);
        this.eventReference = eventReference;
    }

    public static List<PsiMethod> resolveEventReferencedMethods(@NotNull UIBuilderEventReference eventReference, Project project) {
        PsiManager psiManager = PsiManager.getInstance(project);

        VirtualFile controllerBeanFile = FileBasedIndex.getInstance()
            .getContainingFiles(ControllerBeanIndex.INDEX_NAME, eventReference.getControllerBeanName(), UIBuilderProjectUtils.createSearchScope(project))
            .stream().findFirst().orElse(null);

        if (controllerBeanFile == null) {
            return Collections.emptyList();
        }

        PsiJavaFile file = (PsiJavaFile) psiManager.findFile(controllerBeanFile);
        if (file == null) {
            return Collections.emptyList();
        }

        List<PsiMethod> methods = new ArrayList<>();
        for (PsiClass psiClass : file.getClasses()) {
            for (PsiMethod psiMethod : psiClass.getAllMethods()) {
                if (psiMethod.hasAnnotation(FQN_UI_EVENT_HANDLER_ANNOTATION)) {
                    PsiAnnotation psiAnnotation = psiMethod.getAnnotation(FQN_UI_EVENT_HANDLER_ANNOTATION);
                    getValueAnnotationValue(psiAnnotation).ifPresent(value -> {
                        if (eventReference.isEventNameMatch(value)) {
                            methods.add(psiMethod);
                        }
                    });
                }
            }
        }

        return methods;
    }

    @Override
    public boolean isReferenceTo(@NotNull PsiElement element) {
        return super.isReferenceTo(element);
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean incompleteCode) {
        return PsiElementResolveResult.createResults(resolveEventReferencedMethods(eventReference, myElement.getProject()));
    }

    @Override
    public PsiElement handleElementRename(@NotNull String newElementName) throws IncorrectOperationException {
        throw new IncorrectOperationException("Rename is not supported yet");
    }

}
