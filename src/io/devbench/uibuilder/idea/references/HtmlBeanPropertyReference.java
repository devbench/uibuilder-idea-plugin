package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import static io.devbench.uibuilder.idea.util.UIBuilderFormUtils.*;

public class HtmlBeanPropertyReference extends AbstractPropertyReference {

    private final String bindingRootName;

    public HtmlBeanPropertyReference(@NotNull PsiElement psiElement,
                                     @NotNull TextRange rangeInElement,
                                     @Nullable HtmlBeanPropertyReference parentReference,
                                     @NotNull String bindingRootName,
                                     @NotNull String propertyName) {
        // the controller bean property reference defined in the html should be a valid property in the controller bean class
        super(psiElement, rangeInElement, false, parentReference, propertyName);
        this.bindingRootName = bindingRootName;
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        Project project = myElement.getProject();

        PsiClass memberPsiClass;
        if (parentReference == null) {
            if (findTemplateBeanName(myElement).equals(bindingRootName)) {
                memberPsiClass = findItemPropertyClass(myElement).orElse(null);
            } else {
                memberPsiClass = HtmlControllerBeanReference.getControllerBeanClass(project, bindingRootName).orElse(null);
            }
        } else {
            memberPsiClass = resolveFromParentReference(project);
        }

        return resolveFieldOrGetter(memberPsiClass);
    }

    @Override
    public PsiElement handleElementRename(@NotNull String newElementName) throws IncorrectOperationException {
        throw new IncorrectOperationException("Rename is not supported yet");
    }

}
