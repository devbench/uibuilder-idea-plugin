package io.devbench.uibuilder.idea.references;

import com.intellij.patterns.XmlAttributeValuePattern;
import com.intellij.patterns.XmlPatterns;
import com.intellij.psi.PsiReferenceContributor;
import com.intellij.psi.PsiReferenceRegistrar;

public class UIBuilderFlowXmlReferenceContributor extends PsiReferenceContributor {

    @Override
    public void registerReferenceProviders(PsiReferenceRegistrar registrar) {
        XmlAttributeValuePattern htmlAttributePattern = XmlPatterns.xmlAttributeValue(
            XmlPatterns.xmlAttribute().withName("html").withAncestor(1, XmlPatterns.xmlTag().withName("flow")));
        registrar.registerReferenceProvider(htmlAttributePattern, new FlowHtmlReferenceProvider());

        XmlAttributeValuePattern targetAttributePattern = XmlPatterns.xmlAttributeValue(XmlPatterns.or(
            XmlPatterns.xmlAttribute().withName("target").withAncestor(1, XmlPatterns.xmlTag().withName("flow")),
            XmlPatterns.xmlAttribute().withName("target").withAncestor(1, XmlPatterns.xmlTag().withName("include"))));
        registrar.registerReferenceProvider(targetAttributePattern, new FlowTargetReferenceProvider());

        XmlAttributeValuePattern idAttributePattern = XmlPatterns.xmlAttributeValue(
            XmlPatterns.xmlAttribute().withName("id").withAncestor(1, XmlPatterns.xmlTag().withName("flow")));
        registrar.registerReferenceProvider(idAttributePattern, new FlowIdReferenceProvider());

        XmlAttributeValuePattern fallbackIdAttributePattern = XmlPatterns.xmlAttributeValue(
            XmlPatterns.xmlAttribute().withName("fallbackId").withAncestor(1, XmlPatterns.xmlTag()));
        registrar.registerReferenceProvider(fallbackIdAttributePattern, new FlowFallbackIdReferenceProvider());

        XmlAttributeValuePattern includePathAttributePattern = XmlPatterns.xmlAttributeValue(
            XmlPatterns.xmlAttribute().withName("path").withAncestor(1, XmlPatterns.xmlTag().withName("include")));
        registrar.registerReferenceProvider(includePathAttributePattern, new FlowIncludePathReferenceProvider());
    }

}
