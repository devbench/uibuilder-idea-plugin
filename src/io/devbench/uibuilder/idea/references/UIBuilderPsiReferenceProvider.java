package io.devbench.uibuilder.idea.references;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.util.ProcessingContext;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;

public abstract class UIBuilderPsiReferenceProvider extends PsiReferenceProvider {

    @NotNull
    @Override
    public PsiReference[] getReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext processingContext) {
        if (UIBuilderProjectUtils.isInUIBuilderScope(element)) {
            return getUIBuilderReferencesByElement(element, processingContext);
        }
        return new PsiReference[0];
    }

    @NotNull
    protected abstract PsiReference[] getUIBuilderReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext processingContext);

}
