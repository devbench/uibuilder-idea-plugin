package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.references.facade.UIBuilderFlowReference;
import io.devbench.uibuilder.idea.util.UIBuilderFlowUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public class HtmlFlowDefinitionReference extends PsiReferenceBase<PsiElement> {

    private UIBuilderFlowReference flowReference;

    public HtmlFlowDefinitionReference(@NotNull PsiElement psiElement, @NotNull TextRange rangeInElement, @NotNull UIBuilderFlowReference flowReference) {
        // the event reference defined in the html should have a valid flow definition
        super(psiElement, rangeInElement, false);
        this.flowReference = flowReference;
    }

    public static XmlAttributeValue resolveFlowDefinitionAttributeValue(@NotNull Project project, @NotNull String flowId) {
        List<VirtualFile> projectRootFlowDefinitionXml = UIBuilderFlowUtils.findProjectRootFlowDefinitionXml(project);
        Map<String, PsiElement> stringPsiElementMap = UIBuilderFlowUtils.collectFlowIds(project, projectRootFlowDefinitionXml);
        return stringPsiElementMap
            .entrySet()
            .stream()
            .filter(flowIdElementEntry -> flowId.equals(flowIdElementEntry.getKey()))
            .map(Map.Entry::getValue)
            .filter(XmlAttributeValue.class::isInstance)
            .map(XmlAttributeValue.class::cast)
            .findFirst()
            .orElse(null);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        return resolveFlowDefinitionAttributeValue(myElement.getProject(), flowReference.getFlowId());
    }

}
