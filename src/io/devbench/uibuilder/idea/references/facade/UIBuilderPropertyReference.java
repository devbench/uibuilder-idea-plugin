package io.devbench.uibuilder.idea.references.facade;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class UIBuilderPropertyReference {

    private String controllerBeanName;
    private String propertyPath;

    private UIBuilderPropertyReference() {
    }

    public String getControllerBeanName() {
        return controllerBeanName;
    }

    public String getPropertyPath() {
        return propertyPath;
    }

    private String getTrimmedControllerBeanNameWithEndingDotOrEmptyString() {
        return Optional.ofNullable(controllerBeanName)
            .map(String::trim)
            .map(cbName -> cbName + ".")
            .orElse("");
    }

    public String getReference() {
        return "{{" + getTrimmedControllerBeanNameWithEndingDotOrEmptyString() + Objects.requireNonNull(propertyPath).trim() + "}}";
    }

    public String getPropertyPathWithControllerBean() {
        return getTrimmedControllerBeanNameWithEndingDotOrEmptyString() + Objects.requireNonNull(propertyPath).trim();
    }

    public boolean isPropertyPathMatch(String propertyPath) {
        if (propertyPath == null) {
            return false;
        }
        return Objects.requireNonNull(this.propertyPath).trim().equals(propertyPath.trim());
    }

    public boolean isControllerBeanNameMatch(String controllerBeanName) {
        if (controllerBeanName == null || this.controllerBeanName == null) {
            return false;
        }
        return Objects.requireNonNull(this.controllerBeanName).trim().equals(controllerBeanName.trim());
    }

    @NotNull
    public static UIBuilderPropertyReference create(@NotNull String controllerBeanName, @NotNull String propertyPath) {
        UIBuilderPropertyReference uiBuilderEventReference = new UIBuilderPropertyReference();
        uiBuilderEventReference.controllerBeanName = controllerBeanName;
        uiBuilderEventReference.propertyPath = propertyPath;
        return uiBuilderEventReference;
    }

    @Nullable
    public static UIBuilderPropertyReference parse(@NotNull String propertyReference) {
        return parse(propertyReference, true);
    }

    @Nullable
    public static UIBuilderPropertyReference parse(@NotNull String propertyReference, boolean withControllerBean) {
        String strippedPropertyReference;
        if (propertyReference.startsWith("{{") && propertyReference.endsWith("}}")) {
            strippedPropertyReference = propertyReference.substring(2, propertyReference.length() - 2);
        } else {
            strippedPropertyReference = propertyReference;
        }

        UIBuilderPropertyReference uiBuilderPropertyReference = new UIBuilderPropertyReference();
        if (!withControllerBean || strippedPropertyReference.contains(".")) {
            if (withControllerBean) {
                String[] split = strippedPropertyReference.split("\\.");
                if (split.length > 0) {
                    uiBuilderPropertyReference.controllerBeanName = split[0];
                }
                if (split.length > 1) {
                    uiBuilderPropertyReference.propertyPath = String.join(".", Arrays.copyOfRange(split, 1, split.length));
                }
            } else {
                uiBuilderPropertyReference.propertyPath = strippedPropertyReference;
            }
        } else {
            return null;
        }

        if (withControllerBean && (uiBuilderPropertyReference.controllerBeanName == null || uiBuilderPropertyReference.controllerBeanName.trim().isEmpty())) {
            return null;
        }

        if (uiBuilderPropertyReference.propertyPath == null || uiBuilderPropertyReference.propertyPath.trim().isEmpty()) {
            return null;
        }

        return uiBuilderPropertyReference;
    }
}
