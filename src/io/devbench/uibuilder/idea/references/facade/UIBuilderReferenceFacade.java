package io.devbench.uibuilder.idea.references.facade;

import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiRecursiveElementWalkingVisitor;
import com.intellij.psi.xml.XmlTag;
import com.intellij.psi.xml.XmlText;
import org.jetbrains.annotations.NotNull;
import java.util.Arrays;

public class UIBuilderReferenceFacade {

    private final PsiElement element;
    private int inElementOffset;
    private String reference;
    private final boolean withControllerBean;
    private boolean curlyWrapped;
    private UIBuilderPropertyReference propertyReference;
    private UIBuilderEventReference eventReference;
    private UIBuilderFlowReference flowReference;
    private boolean itemAsProperty;

    public UIBuilderReferenceFacade(@NotNull PsiElement element) {
        this(element, true);
    }

    public UIBuilderReferenceFacade(@NotNull PsiElement element, boolean withControllerBean) {
        this.element = element;
        this.inElementOffset = 0;
        this.withControllerBean = withControllerBean;
        this.curlyWrapped = false;
        parseReference();
        processReference();
    }

    public PsiElement getElement() {
        return element;
    }

    private void parseReference() {
        reference = element.getText();

        if (element instanceof XmlTag) {
            XmlText xmlText = Arrays.stream(element.getChildren())
                .filter(XmlText.class::isInstance)
                .map(XmlText.class::cast)
                .findFirst().orElse(null);
            if (xmlText != null) {
                inElementOffset = xmlText.getStartOffsetInParent();
                reference = xmlText.getValue();
            } else {
                reference = "";
            }
        }

        if (reference.contains("{{") && reference.contains("}}")) {
            int curlyBraceOffset = reference.indexOf("{{");
            reference = reference.substring(curlyBraceOffset);
            reference = reference.substring(0, reference.indexOf("}}") + 2);
            inElementOffset = inElementOffset + curlyBraceOffset;
            curlyWrapped = true;
        } else {
            if (StringUtil.isQuotedString(reference)) {
                reference = StringUtil.unquoteString(reference);
                inElementOffset = inElementOffset + 1;
                removePropertyPrefixIfNeeded();
            }
        }
    }

    private void removePropertyPrefixIfNeeded() {
        if (!withControllerBean && reference.contains(":")) {
            final int propertyPrefixOffset = reference.indexOf(":") + 1;
            reference = reference.substring(propertyPrefixOffset);
            inElementOffset = inElementOffset + propertyPrefixOffset;
        }
    }

    private void processReference() {
        if (reference.contains("::")) {
            int doubleColonIndex = reference.indexOf("::");
            if (doubleColonIndex > 0) {
                eventReference = UIBuilderEventReference.parse(reference);
            }
        } else if (reference.startsWith("flow:")) {
            flowReference = UIBuilderFlowReference.parse(reference);
        } else if (withControllerBean && reference.equals(curlyWrapped ? "{{item}}" : "item")) {
            itemAsProperty = true;
        } else {
            int firstDotIndex = reference.indexOf(".");
            if (!withControllerBean || firstDotIndex > 0) {
                propertyReference = UIBuilderPropertyReference.parse(reference, withControllerBean);
            }
        }
    }

    public boolean isPropertyReference() {
        return propertyReference != null;
    }

    public boolean isItemAsProperty() {
        return itemAsProperty;
    }

    public boolean isEventReference() {
        return eventReference != null;
    }

    public boolean isFlowReference() {
        return flowReference != null;
    }

    public TextRange getControllerBeanTextRange() {
        if (isEventReference()) {
            return new TextRange(inElementOffset, inElementOffset + reference.indexOf("::"));
        } else if (itemAsProperty && withControllerBean) {
            return new TextRange(inElementOffset + (curlyWrapped ? 2 : 0), inElementOffset + (curlyWrapped ? reference.indexOf("}}") : reference.length()));
        } else if (isPropertyReference()) {
            if (withControllerBean) {
                return new TextRange(inElementOffset + (curlyWrapped ? 2 : 0), inElementOffset + reference.indexOf("."));
            } else {
                return new TextRange(inElementOffset, inElementOffset);
            }
        }
        return null;
    }

    public TextRange getFlowIdTextRange() {
        if (isFlowReference()) {
            return new TextRange(inElementOffset + 5, inElementOffset + reference.length());
        }
        return null;
    }

    public int getPropertyPathCount() {
        if (isPropertyReference()) {
            return (int) getPropertyReference().getPropertyPath().chars().filter(ch -> ch == '.').count() + 1;
        }
        return 0;
    }

    public String getPropertyPath() {
        if (isPropertyReference()) {
            return getPropertyPath(0);
        }
        return null;
    }

    public String getLeafPropertyPath() {
        if (isPropertyReference() && getPropertyPathCount() > 0) {
            return getPropertyPath(getPropertyPathCount() - 1);
        }
        return null;
    }

    public String getPropertyPath(int level) {
        if (isPropertyReference() && level >= 0 && level < getPropertyPathCount()) {
            String propertyPath = getPropertyReference().getPropertyPath();
            String[] split = propertyPath.split("\\.");
            if (split.length > level) {
                return split[level];
            } else {
                if (split.length == level && propertyPath.indexOf('.') > 0) {
                    return "";
                }
            }
        }
        return null;
    }

    public TextRange getPropertyPathTextRange() {
        if (isPropertyReference()) {
            return getPropertyPathTextRange(0);
        }
        return null;
    }

    public TextRange getPropertyPathTextRange(int level) {
        if (isPropertyReference()) {
            int startOffset = getControllerBeanTextRange().getEndOffset();
            int endOffset = startOffset;
            for (int i = 0; i <= level; i++) {
                startOffset = endOffset + 1;
                endOffset = startOffset + getPropertyPath(i).length();
            }
            return new TextRange(startOffset - (withControllerBean ? 0 : 1), endOffset - (withControllerBean ? 0 : 1));
        }
        return null;
    }

    public TextRange getEventTextRange() {
        if (isEventReference()) {
            return new TextRange(inElementOffset + reference.indexOf("::") + 2, inElementOffset + reference.length());
        }
        return null;
    }

    public UIBuilderPropertyReference getPropertyReference() {
        return propertyReference;
    }

    public UIBuilderEventReference getEventReference() {
        return eventReference;
    }

    public UIBuilderFlowReference getFlowReference() {
        return flowReference;
    }

    public String getReference() {
        return reference;
    }

    public boolean hasReference() {
        return reference != null;
    }

    public String getControllerBeanName() {
        if (isEventReference()) {
            return eventReference.getControllerBeanName();
        } else if (itemAsProperty && withControllerBean) {
            return "item";
        } else if (isPropertyReference()) {
            return propertyReference.getControllerBeanName();
        }
        return null;
    }

    @NotNull
    public PsiElement findPsiElementChild(@NotNull PsiElement rootElement) {
        if (hasReference()) {
            PsiElement[] foundElement = new PsiElement[] { null };
            rootElement.accept(new PsiRecursiveElementWalkingVisitor() {
                @Override
                public void visitElement(@NotNull PsiElement element) {
                    if (getReference().equals(element.getText())) {
                        foundElement[0] = element;
                        stopWalking();
                    } else {
                        super.visitElement(element);
                    }
                }
            });
            return foundElement[0] == null ? rootElement : foundElement[0];
        } else {
            return rootElement;
        }
    }
}
