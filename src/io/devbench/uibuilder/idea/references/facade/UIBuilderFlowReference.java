package io.devbench.uibuilder.idea.references.facade;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class UIBuilderFlowReference {

    private String flowId;

    private UIBuilderFlowReference() {
    }

    public String getFlowId() {
        return flowId;
    }

    public String getReference() {
        return "flow:" + flowId;
    }

    public boolean isFlowIdMatch(String flowId) {
        if (flowId == null) {
            return false;
        }
        return Objects.requireNonNull(this.flowId).trim().equals(flowId.trim());
    }

    public static UIBuilderFlowReference create(@NotNull String flowId) {
        UIBuilderFlowReference uiBuilderFlowReference = new UIBuilderFlowReference();
        uiBuilderFlowReference.flowId = flowId;
        return uiBuilderFlowReference;
    }

    public static UIBuilderFlowReference parse(@NotNull String flowReference) {
        UIBuilderFlowReference uiBuilderFlowReference = new UIBuilderFlowReference();
        if (flowReference.startsWith("flow:")) {
            uiBuilderFlowReference.flowId = flowReference.substring(5).trim();
        } else {
            return null;
            //throw new IllegalArgumentException("UIBuilder event reference must contain double colon");
        }

        if (uiBuilderFlowReference.flowId.isEmpty()) {
            return null;
            //throw new IllegalArgumentException("UIBuilder event reference controller bean name part must be defined");
        }

        return uiBuilderFlowReference;
    }
}
