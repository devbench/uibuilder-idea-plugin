package io.devbench.uibuilder.idea.references.facade;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class UIBuilderEventReference {

    private String controllerBeanName;
    private String eventName;

    private UIBuilderEventReference() {
    }

    public String getControllerBeanName() {
        return controllerBeanName;
    }

    public String getEventName() {
        return eventName;
    }

    public String getReference() {
        return Objects.requireNonNull(controllerBeanName).trim() + "::" + Objects.requireNonNull(eventName).trim();
    }

    public boolean isEventNameMatch(String eventName) {
        if (eventName == null) {
            return false;
        }
        return Objects.requireNonNull(this.eventName).trim().equals(eventName.trim());
    }

    public boolean isControllerBeanNameMatch(String controllerBeanName) {
        if (controllerBeanName == null) {
            return false;
        }
        return Objects.requireNonNull(this.controllerBeanName).trim().equals(controllerBeanName.trim());
    }

    public static UIBuilderEventReference create(@NotNull String controllerBeanName, @NotNull String eventName) {
        UIBuilderEventReference uiBuilderEventReference = new UIBuilderEventReference();
        uiBuilderEventReference.controllerBeanName = controllerBeanName;
        uiBuilderEventReference.eventName = eventName;
        return uiBuilderEventReference;
    }

    public static UIBuilderEventReference parse(@NotNull String eventReference) {
        UIBuilderEventReference uiBuilderEventReference = new UIBuilderEventReference();
        if (eventReference.contains("::")) {
            String[] split = eventReference.split("::");
            if (split.length == 2) {
                uiBuilderEventReference.controllerBeanName = split[0];
                uiBuilderEventReference.eventName = split[1];
            }
        } else {
            return null;
            //throw new IllegalArgumentException("UIBuilder event reference must contain double colon");
        }

        if (uiBuilderEventReference.controllerBeanName == null || uiBuilderEventReference.controllerBeanName.trim().isEmpty()) {
            return null;
            //throw new IllegalArgumentException("UIBuilder event reference controller bean name part must be defined");
        }

        if (uiBuilderEventReference.eventName == null || uiBuilderEventReference.eventName.trim().isEmpty()) {
            return null;
            //throw new IllegalArgumentException("UIBuilder event reference event name part must be defined");
        }

        return uiBuilderEventReference;
    }
}
