package io.devbench.uibuilder.idea.references;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.util.UIBuilderHtmlUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.atomic.AtomicReference;

public class FlowTargetReference extends PsiReferenceBase<PsiElement> {

    public FlowTargetReference(@NotNull PsiElement psiElement) {
        // there should be a html file on the specified path, so it has to be a hard reference
        super(psiElement, false);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        String target = ((XmlAttributeValue) myElement).getValue();
        AtomicReference<PsiElement> resolvedTargetAttribute = new AtomicReference<>(null);
        UIBuilderHtmlUtils.findInHtml(myElement.getProject(), new XmlRecursiveElementVisitor() {
            @Override
            public void visitXmlAttribute(XmlAttribute attribute) {
                if (attribute.getName().equals("id")
                    && attribute.getParent().getName().equals("uibuilder-page")
                    && target.equals(attribute.getValue())) {
                    resolvedTargetAttribute.set(attribute.getValueElement());
                } else {
                    super.visitXmlAttribute(attribute);
                }
            }
        });
        return resolvedTargetAttribute.get();
    }

}
