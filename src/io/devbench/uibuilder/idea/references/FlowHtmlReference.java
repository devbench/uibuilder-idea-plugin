package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.xml.XmlAttributeValue;
import io.devbench.uibuilder.idea.util.UIBuilderHtmlUtils;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.concurrent.atomic.AtomicReference;

public class FlowHtmlReference extends PsiReferenceBase<PsiElement> {

    public FlowHtmlReference(@NotNull PsiElement psiElement) {
        // This must be a soft reference because there is an annotator for the possible path fix, and the error is also handled by the annotator
        super(psiElement, true);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        String htmlPath = ((XmlAttributeValue) myElement).getValue();
        Project project = myElement.getProject();
        return resolveHtmlPath(htmlPath, project);
    }

    @Nullable
    public static PsiElement resolveHtmlPath(@NotNull String htmlPath, @NotNull Project project) {
        PsiManager psiManager = PsiManager.getInstance(project);
        ProjectFileIndex projectFileIndex = ProjectFileIndex.getInstance(project);
        AtomicReference<PsiElement> resolvedHtmlFile = new AtomicReference<>(null);
        UIBuilderProjectUtils.iterateProjectFileIndexInUIBuilderScope(project, html -> {
            VirtualFile sourceRootForFile = projectFileIndex.getSourceRootForFile(html);
            if (sourceRootForFile != null) {
                String relativePath = html.getPath().substring(sourceRootForFile.getPath().length());
                if (relativePath.equals(htmlPath)) {
                    resolvedHtmlFile.set(psiManager.findFile(html));
                    return false;
                }
            }
            return true;
        }, fileOrDir -> !projectFileIndex.isInTestSourceContent(fileOrDir) && !fileOrDir.isDirectory() && UIBuilderHtmlUtils.isUIBuilderHtmlFile(fileOrDir));
        return resolvedHtmlFile.get();
    }

}
