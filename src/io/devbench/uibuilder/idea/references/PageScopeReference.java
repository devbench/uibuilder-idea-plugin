package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import io.devbench.uibuilder.idea.util.UIBuilderHtmlUtils;
import io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.atomic.AtomicReference;

public class PageScopeReference extends PsiReferenceBase<PsiElement> {

    public PageScopeReference(@NotNull PsiElement psiElement) {
        super(psiElement, true);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        Project project = myElement.getProject();
        PsiAnnotation annotation = PsiTreeUtil.getParentOfType(myElement, PsiAnnotation.class, true);
        AtomicReference<PsiElement> resolvedElement = new AtomicReference<>(null);
        if (annotation != null) {
            UIBuilderPsiTreeUtils.getValueAnnotationValue(annotation).ifPresent(pageContextId -> {
                UIBuilderHtmlUtils.findInHtml(project, new XmlRecursiveElementVisitor() {
                    @Override
                    public void visitXmlAttribute(XmlAttribute attribute) {
                        if (attribute.getParent().getName().equals("uibuilder-page")
                            && (attribute.getName().equals("contextId") || attribute.getName().equals("context-id"))
                            && attribute.getValue() != null
                            && pageContextId.equals(attribute.getValue())) {
                            resolvedElement.set(attribute.getValueElement());
                        }
                        super.visitXmlAttribute(attribute);
                    }
                });
            });
        }
        return resolvedElement.get();
    }

}
