package io.devbench.uibuilder.idea.references;

import com.intellij.psi.*;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

public class UIComponentIdReferenceProvider extends UIBuilderPsiReferenceProvider {

    @NotNull
    @Override
    protected PsiReference[] getUIBuilderReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
        return new PsiReference[]{new UIComponentIdReference(element)};
    }

}
