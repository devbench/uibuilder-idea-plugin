package io.devbench.uibuilder.idea.references;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.HtmlCrossImportIndex;
import io.devbench.uibuilder.idea.index.HtmlDomModuleIndex;
import io.devbench.uibuilder.idea.util.UIBuilderWebjarUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public class HtmlImportHrefReference extends PsiReferenceBase<PsiElement> {

    public HtmlImportHrefReference(@NotNull PsiElement psiElement) {
        // there should be a html file in the project for the current import
        super(psiElement, false);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        String href = ((XmlAttributeValue) myElement).getValue();
        if (href.startsWith(UIBuilderWebjarUtils.FRONTEND_BOWER_COMPONENTS)) {
            href = href.substring(UIBuilderWebjarUtils.FRONTEND_BOWER_COMPONENTS.length());

            Project project = myElement.getProject();
            PsiManager psiManager = PsiManager.getInstance(project);
            GlobalSearchScope searchScope = GlobalSearchScope.allScope(project);

            Map<String, VirtualFile> indexedFiles = new HashMap<>();

            for (String key : FileBasedIndex.getInstance().getAllKeys(HtmlCrossImportIndex.INDEX_NAME, project)) {
                FileBasedIndex.getInstance().getContainingFiles(HtmlCrossImportIndex.INDEX_NAME, key, searchScope)
                    .forEach(virtualFile -> UIBuilderWebjarUtils.stripWebjarPath(virtualFile.getPath())
                        .ifPresent(path -> indexedFiles.put(path, virtualFile)));
            }
            for (String key : FileBasedIndex.getInstance().getAllKeys(HtmlDomModuleIndex.INDEX_NAME, project)) {
                FileBasedIndex.getInstance().getContainingFiles(HtmlCrossImportIndex.INDEX_NAME, key, searchScope)
                    .forEach(virtualFile -> UIBuilderWebjarUtils.stripWebjarPath(virtualFile.getPath())
                        .ifPresent(path -> indexedFiles.put(path, virtualFile)));
            }

            VirtualFile virtualFile = indexedFiles.get(href);
            if (virtualFile != null) {
                return psiManager.findFile(virtualFile);
            }

        }

        return null;
    }

}
