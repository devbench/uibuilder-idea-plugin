package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlTag;
import com.intellij.psi.xml.XmlToken;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.TargetDataSourceIndex;
import io.devbench.uibuilder.idea.util.UIBuilderFormUtils;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HtmlDataSourceNameCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement psiElement = parameters.getOriginalPosition();
        if (psiElement instanceof XmlToken) {
            XmlAttributeValue nameAttribute = getNameAttribute((XmlToken) psiElement);
            if (nameAttribute != null && isDataSourceTag(nameAttribute)) {
                FileBasedIndex.getInstance().getAllKeys(TargetDataSourceIndex.INDEX_NAME, nameAttribute.getProject())
                    .forEach(dataSourceName -> {
                        Project project = psiElement.getProject();
                        FileBasedIndex.getInstance()
                            .getContainingFiles(TargetDataSourceIndex.INDEX_NAME, dataSourceName, UIBuilderProjectUtils.createSearchScope(project))
                            .stream()
                            .findFirst()
                            .ifPresent(virtualFile -> {
                                String repositoryEntityName = getRepositoryEntityName(virtualFile, project);
                                result.addElement(LookupElementBuilder.create(dataSourceName)
                                    .withTailText("  " + repositoryEntityName, true)
                                    .withIcon(UIBuilderIcons.Gutter.TargetDataSource)
                                    .withTypeText(virtualFile.getPresentableName()));
                            });
                    });
                result.stopHere();
            }
        }
    }

    private String getRepositoryEntityName(VirtualFile virtualFile, Project project) {
        PsiFile file = PsiManager.getInstance(project).findFile(virtualFile);
        if (file instanceof PsiJavaFile) {
            for (PsiClass psiClass : ((PsiJavaFile) file).getClasses()) {
                for (PsiClassType referencedType : psiClass.getExtendsList().getReferencedTypes()) {
                    if (referencedType.getName().endsWith("Repository") && referencedType.getParameterCount() == 2) {
                        PsiType entityType = referencedType.getParameters()[0];
                        return entityType.getCanonicalText();
                    }
                }
            }
        }
        return "?";
    }

    @Nullable
    private XmlAttributeValue getNameAttribute(@NotNull XmlToken position) {
        if (!(position.getParent() instanceof XmlAttributeValue)) {
            return null;
        }
        XmlAttributeValue attributeValue = (XmlAttributeValue) position.getParent();
        if (!(attributeValue.getParent() instanceof XmlAttribute)) {
            return null;
        }
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        return "name".equals(attribute.getName()) ? attributeValue : null;
    }

    private boolean isDataSourceTag(XmlAttributeValue attributeValue) {
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        XmlTag tag = attribute.getParent();
        return UIBuilderFormUtils.DATA_SOURCE_TAG_NAME.contains(tag.getName());
    }

}
