package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlToken;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HtmlCrudPanelControllerBeanReferenceCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement psiElement = parameters.getOriginalPosition();
        if (psiElement instanceof XmlToken) {
            XmlAttributeValue controllerBeanAttribute = getWebComponentAttributeValue((XmlToken) psiElement);
            if (controllerBeanAttribute != null) {
                Project project = controllerBeanAttribute.getProject();
                GlobalSearchScope projectScope = UIBuilderProjectUtils.createSearchScope(project);
                FileBasedIndex fileBasedIndex = FileBasedIndex.getInstance();
                fileBasedIndex.getAllKeys(ControllerBeanIndex.INDEX_NAME, project).forEach(controllerBeanName -> {
                    fileBasedIndex.getContainingFiles(ControllerBeanIndex.INDEX_NAME, controllerBeanName, projectScope).stream()
                        .findFirst()
                        .ifPresent(controllerBeanFile -> {
                            result.addElement(
                                LookupElementBuilder
                                    .create(controllerBeanName)
                                    .withIcon(UIBuilderIcons.Gutter.EventReference)
                                    .withTypeText(controllerBeanFile.getPresentableName()));
                        });
                });

                result.stopHere();
            }
        }
    }

    @Nullable
    private XmlAttributeValue getWebComponentAttributeValue(@NotNull XmlToken position) {
        if (!(position.getParent() instanceof XmlAttributeValue)) {
            return null;
        }
        XmlAttributeValue attributeValue = (XmlAttributeValue) position.getParent();
        if (!(attributeValue.getParent() instanceof XmlAttribute)) {
            return null;
        }
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        return attribute.getName().equals("controller-bean") ? attributeValue : null;
    }

}
