package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlToken;
import io.devbench.uibuilder.idea.util.UIBuilderFlowUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class FlowIncludePathPathCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement psiElement = parameters.getOriginalPosition();
        if (psiElement instanceof XmlToken) {
            XmlAttributeValue htmlAttribute = getWebComponentAttributeValue((XmlToken) psiElement);
            if (htmlAttribute != null) {
                Project project = htmlAttribute.getProject();

                VirtualFile currentVirtualFile = psiElement.getContainingFile().getVirtualFile();
                VirtualFile currentFileSourceRoot = ProjectFileIndex.getInstance(project).getSourceRootForFile(currentVirtualFile);
                String currentFilePath = currentVirtualFile.getPath().substring(currentFileSourceRoot.getPath().length());

                UIBuilderFlowUtils.findAllFlowDefinitionXmlPaths(project).forEach((path, xmlFile) -> {
                    if (!path.equals(currentFilePath)) {
                        result.addElement(LookupElementBuilder.create(path));
                    }
                });

                result.stopHere();
            }
        }
    }

    @Nullable
    private XmlAttributeValue getWebComponentAttributeValue(@NotNull XmlToken position) {
        if (!(position.getParent() instanceof XmlAttributeValue)) {
            return null;
        }
        XmlAttributeValue attributeValue = (XmlAttributeValue) position.getParent();
        if (!(attributeValue.getParent() instanceof XmlAttribute)) {
            return null;
        }
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        if (attribute.getParent().getName().equals("include")) {
            return attribute.getName().equals("path") ? attributeValue : null;
        }
        return null;
    }

}
