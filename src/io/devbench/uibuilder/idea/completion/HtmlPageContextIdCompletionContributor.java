package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlToken;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.util.UIBuilderConstants;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HtmlPageContextIdCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement psiElement = parameters.getOriginalPosition();
        if (psiElement instanceof XmlToken) {
            XmlAttributeValue contextIdAttribute = getWebComponentAttributeValue((XmlToken) psiElement);
            if (contextIdAttribute != null) {
                Project project = contextIdAttribute.getProject();

                FileBasedIndex fileBasedIndex = FileBasedIndex.getInstance();
                fileBasedIndex.getAllKeys(ControllerBeanIndex.INDEX_NAME, project).forEach(controllerBeanName -> {
                    fileBasedIndex.getContainingFiles(ControllerBeanIndex.INDEX_NAME, controllerBeanName, UIBuilderProjectUtils.createSearchScope(project)).stream()
                        .findFirst()
                        .ifPresent(controllerBeanFile -> {

                            PsiJavaFile psiControllerBean = (PsiJavaFile) PsiManager.getInstance(project).findFile(controllerBeanFile);
                            if (psiControllerBean != null) {
                                for (PsiClass psiClass : psiControllerBean.getClasses()) {
                                    if (psiClass.hasAnnotation(UIBuilderConstants.FQN_PAGE_SCOPE_ANNOTATION)) {
                                        PsiAnnotation psiPageScpeAnnotation = psiClass.getAnnotation(UIBuilderConstants.FQN_PAGE_SCOPE_ANNOTATION);
                                        UIBuilderPsiTreeUtils.getValueAnnotationValue(psiPageScpeAnnotation).ifPresent(contextId -> {
                                            result.addElement(
                                                LookupElementBuilder
                                                    .create(contextId)
                                                    .withIcon(UIBuilderIcons.Gutter.Page)
                                                    .withTypeText(controllerBeanFile.getPresentableName()));
                                        });
                                    }
                                }
                            }
                        });
                });

                result.stopHere();
            }
        }
    }

    @Nullable
    private XmlAttributeValue getWebComponentAttributeValue(@NotNull XmlToken position) {
        if (!(position.getParent() instanceof XmlAttributeValue)) {
            return null;
        }
        XmlAttributeValue attributeValue = (XmlAttributeValue) position.getParent();
        if (!(attributeValue.getParent() instanceof XmlAttribute)) {
            return null;
        }
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        return attribute.getName().equals("contextId") || attribute.getName().equals("context-id") ? attributeValue : null;
    }

}
