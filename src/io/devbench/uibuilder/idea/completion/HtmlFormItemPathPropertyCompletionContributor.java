package io.devbench.uibuilder.idea.completion;

import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlToken;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HtmlFormItemPathPropertyCompletionContributor extends HtmlFormItemBindBeanPropertyCompletionContributor {

    @Override
    @Nullable
    protected XmlAttributeValue getWebComponentItemBindAttribute(@NotNull XmlToken position) {
        if (!(position.getParent() instanceof XmlAttributeValue)) {
            return null;
        }
        XmlAttributeValue attributeValue = (XmlAttributeValue) position.getParent();
        if (!(attributeValue.getParent() instanceof XmlAttribute)) {
            return null;
        }
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        return "path".equals(attribute.getName()) ? attributeValue : null;
    }

}
