package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.source.xml.XmlTokenImpl;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlToken;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanIndex;
import io.devbench.uibuilder.idea.util.UIBuilderFormUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.ArrayList;
import java.util.List;

public class HtmlBeanPropertyCompletionContributor extends AbstractClassPropertyAwareCompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement psiElement = parameters.getOriginalPosition();
        if (psiElement instanceof XmlToken) {
            Project project = psiElement.getProject();
            String text = psiElement.getText();
            PsiElement position = parameters.getPosition();

            String currentText = text.substring(0, parameters.getOffset() - ((XmlTokenImpl) position).getStartOffset());
            if (currentText.contains("{{")) {
                PsiClass memberClass = UIBuilderFormUtils.findItemPropertyClass(psiElement).orElse(null);
                String templateBeanName = UIBuilderFormUtils.findTemplateBeanName(psiElement);
                String propertyPath = currentText.substring(currentText.indexOf("{{") + 2);
                if (currentText.contains(".")) {
                    String prefix = propertyPath.substring(propertyPath.lastIndexOf('.') + 1);
                    propertyPath = propertyPath.substring(0, propertyPath.lastIndexOf('.'));
                    UIBuilderFormUtils.findMemberClassByPropertyPath(project, templateBeanName, propertyPath, memberClass, true)
                        .ifPresent(psiClass -> getAvailableProperties(psiClass, isItemDataSourceItemsAttribute(psiElement), false)
                            .forEach(lookupElementBuilder -> result
                                .withPrefixMatcher(prefix)
                                .addElement(lookupElementBuilder)));
                } else {
                    String prefix = propertyPath;
                    getAvailableControllerBeanName(project, templateBeanName, memberClass).forEach(lookupElementBuilder -> result
                        .withPrefixMatcher(prefix)
                        .addElement(lookupElementBuilder));
                }
                result.stopHere();
            }
        }
    }

    private boolean isItemDataSourceItemsAttribute(PsiElement element) {
        PsiElement parent = element.getParent();
        if (parent instanceof XmlAttributeValue) {
            XmlAttribute attribute = (XmlAttribute) parent.getParent();
            return "items".equalsIgnoreCase(attribute.getName())
                && "item-data-source".equalsIgnoreCase(attribute.getParent().getName());
        }
        return false;
    }

    private List<LookupElementBuilder> getAvailableControllerBeanName(@NotNull Project project,
                                                                      @NotNull String templateBeanName,
                                                                      @Nullable PsiClass itemClass) {

        List<LookupElementBuilder> availableControllerBeanNames = new ArrayList<>();
        if (itemClass != null) {
            availableControllerBeanNames.add(LookupElementBuilder
                .create(templateBeanName)
                .withTypeText(itemClass.getName(), true)
            );
        }

        FileBasedIndex fileBasedIndex = FileBasedIndex.getInstance();
        fileBasedIndex.getAllKeys(ControllerBeanIndex.INDEX_NAME, project).forEach(controllerBeanName -> {
            fileBasedIndex.getContainingFiles(ControllerBeanIndex.INDEX_NAME, controllerBeanName, GlobalSearchScope.allScope(project)).stream()
                .findFirst()
                .ifPresent(controllerBeanFile -> availableControllerBeanNames.add(LookupElementBuilder
                    .create(controllerBeanName)
                    .withTypeText(controllerBeanFile.getPresentableName())));
        });
        return availableControllerBeanNames;
    }

}
