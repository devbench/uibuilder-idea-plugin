package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.*;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractClassPropertyAwareCompletionContributor extends CompletionContributor {

    @NotNull
    private List<PsiType> getAllSupertypes(@NotNull PsiType type) {
        List<PsiType> types = new ArrayList<>();
        types.add(type);
        PsiType[] superTypes = type.getSuperTypes();
        for (PsiType superType : superTypes) {
            types.addAll(getAllSupertypes(superType));
        }
        return types.stream().distinct().collect(Collectors.toList());
    }

    private boolean isCollection(@NotNull PsiType type) {
        return getAllSupertypes(type).stream()
            .filter(PsiClassType.class::isInstance)
            .map(PsiClassType.class::cast)
            .map(PsiClassType::getClassName)
            .anyMatch("Collection"::equals);
    }

    @NotNull
    protected List<LookupElementBuilder> getAvailableProperties(@NotNull PsiClass memberClass) {
        return getAvailableProperties(memberClass, false, false);
    }

    @NotNull
    protected List<LookupElementBuilder> getAvailableProperties(@NotNull PsiClass memberClass, boolean onlyCollectionTypes, boolean preventCollectionTypes) {
        Map<String, String> properties = new HashMap<>();
        for (PsiField field : memberClass.getAllFields()) {
            if (onlyCollectionTypes && !isCollection(field.getType())) {
                continue;
            }
            if (preventCollectionTypes && isCollection(field.getType())) {
                continue;
            }
            properties.put(field.getName(), field.getType().getPresentableText());
        }
        for (PsiMethod method : memberClass.getAllMethods()) {
            String name = method.getName();
            if (onlyCollectionTypes && (method.getReturnType() == null || !isCollection(method.getReturnType()))) {
                continue;
            }
            if (preventCollectionTypes && method.getReturnType() != null && isCollection(method.getReturnType())) {
                continue;
            }
            String presentableText = method.getReturnType() != null ? method.getReturnType().getPresentableText() : "-";
            if (name.startsWith("get")) {
                name = StringUtil.decapitalize(name.substring(3));
                properties.put(name, presentableText);
            } else if (name.startsWith("is")) {
                name = StringUtil.decapitalize(name.substring(2));
                properties.put(name, presentableText);
            }
        }

        List<LookupElementBuilder> lookupElementBuilders = new ArrayList<>();
        properties.forEach((name, type) -> lookupElementBuilders.add(LookupElementBuilder
            .create(name)
            .withIcon(UIBuilderIcons.Gutter.PropertyBinding)
            .withTailText("  " + type, true)
            .withTypeText(memberClass.getName(), true)));
        return lookupElementBuilders;
    }

}
