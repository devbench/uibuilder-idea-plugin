package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlTag;
import com.intellij.psi.xml.XmlToken;
import io.devbench.uibuilder.idea.references.HtmlFormItemPropertyReference;
import io.devbench.uibuilder.idea.util.UIBuilderFormUtils;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HtmlFormItemBindBeanPropertyCompletionContributor extends AbstractClassPropertyAwareCompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement psiElement = parameters.getOriginalPosition();
        if (psiElement instanceof XmlToken) {
            XmlAttributeValue itemBind = getWebComponentItemBindAttribute((XmlToken) psiElement);
            if (itemBind != null) {
                Project project = itemBind.getProject();
                HtmlFormItemPropertyReference.findInitialMemberPsiClass(psiElement)
                    .or(() -> UIBuilderFormUtils.findItemPropertyClass(psiElement))
                    .ifPresent(memberClass -> {
                        String propertyPath = itemBind.getValue();
                        boolean onlyCollections = propertyPath.startsWith("items:");
                        int colonIndex = propertyPath.indexOf(':');
                        if (colonIndex != -1) {
                            propertyPath = propertyPath.substring(colonIndex + 1);
                        }
                        if (propertyPath.contains(".")) {
                            String prefix = propertyPath.substring(propertyPath.lastIndexOf('.') + 1);
                            UIBuilderFormUtils
                                .findMemberClassByPropertyPath(project, propertyPath.substring(0, propertyPath.lastIndexOf('.')), memberClass, false)
                                .ifPresent(psiClass -> getAvailableProperties(psiClass, onlyCollections, !onlyCollections)
                                    .forEach(lookupElementBuilder -> result.withPrefixMatcher(prefix).addElement(lookupElementBuilder)));
                        } else {
                            String prefix = propertyPath;
                            getAvailableProperties(memberClass, onlyCollections, !onlyCollections)
                                .forEach(lookupElementBuilder -> result.withPrefixMatcher(prefix).addElement(lookupElementBuilder));
                        }

                        if (colonIndex == -1 && !propertyPath.contains(".")) {
                            String tagName = getTagName(itemBind);
                            if (isContainerTag(tagName)) {
                                result.withPrefixMatcher(propertyPath).addElement(LookupElementBuilder
                                    .create("items:")
                                    .withIcon(UIBuilderIcons.Gutter.FormPropertyBinding)
                                    .withTailText("  Use a collection as data source", true));
                                if (!"item-data-source".equals(tagName)) {
                                    result.withPrefixMatcher(propertyPath).addElement(LookupElementBuilder
                                        .create("selected:")
                                        .withIcon(UIBuilderIcons.Gutter.FormPropertyBinding)
                                        .withTailText("  Use a backend driven two-way binding to the specified property", true));
                                    result.withPrefixMatcher(propertyPath).addElement(LookupElementBuilder
                                        .create("backend:")
                                        .withIcon(UIBuilderIcons.Gutter.FormPropertyBinding)
                                        .withTailText("  Use a backend driven two-way binding to the specified property", true));
                                }
                            } else if (isFormTag(tagName)) {
                                result.withPrefixMatcher(propertyPath).addElement(LookupElementBuilder
                                    .create("formItem:")
                                    .withIcon(UIBuilderIcons.Gutter.FormPropertyBinding)
                                    .withTailText("  Use a data object as data source", true));
                            }
                        }
                        result.stopHere();
                    });
            }
        }
    }

    private String getTagName(@NotNull XmlAttributeValue attributeValue) {
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        XmlTag tag = attribute.getParent();
        return tag.getName();
    }

    private boolean isFormTag(@NotNull String tagName) {
        return UIBuilderFormUtils.DATA_CONTAINER_FORM.contains(tagName);
    }

    private boolean isContainerTag(@NotNull String tagName) {
        return UIBuilderFormUtils.DATA_CONTAINER_TAGS.contains(tagName) || "item-data-source".equals(tagName);
    }

    @Nullable
    protected XmlAttributeValue getWebComponentItemBindAttribute(@NotNull XmlToken position) {
        if (!(position.getParent() instanceof XmlAttributeValue)) {
            return null;
        }
        XmlAttributeValue attributeValue = (XmlAttributeValue) position.getParent();
        if (!(attributeValue.getParent() instanceof XmlAttribute)) {
            return null;
        }
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        return "item-bind".equals(attribute.getName()) ? attributeValue : null;
    }

}
