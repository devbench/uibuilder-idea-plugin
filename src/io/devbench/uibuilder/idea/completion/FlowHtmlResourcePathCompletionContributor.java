package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.completion.PrefixMatcher;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.source.xml.XmlTokenImpl;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlToken;
import io.devbench.uibuilder.idea.util.UIBuilderHtmlUtils;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FlowHtmlResourcePathCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement psiElement = parameters.getOriginalPosition();
        if (psiElement instanceof XmlToken) {
            XmlAttributeValue htmlAttribute = getWebComponentAttributeValue((XmlToken) psiElement);
            if (htmlAttribute != null) {
                ProjectFileIndex projectFileIndex = ProjectFileIndex.getInstance(htmlAttribute.getProject());

                List<String> completionPaths = new ArrayList<>();
                projectFileIndex.iterateContent(html -> {
                    VirtualFile sourceRootForFile = projectFileIndex.getSourceRootForFile(html);
                    if (sourceRootForFile != null) {
                        String relativePath = html.getPath().substring(sourceRootForFile.getPath().length());
                        completionPaths.add(relativePath);
                    }
                    return true;
                }, fileOrDir -> !fileOrDir.isDirectory() && UIBuilderHtmlUtils.isUIBuilderHtmlFile(fileOrDir));

                CompletionResultSet resultSet = result.withPrefixMatcher(new ContainsPrefixMatcher(getCurrentText(parameters, psiElement)));
                resultSet.addAllElements(completionPaths.stream()
                    .map(path -> LookupElementBuilder.create(path).withIcon(UIBuilderIcons.Gutter.Page))
                    .collect(Collectors.toList()));
                resultSet.stopHere();
                result.stopHere();
            }
        }
    }

    @NotNull
    private String getCurrentText(@NotNull CompletionParameters parameters, PsiElement psiElement) {
        String text = psiElement.getText();
        PsiElement position = parameters.getPosition();
        return text.substring(0, parameters.getOffset() - ((XmlTokenImpl) position).getStartOffset());
    }

    @Nullable
    private XmlAttributeValue getWebComponentAttributeValue(@NotNull XmlToken position) {
        if (!(position.getParent() instanceof XmlAttributeValue)) {
            return null;
        }
        XmlAttributeValue attributeValue = (XmlAttributeValue) position.getParent();
        if (!(attributeValue.getParent() instanceof XmlAttribute)) {
            return null;
        }
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        if (attribute.getParent().getName().equals("flow")) {
            return attribute.getName().equals("html") ? attributeValue : null;
        }
        return null;
    }

    public static class ContainsPrefixMatcher extends PrefixMatcher {
        public ContainsPrefixMatcher(String prefix) {
            super(prefix);
        }

        @Override
        public boolean prefixMatches(@NotNull String name) {
            return name.toLowerCase().contains(getPrefix().toLowerCase());
        }

        @NotNull
        @Override
        public PrefixMatcher cloneWithPrefix(@NotNull String prefix) {
            return this;
        }
    }
}
