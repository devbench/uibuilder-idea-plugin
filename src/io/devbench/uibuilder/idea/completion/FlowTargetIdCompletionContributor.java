package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlToken;
import io.devbench.uibuilder.idea.util.UIBuilderHtmlUtils;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class FlowTargetIdCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement psiElement = parameters.getOriginalPosition();
        if (psiElement instanceof XmlToken) {
            XmlAttributeValue targetAttribute = getWebComponentAttributeValue((XmlToken) psiElement);
            if (targetAttribute != null) {
                Project project = targetAttribute.getProject();
                PsiManager psiManager = PsiManager.getInstance(project);

                UIBuilderProjectUtils.iterateProjectFileIndexInUIBuilderScope(project, html -> {
                    XmlFile psiHtml = (XmlFile) psiManager.findFile(html);
                    if (psiHtml != null) {
                        psiHtml.accept(new XmlRecursiveElementVisitor() {
                            @Override
                            public void visitXmlAttribute(XmlAttribute attribute) {
                                if (attribute.getName().equals("id")
                                    && attribute.getParent().getName().equals("uibuilder-page")
                                    && attribute.getValue() != null) {
                                    result.addElement(LookupElementBuilder
                                        .create(attribute.getValue())
                                        .withIcon(UIBuilderIcons.Gutter.Page)
                                        .withTypeText(html.getPresentableName()));
                                }
                                super.visitXmlAttribute(attribute);
                            }
                        });
                    }
                    return true;
                }, fileOrDir -> !fileOrDir.isDirectory() && UIBuilderHtmlUtils.isUIBuilderHtmlFile(fileOrDir));

                result.stopHere();
            }
        }
    }

    @Nullable
    private XmlAttributeValue getWebComponentAttributeValue(@NotNull XmlToken position) {
        if (!(position.getParent() instanceof XmlAttributeValue)) {
            return null;
        }
        XmlAttributeValue attributeValue = (XmlAttributeValue) position.getParent();
        if (!(attributeValue.getParent() instanceof XmlAttribute)) {
            return null;
        }
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        if (attribute.getParent().getName().equals("flow")) {
            return attribute.getName().equals("target") ? attributeValue : null;
        }
        return null;
    }

}
