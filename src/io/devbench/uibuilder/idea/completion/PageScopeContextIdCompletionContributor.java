package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiJavaToken;
import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlAttribute;
import io.devbench.uibuilder.idea.util.UIBuilderHtmlUtils;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import org.jetbrains.annotations.NotNull;

import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;

public class PageScopeContextIdCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement element = parameters.getOriginalPosition();
        if (element instanceof PsiJavaToken) {
            PsiAnnotation annotation = PsiTreeUtil.getParentOfType(element, PsiAnnotation.class, true);
            if (annotation != null && annotation.hasQualifiedName(FQN_PAGE_SCOPE_ANNOTATION)) {

                Project project = annotation.getProject();

                UIBuilderHtmlUtils.findInHtml(project, new XmlRecursiveElementVisitor() {
                    @Override
                    public void visitXmlAttribute(XmlAttribute attribute) {
                        if (attribute.getParent().getName().equals("uibuilder-page")
                            && (attribute.getName().equals("contextId") || attribute.getName().equals("context-id"))
                            && attribute.getValue() != null) {
                            result.addElement(
                                LookupElementBuilder
                                    .create(attribute.getValue())
                                    .withIcon(UIBuilderIcons.Gutter.Page)
                                    .withTypeText(attribute.getContainingFile().getVirtualFile().getPresentableName()));
                        }
                        super.visitXmlAttribute(attribute);
                    }
                });

                result.stopHere();
            }
        }
    }

}
