package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlToken;
import io.devbench.uibuilder.idea.util.UIBuilderFlowUtils;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class FlowFallbackIdCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement psiElement = parameters.getOriginalPosition();
        if (psiElement instanceof XmlToken) {
            XmlAttributeValue targetAttribute = getWebComponentAttributeValue((XmlToken) psiElement);
            if (targetAttribute != null) {
                Project project = targetAttribute.getProject();

                UIBuilderFlowUtils.withAllFlowDefinitions(project, (flowId, route, html) -> result.addElement(
                    LookupElementBuilder
                        .create(flowId)
                        .withIcon(UIBuilderIcons.Gutter.Page)
                        .withTailText("  " + route, true)
                        .withTypeText(html)));

                result.stopHere();
            }
        }
    }

    @Nullable
    private XmlAttributeValue getWebComponentAttributeValue(@NotNull XmlToken position) {
        if (!(position.getParent() instanceof XmlAttributeValue)) {
            return null;
        }
        XmlAttributeValue attributeValue = (XmlAttributeValue) position.getParent();
        if (!(attributeValue.getParent() instanceof XmlAttribute)) {
            return null;
        }
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        if (attribute.getName().equals("fallbackId")) {
            return attributeValue;
        }
        return null;
    }

}
