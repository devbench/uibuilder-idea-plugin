package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.ide.highlighter.HtmlFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiJavaToken;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.HtmlEventIndex;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import io.devbench.uibuilder.idea.util.UIBuilderPsiTreeUtils;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;

public class UIEventHandlerCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement element = parameters.getOriginalPosition();
        if (element instanceof PsiJavaToken) {
            PsiAnnotation annotation = PsiTreeUtil.getParentOfType(element, PsiAnnotation.class, true);
            if (annotation != null && annotation.hasQualifiedName(FQN_UI_EVENT_HANDLER_ANNOTATION)) {
                findControllerBeanName(annotation).ifPresent(controllerBeanName -> {
                    Project project = annotation.getProject();

                    List<VirtualFile> htmlFiles = new ArrayList<>();
                    UIBuilderProjectUtils.iterateProjectFileIndexInUIBuilderScope(project, htmlFile -> {
                        htmlFiles.add(htmlFile);
                        return true;
                    }, file -> HtmlFileType.INSTANCE.equals(file.getFileType()));

                    htmlFiles.forEach(htmlFile -> {
                        Map<String, String> data = FileBasedIndex.getInstance().getFileData(HtmlEventIndex.INDEX_NAME, htmlFile, project);
                        data.forEach((eventReference, eventAttributeName) -> {
                            if (eventReference.startsWith(controllerBeanName + "::")) {
                                String eventName = extractEventName(eventReference);
                                if (eventName != null && !eventName.trim().isEmpty()) {
                                    result.addElement(
                                        LookupElementBuilder
                                            .create(eventName.trim())
                                            .withTailText("  " + eventAttributeName, true)
                                            .withIcon(UIBuilderIcons.Gutter.EventReference)
                                            .withTypeText(htmlFile.getPresentableName()));
                                }
                            }
                        });
                    });

                    result.stopHere();
                });
            }
        }
    }

    private Optional<String> findControllerBeanName(PsiElement element) {
        PsiClass psiClass = PsiTreeUtil.getParentOfType(element, PsiClass.class, true);
        if (psiClass != null) {
            PsiAnnotation annotation = psiClass.getAnnotation(FQN_CONTROLLER_BEAN_ANNOTATION);
            if (annotation != null) {
                return UIBuilderPsiTreeUtils.getValueAnnotationValue(annotation);
            }
        }
        return Optional.empty();
    }

    private String extractEventName(String referenceWithControllerBean) {
        String[] split = referenceWithControllerBean.split("::");
        return split.length == 2 ? split[1] : null;
    }

}
