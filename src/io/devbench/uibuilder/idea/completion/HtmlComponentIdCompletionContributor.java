package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlTag;
import com.intellij.psi.xml.XmlToken;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.ControllerBeanComponentIdIndex;
import io.devbench.uibuilder.idea.references.HtmlComponentIdReference;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HtmlComponentIdCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement psiElement = parameters.getOriginalPosition();
        if (psiElement instanceof XmlToken) {
            XmlAttributeValue idAttribute = getWebComponentIdAttribute((XmlToken) psiElement);
            if (idAttribute != null) {

                Project project = idAttribute.getProject();
                XmlTag tag = ((XmlAttribute) idAttribute.getParent()).getParent();
                String tagName = tag.getName();

                List<VirtualFile> javaFiles = new ArrayList<>();
                UIBuilderProjectUtils.iterateProjectFileIndexInUIBuilderScope(project, javaFile -> {
                    javaFiles.add(javaFile);
                    return true;
                }, file -> JavaFileType.INSTANCE.equals(file.getFileType()));

                javaFiles.forEach(javaFile -> {
                    Map<String, String> data = FileBasedIndex.getInstance().getFileData(ControllerBeanComponentIdIndex.INDEX_NAME, javaFile, project);
                    data.forEach((idValue, presentationText) -> {
                        for (PsiElement injectionPoint : HtmlComponentIdReference.resolveComponentIdInjection(idValue, project)) {
                            HtmlComponentIdReference.findPsiTypeOfUIComponentInjection(injectionPoint).ifPresent(psiType -> {
                                if (UIComponentIdCompletionContributor.isTagMatchWithParamType(project, tagName, psiType)) {
                                    result.addElement(
                                        LookupElementBuilder
                                            .create(idValue)
                                            .withTailText("  " + presentationText, true)
                                            .withIcon(UIBuilderIcons.Gutter.UIComponent)
                                            .withTypeText(javaFile.getPresentableName()));
                                }
                            });
                        }
                    });
                });

                result.stopHere();
            }
        }
    }

    @Nullable
    private XmlAttributeValue getWebComponentIdAttribute(@NotNull XmlToken position) {
        if (!(position.getParent() instanceof XmlAttributeValue)) {
            return null;
        }
        XmlAttributeValue attributeValue = (XmlAttributeValue) position.getParent();
        if (!(attributeValue.getParent() instanceof XmlAttribute)) {
            return null;
        }
        XmlAttribute attribute = (XmlAttribute) attributeValue.getParent();
        return "id".equals(attribute.getName()) ? attributeValue : null;
    }

}
