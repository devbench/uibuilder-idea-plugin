package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.ide.highlighter.HtmlFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.util.PsiTypesUtil;
import com.intellij.util.indexing.FileBasedIndex;
import io.devbench.uibuilder.idea.index.HtmlComponentIdIndex;
import io.devbench.uibuilder.idea.references.HtmlComponentIdReference;
import io.devbench.uibuilder.idea.util.UIBuilderIcons;
import io.devbench.uibuilder.idea.util.UIBuilderProjectUtils;
import io.devbench.uibuilder.idea.xml.html.UIBuilderTagNameProvider;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static io.devbench.uibuilder.idea.util.UIBuilderConstants.*;

public class UIComponentIdCompletionContributor extends CompletionContributor {

    @Override
    public void fillCompletionVariants(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement element = parameters.getOriginalPosition();
        if (element instanceof PsiJavaToken) {
            PsiAnnotation annotation = PsiTreeUtil.getParentOfType(element, PsiAnnotation.class, true);
            if (annotation != null && annotation.hasQualifiedName(FQN_UI_COMPONENT_ANNNOTATION)) {

                Project project = annotation.getProject();

                Optional<PsiType> paramType = findPsiType(element);
                List<VirtualFile> htmlFiles = new ArrayList<>();
                UIBuilderProjectUtils.iterateProjectFileIndexInUIBuilderScope(project, htmlFile -> {
                    htmlFiles.add(htmlFile);
                    return true;
                }, file -> HtmlFileType.INSTANCE.equals(file.getFileType()));

                htmlFiles.forEach(htmlFile -> {
                    Map<String, String> data = FileBasedIndex.getInstance().getFileData(HtmlComponentIdIndex.INDEX_NAME, htmlFile, project);
                    data.forEach((idValue, tagName) -> {
                        if (paramType.isEmpty() || isTagMatchWithParamType(project, tagName, paramType.get())) {
                            result.addElement(
                                LookupElementBuilder
                                    .create(idValue)
                                    .withTailText("  <" + tagName + ">", true)
                                    .withIcon(UIBuilderIcons.Gutter.UIComponent)
                                    .withTypeText(htmlFile.getPresentableName()));
                        }
                    });
                });

                result.stopHere();
            }
        }
    }

    static boolean isTagMatchWithParamType(Project project, String tagName, PsiType paramType) {
        PsiClass paramTypeClass = PsiTypesUtil.getPsiClass(paramType);
        if (paramTypeClass != null) {
            PsiClass componentClass = UIBuilderTagNameProvider.findUIBuilderSupportedTagDeclaration(project, tagName)
                .filter(PsiJavaFile.class::isInstance)
                .map(PsiJavaFile.class::cast)
                .map(psiJavaFile -> {
                    PsiClass[] classes = psiJavaFile.getClasses();
                    return classes.length > 0 ? classes[0] : null;
                })
                .orElse(null);

            if (componentClass != null) {
                return paramTypeClass.isEquivalentTo(componentClass) || paramTypeClass.isInheritor(componentClass, true);
            }
        }

        return false;
    }

    private static Optional<PsiType> findPsiType(PsiElement element) {
        PsiElement psiContext = PsiTreeUtil.getParentOfType(element, PsiParameter.class, true);
        if (psiContext == null) {
            psiContext = PsiTreeUtil.getParentOfType(element, PsiField.class, true);
        }
        return HtmlComponentIdReference.findPsiTypeOfUIComponentInjection(psiContext);
    }

}
