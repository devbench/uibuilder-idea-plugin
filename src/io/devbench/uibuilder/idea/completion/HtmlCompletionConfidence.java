package io.devbench.uibuilder.idea.completion;

import com.intellij.codeInsight.completion.CompletionConfidence;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.ThreeState;
import org.jetbrains.annotations.NotNull;

// task: refactor this (Create components and pass the responsibility to them to decide the completion mode)
public class HtmlCompletionConfidence extends CompletionConfidence {

    @NotNull
    @Override
    public ThreeState shouldSkipAutopopup(@NotNull PsiElement contextElement, @NotNull PsiFile psiFile, int offset) {
        if (contextElement.getParent() instanceof XmlAttributeValue) {
            XmlAttribute attribute = (XmlAttribute) contextElement.getParent().getParent();
            if (attribute.getName().startsWith("on-") || isAutoCompletionPopupAttribute(attribute)) {
                return ThreeState.NO;
            }
        }
        return ThreeState.UNSURE;
    }

    private boolean isAutoCompletionPopupAttribute(XmlAttribute attribute) {
        if (isComponentIdCompletionSupported(attribute)) {
            return true;
        }

        if (isItemBindTypeCompletionSupported(attribute)) {
            return true;
        }

        if (isDataSourceCompletionSupported(attribute)) {
            return true;
        }

        if ("form-control".equals(attribute.getName())) {
            return true;
        }

        return false;
    }

    private boolean isItemBindTypeCompletionSupported(XmlAttribute attribute) {
        XmlTag tag = attribute.getParent();
        String tagName = tag.getName();
        String attributeName = attribute.getName();
        if ("item-bind".equals(attributeName)) {
            return true;
        }
        if ("path".equals(attributeName) && "vaadin-grid-sorter".equals(tagName)) {
            return true;
        }
        if ("value-item-path".equals(attributeName) && "uibuilder-multi-value".equals(tagName)) {
            return true;
        }
        if ("filter".equals(attributeName) && "filter-type".equals(tagName)) {
            return true;
        }

        return false;
    }

    private boolean isDataSourceCompletionSupported(XmlAttribute attribute) {
        XmlTag tag = attribute.getParent();
        String tagName = tag.getName();
        String attributeName = attribute.getName();

        if ("items".equals(attributeName) && "item-data-source".equals(tagName)) {
            return true;
        }

        if ("name".equals(attributeName) && "data-source".equals(tagName)) {
            return true;
        }

        return false;
    }

    private boolean isComponentIdCompletionSupported(XmlAttribute attribute) {
        if ("id".equals(attribute.getName())) {
            return true;
        }
        return false;
    }


}
