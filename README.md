# UIBuilder plugin for Jetbrains IntelliJ IDEA [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

This is a UIBuilder plugin for [IntelliJ IDEA](https://www.jetbrains.com/idea).  

### Building the plugin

To compile the plugin simply run the following command in the root of the project:

```
./gradlew buildPlugin
```

To run the plugin in a separated IntelliJ Idea sandbox, use the following command:

```
./gradlew runIde
```
